import { getColor } from "../utils/formatData";
import * as d3 from "d3";

export const heartRateZones = [
  { min: 0, max: 110, color: getColor("heart-rate-zone1-color") },
  { min: 110, max: 130, color: getColor("heart-rate-zone2-color") },
  { min: 130, max: 150, color: getColor("heart-rate-zone3-color") },
  { min: 150, max: 170, color: getColor("heart-rate-zone4-color") },
  { min: 170, max: 200, color: getColor("heart-rate-zone5-color") },
];

export const heartRateZonesColors = d3
  .scaleOrdinal()
  .range([
    getColor("heart-rate-zone1-color"),
    getColor("heart-rate-zone2-color"),
    getColor("heart-rate-zone3-color"),
    getColor("heart-rate-zone4-color"),
    getColor("heart-rate-zone5-color"),
  ]);
