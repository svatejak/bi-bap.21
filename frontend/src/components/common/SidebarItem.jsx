import React from "react";
import "../../styles/App.css";
import { Link } from "react-router-dom";

const SidebarItem = ({ src, text, linkTo, active }) => {
  return (
    <Link className={`sidebarItem ${active ? "active" : ""}`} to={linkTo}>
      <img className="icon" src={src} alt="icon" />
      <span>{text}</span>
    </Link>
  );
};

export default SidebarItem;
