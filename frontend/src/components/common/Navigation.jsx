import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import NavigationItem from "./NavigationItem";
import generateNavigation from "../../utils/generateNavigation";
import { useQuery } from "react-query";
import { getExercise } from "../../utils/api";

function Navigation() {
  const [navigationItems, setNavigationItems] = useState([]);
  const location = useLocation();
  const pageName = location.pathname.split("/")[1];
  const exerciseId =
    pageName === "exercises" ? location.pathname.split("/")[2] : null;
  const navigationItem = getNavigationItem();
  const {
    data: exercise,
    isLoading: exerciseLoading,
    error: exerciseError,
  } = useQuery(["exercise", exerciseId], () => getExercise(exerciseId), {
    enabled: !!exerciseId,
  });

  function getNavigationItem() {
    const lastSegment = location.pathname.split("/").pop();
    if (!isNaN(lastSegment)) return "exercises";
    return lastSegment;
  }

  useEffect(() => {
    if (exerciseLoading || exerciseError) return;
    generateNavigation(
      pageName,
      navigationItem,
      setNavigationItems,
      exerciseId,
      exercise,
    );
  }, [location, exerciseLoading]);

  return (
    <div className="navigation">
      {navigationItems.map((item) => (
        <NavigationItem
          key={item.text}
          text={item.text}
          linkTo={item.linkTo}
          active={item.active}
        />
      ))}
    </div>
  );
}

export default Navigation;
