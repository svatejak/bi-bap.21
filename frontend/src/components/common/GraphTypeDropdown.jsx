import React from "react";

function GraphTypeDropdown({ graphType, setGraphType, options }) {
  const handleOptionChange = (e) => {
    setGraphType(e.target.value);
  };

  return (
    <select
      className="graphTypeDropdown"
      value={graphType}
      onChange={handleOptionChange}
    >
      {options.map((option) => (
        <option key={option.value} value={option.value}>
          {option.label}
        </option>
      ))}
    </select>
  );
}

export default GraphTypeDropdown;
