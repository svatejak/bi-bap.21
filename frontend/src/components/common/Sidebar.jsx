import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import SidebarItem from "./SidebarItem";
import generateSidebar from "../../utils/generateSidebar";
import defaultSidebar from "../../utils/defaultSidebar";

function Sidebar() {
  const [sidebarItems, setSidebarItems] = useState(defaultSidebar);
  const location = useLocation();
  const pageName = location.pathname.split("/")[1];

  useEffect(() => {
    generateSidebar(sidebarItems, setSidebarItems, pageName);
  }, [location]);

  return (
    <div className="sidebar">
      {sidebarItems.map((item, index) => (
        <SidebarItem
          key={item.text}
          src={item.src}
          text={item.text}
          linkTo={item.linkTo}
          active={item.active}
        />
      ))}
    </div>
  );
}

export default Sidebar;
