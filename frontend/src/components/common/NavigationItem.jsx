import React from "react";
import { Link } from "react-router-dom";

const NavigationItem = ({ text, linkTo, active }) => {
  return (
    <Link className={`navigationItem ${active ? "active" : ""}`} to={linkTo}>
      <span>{text}</span>
      {active && <div className="activeIndicator"></div>}
    </Link>
  );
};

export default NavigationItem;
