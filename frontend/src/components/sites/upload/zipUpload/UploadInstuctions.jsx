import { useState } from "react";

function UploadInstructions() {
  const [activeTab, setActiveTab] = useState("GoogleFit");

  return (
    <div className="uploadItem">
      <div className="uploadInstructions">
        <select
          className="selectButton"
          defaultValue="GoogleFit"
          onChange={(e) => setActiveTab(e.target.value)}
        >
          <option value="GoogleFit">Google Fit</option>
          <option value="SamsungHealth">Samsung Health</option>
        </select>

        {activeTab === "GoogleFit" && (
          <>
            <div className="instructionHeader">
              How to Export ZIP Data from Google Fit
            </div>
            <div className="instructionContent">
              <ol>
                <li>
                  <strong>Set the language to English in Google Account</strong>
                  <ul>
                    <li>
                      If the language is not set to English, change it using the
                      instructions here:{" "}
                      <a
                        href="https://support.google.com/accounts/answer/32047?hl=en&co=GENIE.Platform%3DDesktop"
                        target="_blank"
                      >
                        Change Google Account Language
                      </a>
                      .
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Go to Google Takeout</strong>
                  <ul>
                    <li>
                      Visit the{" "}
                      <a href="https://takeout.google.com/" target="_blank">
                        Google Takeout
                      </a>{" "}
                      website.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Log in with your Google account</strong>
                  <ul>
                    <li>
                      If you are not already signed in, log in using your Google
                      account.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Deselect all services</strong>
                  <ul>
                    <li>
                      By default, all services are selected on the Google
                      Takeout page. To clear the selection, click on the blue
                      text that says <em>"Deselect all"</em>. This will deselect
                      all services, allowing you to specifically choose the data
                      you want to export.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Select Google Fit data</strong>
                  <ul>
                    <li>
                      Scroll through the list of services and find the{" "}
                      <em>Google Fit</em> option.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Create the export</strong>
                  <ul>
                    <li>
                      After selecting the data, click on <em>Next step</em> and
                      then <em>Create export</em>.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Receive your data</strong>
                  <ul>
                    <li>
                      After a few minutes to hours, you will receive an email
                      with a link to download a zip file containing your data.
                    </li>
                  </ul>
                </li>
              </ol>
            </div>
          </>
        )}

        {activeTab === "SamsungHealth" && (
          <>
            <div className="instructionHeader">
              How to Export ZIP Data from Samsung Health
            </div>
            <div className="instructionContent">
              <ol>
                <li>
                  <strong>Open the Samsung Health app</strong>
                  <ul>
                    <li>
                      Tap <em>More options</em> (the three vertical dots), and
                      then tap <em>Settings</em>.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Download personal data</strong>
                  <ul>
                    <li>
                      Swipe to and tap <em>Download personal data</em>, then tap{" "}
                      <em>Download</em>.
                    </li>
                    <li>Allow the necessary permissions if prompted.</li>
                  </ul>
                </li>
                <li>
                  <strong>Enter Samsung account information</strong>
                  <ul>
                    <li>
                      When prompted, enter your Samsung account information and
                      wait for the data to download.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Find the Downloaded Data</strong>
                  <ul>
                    <li>
                      Navigate to <em>My Files</em> on your mobile device, then
                      tap <em>Downloads</em>.
                    </li>
                    <li>
                      Locate the <em>Samsung Health</em> folder and browse the
                      downloaded files.
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Zip the Downloaded Data</strong>
                  <ul>
                    <li>
                      Select the downloaded files, then tap{" "}
                      <em>More options</em> (the three vertical dots) and choose{" "}
                      <em>Zip</em> to compress the files into a ZIP folder.
                    </li>
                  </ul>
                </li>
              </ol>
            </div>
          </>
        )}
      </div>
    </div>
  );
}

export default UploadInstructions;
