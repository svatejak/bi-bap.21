import React from "react";
import DataUploadForm from "./DataUploadForm";
import UploadInstructions from "./UploadInstuctions";

function Upload() {
  return (
    <div className="uploadLayout">
      <div className="uploadItem">
        <DataUploadForm />
      </div>
      <UploadInstructions />
    </div>
  );
}

export default Upload;
