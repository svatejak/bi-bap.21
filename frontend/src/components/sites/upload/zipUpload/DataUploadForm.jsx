import React, { useState, useRef } from "react";
import s3 from "../../../../services/s3Client";
import { uploadFiles } from "../../../../utils/api";
import { useQueryClient } from "react-query";

const DataUploadForm = () => {
  const queryClient = useQueryClient();
  const [samsungHealthFile, setSamsungHealthFile] = useState(null);
  const [googleFitFile, setGoogleFitFile] = useState(null);
  const [uploading, setUploading] = useState(false);
  const [uploadMessage, setUploadMessage] = useState("");

  const samsungHealthInputRef = useRef(null);
  const googleFitInputRef = useRef(null);

  const resetFileInputs = () => {
    setSamsungHealthFile(null);
    setGoogleFitFile(null);
    if (samsungHealthInputRef.current) {
      samsungHealthInputRef.current.value = "";
    }
    if (googleFitInputRef.current) {
      googleFitInputRef.current.value = "";
    }
  };

  const handleSamsungHealthFileChange = (e) => {
    setSamsungHealthFile(e.target.files[0]);
  };

  const handleGoogleFitFileChange = (e) => {
    setGoogleFitFile(e.target.files[0]);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      setUploadMessage("");
      setUploading(true);

      const filesToUpload = [];

      if (samsungHealthFile) {
        filesToUpload.push({
          file: samsungHealthFile,
          dataSource: "SamsungHealth",
        });
      }

      if (googleFitFile) {
        filesToUpload.push({
          file: googleFitFile,
          dataSource: "GoogleFit",
        });
      }

      for (const { file } of filesToUpload) {
        const params = {
          Bucket: "default",
          Key: file.name,
          Body: file,
          ContentType: file.type,
        };

        await s3.putObject(params).promise();
      }

      const fileData = filesToUpload.map(({ file, dataSource }) => ({
        name: file.name,
        type: file.type,
        dataSource: dataSource,
      }));

      await uploadFiles(fileData);
      queryClient.invalidateQueries();
      setUploadMessage("Files uploaded successfully!");

      resetFileInputs();
    } catch (error) {
      const errorMessage = typeof error.message === 'string' ? error.message : 'An error occurred during upload.';
      setUploadMessage(errorMessage);
    }
    setUploading(false);
  };

  return (
    <form className="uploadDataForm" onSubmit={handleSubmit}>
      <div className="uploadRow">
        <label htmlFor="samsungHealthFile">
          Select a ZIP file from Samsung Health.
        </label>
        <input
          className="fileInput"
          id="samsungHealthFile"
          type="file"
          accept=".zip"
          title="Choose file"
          onChange={handleSamsungHealthFileChange}
          ref={samsungHealthInputRef}
        />
      </div>
      <div className="uploadRow">
        <label htmlFor="googleFitFile">
          Select a ZIP file from Google Fit.
        </label>
        <input
          className="fileInput"
          id="googleFitFile"
          type="file"
          accept=".zip"
          title="Choose file"
          onChange={handleGoogleFitFileChange}
          ref={googleFitInputRef}
        />
      </div>
      <button className="submitUpload" type="submit" disabled={uploading}>
        Upload
      </button>
      <div id="uploadStatus">
        {uploading && <div className="loader"></div>}
        {uploadMessage}
      </div>
    </form>
  );
};

export default DataUploadForm;
