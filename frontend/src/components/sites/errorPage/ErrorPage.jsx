import React from "react";

function ErrorPage({ errorCode }) {
  const errorMessage = getErrorMessage(errorCode);

  function getErrorMessage(errorCode) {
    switch (errorCode) {
      case 400:
        return "Bad request";
      case 404:
        return "Page not found";
      case 500:
        return "Internal server error";
      default:
        return "An error occurred";
    }
  }

  return (
    <div className="errorContainer">
      <h1>{errorCode}</h1>
      <h2>{errorMessage}</h2>
    </div>
  );
}

export default ErrorPage;
