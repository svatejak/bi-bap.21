import React, { useEffect, useRef } from "react";
import {
  getUnit,
  convertSpeedToPace,
  minutesToMiliseconds,
  formatDuration,
  truncateDecimals,
  formatPace,
  convertSpeed,
} from "../../../../utils/formatData";

function MotionToolBar({ hoveredTime, startTime, motionData }) {
  const timeBoxRef = useRef(null);
  const speedBoxRef = useRef(null);
  const paceBoxRef = useRef(null);
  const cadenceBoxRef = useRef(null);

  useEffect(() => {
    if (hoveredTime === null) {
      timeBoxRef.current.innerHTML = "";
      speedBoxRef.current.innerHTML = "";
      paceBoxRef.current.innerHTML = "";
      cadenceBoxRef.current.innerHTML = "";
    } else {
      const timeLimit = 2000;
      const time = minutesToMiliseconds(hoveredTime) + startTime;
      const closestDataPoint = motionData.find(
        (d) => Math.abs(time - d.startTime) < timeLimit,
      );
      if (!closestDataPoint) return;
      timeBoxRef.current.innerHTML = formatDuration(
        minutesToMiliseconds(hoveredTime),
      );

      if (closestDataPoint.speed !== null) {
        speedBoxRef.current.innerHTML = `${convertSpeed(closestDataPoint.speed)} ${getUnit("speed")}`;
        paceBoxRef.current.innerHTML = `${formatPace(convertSpeedToPace(closestDataPoint.speed, "s/km"))} ${getUnit("paceShort")}`;
      }
      if (closestDataPoint.cadence !== null) {
        cadenceBoxRef.current.innerHTML = `${truncateDecimals(closestDataPoint.cadence, 0)} ${getUnit("cadence")}`;
      }
    }
  }, [hoveredTime]);

  return (
    <div className="motionToolBar">
      <div className="motionToolBarTimeBox">
        <div className="timeBoxLabel">Time</div>
        <div ref={timeBoxRef} className="timeBoxContent"></div>
      </div>
      <div className="motionToolBarBox">
        <div className="box-label">Speed</div>
        <div ref={speedBoxRef} className="content"></div>
      </div>
      <div className="motionToolBarBox">
        <div className="box-label">Pace</div>
        <div ref={paceBoxRef} className="content"></div>
      </div>
      <div className="motionToolBarBox">
        <div className="box-label">Cadence</div>
        <div ref={cadenceBoxRef} className="content"></div>
      </div>
    </div>
  );
}

export default MotionToolBar;
