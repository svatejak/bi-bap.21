import React, { useState, useMemo } from "react";
import { useParams } from "react-router-dom";
import { getExercise, getExerciseMotionData } from "../../../../utils/api";
import { getUnit, milisecondsToMinutes } from "../../../../utils/formatData";

import MotionGraph from "../../../../charts/MotionGraph";

import {
  processSpeedData,
  processPaceData,
  processCadenceData,
} from "../../../../utils/processData";
import { useQuery } from "react-query";
import ErrorPage from "../../errorPage/ErrorPage";
import checkForErrors from "../../../../utils/checkForErrors";
import MotionToolBar from "./MotionToolBar";
import { remToPixels } from "../../../../utils/formatData";

const graphSizeInfo = {
  width: remToPixels(65),
  height: remToPixels(31),
  marginTop: remToPixels(6),
  marginRight: remToPixels(4),
  marginBottom: remToPixels(5),
  marginLeft: remToPixels(5),
};

function processMotionData(motionData, startTime, loading, errors) {
  if (
    loading ||
    errors.hasError ||
    !motionData ||
    motionData.length === 0 ||
    !startTime
  )
    return;

  let res = {};
  res.speedData = processSpeedData(motionData, startTime);
  res.paceData = processPaceData(motionData, startTime);
  res.cadenceData = processCadenceData(motionData, startTime);
  return res;
}

function getGraphs(exercise) {
  if (!exercise) return [];

  let validGraphs = [];
  if (exercise.meanSpeed !== null) {
    validGraphs.push("speed");
    validGraphs.push("pace");
  }
  if (exercise.meanCadence !== null) {
    validGraphs.push("cadence");
  }
  return validGraphs;
}

function ExerciseMotion() {
  const { exerciseId } = useParams();
  const {
    data: exercise,
    isLoading: exerciseLoading,
    error: exerciseError,
  } = useQuery(["exercise", exerciseId], () => getExercise(exerciseId));

  const {
    data: motionData,
    isLoading: motionDataLoading,
    error: motionDataError,
  } = useQuery(["exerciseMotionData", exerciseId], () =>
    getExerciseMotionData(exerciseId),
  );

  const startTime = useMemo(() => {
    return exercise ? new Date(exercise.startTime).getTime() : null;
  }, [exercise]);

  const loading = exerciseLoading || motionDataLoading;
  const errors = checkForErrors([exerciseError, motionDataError]);

  const procMotionData = useMemo(() => {
    return processMotionData(motionData, startTime, loading, errors);
  }, [motionData, startTime, loading, errors]);

  const graphs = useMemo(() => {
    return getGraphs(exercise);
  }, [exercise]);

  const [hoveredTime, setHoveredTime] = useState(null);

  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  if (loading) {
    return <></>;
  }

  if (
    !procMotionData ||
    (!graphs.includes("pace") && !graphs.includes("cadence"))
  ) {
    return <p className="message">No motion data found.</p>;
  }

  return (
    <div className="quadLayout">
      {graphs.includes("pace") && (
        <div className="motionGraphContainer">
          <MotionGraph
            data={procMotionData.speedData}
            yLabel={`Speed (${getUnit("speed")})`}
            type="speed"
            duration={milisecondsToMinutes(exercise.duration)}
            hoveredTime={hoveredTime}
            setHoveredTime={setHoveredTime}
            graphSizeInfo={graphSizeInfo}
          />
        </div>
      )}
      {graphs.includes("pace") && (
        <div className="motionGraphContainer">
          <MotionGraph
            data={procMotionData.paceData}
            yLabel={`Pace (${getUnit("pace")})`}
            type="pace"
            duration={milisecondsToMinutes(exercise.duration)}
            hoveredTime={hoveredTime}
            setHoveredTime={setHoveredTime}
            graphSizeInfo={graphSizeInfo}
          />
        </div>
      )}
      {graphs.includes("cadence") && (
        <div className="motionGraphContainer">
          <MotionGraph
            data={procMotionData.cadenceData}
            yLabel={`Cadence (${getUnit("cadence")})`}
            type="cadence"
            duration={milisecondsToMinutes(exercise.duration)}
            hoveredTime={hoveredTime}
            setHoveredTime={setHoveredTime}
            graphSizeInfo={graphSizeInfo}
          />
        </div>
      )}

      <MotionToolBar
        hoveredTime={hoveredTime}
        startTime={startTime}
        motionData={motionData}
      />
    </div>
  );
}

export default ExerciseMotion;
