import React from "react";
import Zones from "../../../../charts/heartRateGraphs/Zones";
import { remToPixels } from "../../../../utils/formatData";

const graphSizeInfo = {
  width: remToPixels(25),
  height: remToPixels(25),
  radius: remToPixels(12),
};

function inInterval(from, to, value) {
  return value >= from && value < to;
}

function getZoneSizes(heartRateZones, heartRateData) {
  const zoneSizes = [0, 0, 0, 0, 0];

  for (let i = 0; i < heartRateData.length; i++) {
    const record = heartRateData[i];
    if (
      inInterval(heartRateZones[0].min, heartRateZones[0].max, record.heartRate)
    ) {
      zoneSizes[0] += 1;
    } else if (
      inInterval(heartRateZones[1].min, heartRateZones[1].max, record.heartRate)
    ) {
      zoneSizes[1] += 1;
    } else if (
      inInterval(heartRateZones[2].min, heartRateZones[2].max, record.heartRate)
    ) {
      zoneSizes[2] += 1;
    } else if (
      inInterval(heartRateZones[3].min, heartRateZones[3].max, record.heartRate)
    ) {
      zoneSizes[3] += 1;
    } else if (
      inInterval(heartRateZones[4].min, heartRateZones[4].max, record.heartRate)
    ) {
      zoneSizes[4] += 1;
    }
  }

  return zoneSizes;
}

function genResZones(heartRateZones) {
  return heartRateZones.map((zone, index) => {
    const zoneId = `heartRateZone${index + 1}`;

    return (
      <div key={zoneId} className="heartRateZone" id={zoneId}>
        <div className="heartRateZoneColor"></div>
        <span>
          {zone.min}-{zone.max} b/m
        </span>
      </div>
    );
  });
}

function HeartRateZonesContainer({ heartRateZones, heartRateData }) {
  const zoneSizes = getZoneSizes(heartRateZones, heartRateData);
  const resZones = genResZones(heartRateZones);

  return (
    <div className="heartRateZonesContainer">
      {resZones}
      <Zones data={zoneSizes} sizeInfo={graphSizeInfo} />
    </div>
  );
}

export default HeartRateZonesContainer;
