import React, { useMemo } from "react";
import { useParams } from "react-router-dom";
import HeartRateGraphContainer from "./HeartRateGraphContainer";
import HeartRateZonesContainer from "./HeartRateZonesContainer";
import RecoveryHeartRateContainer from "./RecoveryHeartRateContainer";
import {
  getExercise,
  getExerciseMotionData,
  getExerciseLocationData,
  getExerciseHeartRateData,
  getExerciseRecoveryHeartRate,
} from "../../../../utils/api";
import { useQuery } from "react-query";
import checkForErrors from "../../../../utils/checkForErrors";
import ErrorPage from "../../errorPage/ErrorPage";
import {
  processSpeedData,
  processPaceData,
  processCadenceData,
  processAltitudeData,
  processHeartRateData,
  processRecoveryHeartRateData,
} from "../../../../utils/processData";
import { heartRateZones } from "../../../../constants/constants";

function processData(
  exercise,
  motionData,
  locationData,
  heartRateData,
  recoveryHeartRate,
  loading,
  errors,
) {
  if (
    loading ||
    errors.hasError ||
    !exercise ||
    !motionData ||
    !locationData ||
    !heartRateData ||
    !recoveryHeartRate
  )
    return;
  const startTime = new Date(exercise.startTime).getTime();
  const endTime = new Date(exercise.endTime).getTime();

  let res = {};
  res.heartRateData = processHeartRateData(heartRateData, startTime);
  res.recoveryHeartRateData = processRecoveryHeartRateData(
    recoveryHeartRate,
    endTime,
  );
  res.speedData = processSpeedData(motionData, startTime);
  res.paceData = processPaceData(motionData, startTime);
  res.cadenceData = processCadenceData(motionData, startTime);
  res.altitudeData = processAltitudeData(locationData, startTime);
  return res;
}

function ExerciseHeartRate() {
  const { exerciseId } = useParams();
  const {
    data: exercise,
    isLoading: exerciseLoading,
    error: exerciseError,
  } = useQuery(["exercise", exerciseId], () => getExercise(exerciseId));

  const {
    data: motionData,
    isLoading: motionDataLoading,
    error: motionDataError,
  } = useQuery(["exerciseMotionData", exerciseId], () =>
    getExerciseMotionData(exerciseId),
  );

  const {
    data: locationData,
    isLoading: locationDataLoading,
    error: locationDataError,
  } = useQuery(["exerciseLocationData", exerciseId], () =>
    getExerciseLocationData(exerciseId),
  );

  const {
    data: heartRateData,
    isLoading: heartRateDataLoading,
    error: heartRateDataError,
  } = useQuery(["exerciseHeartRateData", exerciseId], () =>
    getExerciseHeartRateData(exerciseId),
  );

  const {
    data: recoveryHeartRate,
    isLoading: recoveryHeartRateLoading,
    error: recoveryHeartRateError,
  } = useQuery(["exerciseRecoveryHeartRate", exerciseId], () =>
    getExerciseRecoveryHeartRate(exerciseId),
  );

  const loading =
    exerciseLoading ||
    motionDataLoading ||
    locationDataLoading ||
    heartRateDataLoading ||
    recoveryHeartRateLoading;
  const errors = checkForErrors([
    exerciseError,
    motionDataError,
    locationDataError,
    heartRateDataError,
    recoveryHeartRateError,
  ]);

  const procData = useMemo(
    () =>
      processData(
        exercise,
        motionData,
        locationData,
        heartRateData,
        recoveryHeartRate,
        loading,
        errors,
      ),
    [
      exercise,
      motionData,
      locationData,
      heartRateData,
      recoveryHeartRate,
      loading,
      errors,
    ],
  );
  const recoveryHeartRateData = procData
    ? procData.recoveryHeartRateData
    : null;

  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  if (loading) {
    return <></>;
  }

  console.log(procData);

  return (
    <div className="exerciseHeartRateLayout">
      <HeartRateGraphContainer
        exercise={exercise}
        data={procData}
        heartRateZones={heartRateZones}
      />

      <HeartRateZonesContainer
        heartRateZones={heartRateZones}
        heartRateData={heartRateData}
      />
      <RecoveryHeartRateContainer data={recoveryHeartRateData} />
    </div>
  );
}

export default ExerciseHeartRate;
