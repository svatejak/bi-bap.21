import React, { useMemo } from "react";

function getLegendItems(data) {
  return data.map((item) => {
    return (
      <div className="legendItem" key={item.name}>
        <div
          className="legendItemColor"
          style={{ backgroundColor: item.color }}
        ></div>
        <span>{item.name}</span>
      </div>
    );
  });
}

function GraphLegend({ data }) {
  const legendItems = useMemo(() => {
    return getLegendItems(data);
  }, [data]);

  return <div className="heartRateGraphLegend">{legendItems}</div>;
}

export default GraphLegend;
