import RecoveryHeartRate from "../../../../charts/heartRateGraphs/RecoveryHeartRate";
import { remToPixels } from "../../../../utils/formatData";
import React from "react";

const graphSizeInfo = {
  width: remToPixels(50),
  height: remToPixels(25),
  marginTop: remToPixels(4),
  marginRight: remToPixels(4),
  marginBottom: remToPixels(5),
  marginLeft: remToPixels(6),
};

function RecoveryHeartRateContainer({ data }) {
  if (data.length === 0) {
    return <></>;
  }

  return (
    <div className="recoveryHeartRateContainer">
      <div className="recoveryHeartRateContainerTitle">Recovery heart rate</div>
      <RecoveryHeartRate data={data} sizeInfo={graphSizeInfo} />
    </div>
  );
}

export default RecoveryHeartRateContainer;
