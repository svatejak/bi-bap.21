import React, { useState, useMemo } from "react";
import GraphTypeDropdown from "../../../common/GraphTypeDropdown";
import { remToPixels, getUnit, getColor } from "../../../../utils/formatData";
import Classic from "../../../../charts/heartRateGraphs/Classic";
import Speed from "../../../../charts/heartRateGraphs/Speed";
import Pace from "../../../../charts/heartRateGraphs/Pace";
import Cadence from "../../../../charts/heartRateGraphs/Cadence";
import Altitude from "../../../../charts/heartRateGraphs/Altitude";
import GraphLegend from "./GraphLegend";

const graphSizeInfo = {
  width: remToPixels(73),
  height: remToPixels(58),
  marginTop: remToPixels(6),
  marginRight: remToPixels(4),
  marginBottom: remToPixels(6),
  marginLeft: remToPixels(7),
};

function getOptions(exercise) {
  let resOptions = [{ value: "classic", label: "Classic" }];
  if (exercise.hasMotionData) {
    if (exercise.meanSpeed) {
      resOptions.push(
        { value: "speed", label: "Speed" },
        { value: "pace", label: "Pace" },
      );
    }
    if (exercise.meanCadence) {
      resOptions.push({ value: "cadence", label: "Cadence" });
    }
  }
  if (exercise.hasLocationData) {
    resOptions.push({ value: "altitude", label: "Altitude" });
  }
  return resOptions;
}

function getGraphContent(graphType, data, heartRateZones) {
  switch (graphType) {
    case "classic":
      return (
        <Classic
          data={data.heartRateData}
          sizeInfo={graphSizeInfo}
          heartRateZones={heartRateZones}
          yLabel={`Heart rate (${getUnit("heartRate")})`}
        />
      );
    case "speed":
      return (
        <Speed
          heartRateData={data.heartRateData}
          data={data.speedData}
          sizeInfo={graphSizeInfo}
          yLabel={`Speed (${getUnit("speed")})`}
        />
      );
    case "pace":
      return (
        <Pace
          heartRateData={data.heartRateData}
          data={data.paceData}
          sizeInfo={graphSizeInfo}
          yLabel={`Pace (${getUnit("pace")})`}
        />
      );
    case "cadence":
      return (
        <Cadence
          heartRateData={data.heartRateData}
          data={data.cadenceData}
          sizeInfo={graphSizeInfo}
          yLabel={`Cadence (${getUnit("cadence")})`}
        />
      );
    case "altitude":
      return (
        <Altitude
          heartRateData={data.heartRateData}
          data={data.altitudeData}
          sizeInfo={graphSizeInfo}
          yLabel={`Altitude (${getUnit("altitude")})`}
        />
      );
    default:
      return null;
  }
}

function getGraphLines(graphType) {
  switch (graphType) {
    case "classic":
      return [{ name: "Heart rate", color: getColor("primary-color") }];
    case "speed":
      return [
        { name: "Heart rate", color: "red" },
        { name: "Speed", color: getColor("secondary-color") },
      ];
    case "pace":
      return [
        { name: "Heart rate", color: "red" },
        { name: "Pace", color: getColor("secondary-color") },
      ];
    case "cadence":
      return [
        { name: "Heart rate", color: "red" },
        { name: "Cadence", color: getColor("secondary-color") },
      ];
    case "altitude":
      return [
        { name: "Heart rate", color: "red" },
        { name: "Altitude", color: getColor("secondary-color") },
      ];
    default:
      return [];
  }
}

function HeartRateGraphContainer({ exercise, data, heartRateZones }) {
  const [graphType, setGraphType] = useState("classic");

  const graphContent = useMemo(() => {
    return getGraphContent(graphType, data, heartRateZones);
  }, [graphType, data, heartRateZones]);
  const graphLines = useMemo(() => {
    return getGraphLines(graphType);
  }, [graphType]);
  const dropDownOptions = getOptions(exercise);

  return (
    <div className="heartRateGraphContainer">
      <GraphTypeDropdown
        graphType={graphType}
        setGraphType={setGraphType}
        options={dropDownOptions}
      />
      <GraphLegend data={graphLines} />
      <div className="heartRateGraph">{graphContent}</div>
    </div>
  );
}

export default HeartRateGraphContainer;
