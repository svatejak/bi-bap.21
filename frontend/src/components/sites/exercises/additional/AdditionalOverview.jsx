import React from "react";

function getItemTitle(dataType) {
  switch (dataType) {
    case "asymmetry":
      return "Asymmetry";
    case "regularity":
      return "Regularity";
    case "stiffness":
      return "Stiffness";
    case "undulation":
      return "Undulation";
    case "effective_contact_time":
      return "Contact time";
    case "effective_flight_time":
      return "Flight time";
    default:
      return "";
  }
}

function AdditionalOverview({ additionalData }) {
  return (
    <div className="additionalOverview">
      {additionalData.map((data) => (
        <div className="additionalOverviewItem" key={data.dataType}>
          <div className="additionalOverviewItemTitle">
            {getItemTitle(data.dataType)}
          </div>
          <div className="additionalOverviewItemContent">
            {[...Array(3).keys()].map((_, i) => (
              <div
                key={`zone ${i}`}
                className={`contentPart ${i === data.score ? "active" : ""}`}
              ></div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}

export default AdditionalOverview;
