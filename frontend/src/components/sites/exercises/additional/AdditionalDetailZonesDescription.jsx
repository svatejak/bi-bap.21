import React from "react";
import { getColor } from "../../../../utils/formatData";

function AdditionalDetailZonesDescription() {
  return (
    <div className="zonesDescription">
      <div className="zonesDescriptionItem">
        <div
          className="zonesDescriptionColor"
          style={{ backgroundColor: getColor("additional-color1") }}
        ></div>
        <span>Great</span>
      </div>
      <div className="zonesDescriptionItem">
        <div
          className="zonesDescriptionColor"
          style={{ backgroundColor: getColor("additional-color2") }}
        ></div>
        <span>Good</span>
      </div>
      <div className="zonesDescriptionItem">
        <div
          className="zonesDescriptionColor"
          style={{ backgroundColor: getColor("additional-color3") }}
        ></div>
        <span>Improve</span>
      </div>
    </div>
  );
}

export default AdditionalDetailZonesDescription;
