import React, { useState, useMemo } from "react";
import { useParams } from "react-router-dom";

import AdditionalGraphContainer from "./AdditionalGraphContainer";
import AdditionalDetail from "./AdditionalDetail";
import AdditionalOverview from "./AdditionalOverview";
import {
  processSpecAdditionalData,
  processStiffnessData,
  processUndulationData,
} from "../../../../utils/processData";

import { getExercise, getExerciseAdditionalData } from "../../../../utils/api";
import { useQuery } from "react-query";
import ErrorPage from "../../errorPage/ErrorPage";
import checkForErrors from "../../../../utils/checkForErrors";

function processAdditionalData(additionalData, loading, errors) {
  if (
    loading ||
    errors.hasError ||
    !additionalData ||
    additionalData.length === 0
  )
    return;

  let res = {};
  res.asymmetryData = processSpecAdditionalData(
    additionalData.find((d) => d.dataType === "asymmetry").records,
  );
  res.regularityData = processSpecAdditionalData(
    additionalData.find((d) => d.dataType === "regularity").records,
  );
  res.stiffnessData = processStiffnessData(
    additionalData.find((d) => d.dataType === "stiffness").records,
  );
  res.undulationData = processUndulationData(
    additionalData.find((d) => d.dataType === "undulation").records,
  );
  res.effectiveContactTimeData = processSpecAdditionalData(
    additionalData.find((d) => d.dataType === "effective_contact_time").records,
  );
  res.effectiveFlightTimeData = processSpecAdditionalData(
    additionalData.find((d) => d.dataType === "effective_flight_time").records,
  );
  return res;
}

function ExerciseAdditional() {
  const { exerciseId } = useParams();
  const { isLoading: exerciseLoading, error: exerciseError } = useQuery(
    ["exercise", exerciseId],
    () => getExercise(exerciseId),
  );
  const {
    data: additionalData,
    isLoading: additionalDataLoading,
    error: additionalDataError,
  } = useQuery(["exerciseadditionalData", exerciseId], () =>
    getExerciseAdditionalData(exerciseId),
  );

  const [graphType, setGraphType] = useState("asymmetry");

  const loading = exerciseLoading || additionalDataLoading;
  const errors = checkForErrors([exerciseError, additionalDataError]);

  const procAddData = useMemo(
    () => processAdditionalData(additionalData, loading, errors),
    [additionalData, loading, errors],
  );

  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  if (loading) {
    return <></>;
  }

  if (!procAddData) {
    return <p className="message">No additional data found.</p>;
  }

  return (
    <div className="additionalLayout">
      <AdditionalGraphContainer
        graphType={graphType}
        setGraphType={setGraphType}
        data={procAddData}
      />

      <AdditionalOverview additionalData={additionalData} />
      <AdditionalDetail
        graphType={graphType}
        data={procAddData}
        additionalData={additionalData}
      />
    </div>
  );
}

export default ExerciseAdditional;
