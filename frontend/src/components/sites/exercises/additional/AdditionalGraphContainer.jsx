import React, { useMemo } from "react";
import GraphTypeDropdown from "../../../common/GraphTypeDropdown";
import Asymmetry from "../../../../charts/additionalGraphs/Asymmetry";
import ContactTime from "../../../../charts/additionalGraphs/ContactTime";
import FlightTime from "../../../../charts/additionalGraphs/FlightTime";
import Regularity from "../../../../charts/additionalGraphs/Regularity";
import Stiffness from "../../../../charts/additionalGraphs/Stiffness";
import Undulation from "../../../../charts/additionalGraphs/Undulation";
import { remToPixels, getUnit } from "../../../../utils/formatData";

const dropDownOptions = [
  { value: "asymmetry", label: "Asymmetry" },
  { value: "regularity", label: "Regularity" },
  { value: "stiffness", label: "Stiffness" },
  { value: "undulation", label: "Undulation" },
  { value: "contactTime", label: "Contact time" },
  { value: "flightTime", label: "Flight time" },
];

const graphSizeInfo = {
  width: remToPixels(75),
  height: remToPixels(55),
  marginTop: remToPixels(6),
  marginRight: remToPixels(4),
  marginBottom: remToPixels(5),
  marginLeft: remToPixels(8),
};

function getGraphContent(graphType, data) {
  switch (graphType) {
    case "asymmetry":
      return (
        <Asymmetry
          data={data.asymmetryData}
          sizeInfo={graphSizeInfo}
          yLabel={"Asymmetry"}
        />
      );
    case "regularity":
      return (
        <Regularity
          data={data.regularityData}
          sizeInfo={graphSizeInfo}
          yLabel={"Regularity"}
        />
      );
    case "stiffness":
      return (
        <Stiffness
          data={data.stiffnessData}
          sizeInfo={graphSizeInfo}
          yLabel={`Stiffness (${getUnit("stiffness")})`}
        />
      );
    case "undulation":
      return (
        <Undulation
          data={data.undulationData}
          sizeInfo={graphSizeInfo}
          yLabel={`Undulation (${getUnit("undulation")})`}
        />
      );
    case "contactTime":
      return (
        <ContactTime
          data={data.effectiveContactTimeData}
          sizeInfo={graphSizeInfo}
          yLabel={`Contact time (${getUnit("time")})`}
        />
      );
    case "flightTime":
      return (
        <FlightTime
          data={data.effectiveFlightTimeData}
          sizeInfo={graphSizeInfo}
          yLabel={`Flight time (${getUnit("time")})`}
        />
      );
    default:
      return null;
  }
}

function AdditionalGraphContainer({ graphType, setGraphType, data }) {
  const graphContent = useMemo(() => {
    return getGraphContent(graphType, data);
  }, [graphType, data]);

  return (
    <div className="additionalGraphContainer">
      <GraphTypeDropdown
        graphType={graphType}
        setGraphType={setGraphType}
        options={dropDownOptions}
      />
      <div className="additionalGraph">{graphContent}</div>
    </div>
  );
}

export default AdditionalGraphContainer;
