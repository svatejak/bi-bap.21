import Zones from "../../../../charts/additionalGraphs/Zones";
import { useMemo } from "react";
import AdditionalDetailScore from "./AdditionalDetailScore";
import AdditionalDetailAverage from "./AdditionalDetailAverage";
import AdditionalDetailZonesDescription from "./AdditionalDetailZonesDescription";
import AdditionalDetailTitle from "./AdditionalDetailTitle";
import { remToPixels } from "../../../../utils/formatData";

const graphSizeInfo = {
  width: remToPixels(25),
  height: remToPixels(25),
  radius: remToPixels(9),
};

function getZoneSizes(records) {
  const zoneSizes = [0, 0, 0];

  const filteredRecords = records.filter((record) => record.score !== 3);
  for (let i = 0; i < filteredRecords.length; i++) {
    zoneSizes[filteredRecords[i].score] += 1;
  }
  return zoneSizes;
}

function getTypeData(graphType, data) {
  switch (graphType) {
    case "asymmetry":
      return data.asymmetryData;
    case "regularity":
      return data.regularityData;
    case "stiffness":
      return data.stiffnessData;
    case "undulation":
      return data.undulationData;
    case "contactTime":
      return data.effectiveContactTimeData;
    case "flightTime":
      return data.effectiveFlightTimeData;
    default:
      return null;
  }
}

function AdditionalDetail({ graphType, data, additionalData }) {
  const typeData = useMemo(() => {
    return getTypeData(graphType, data);
  }, [graphType, data]);
  const zoneSizes = useMemo(() => getZoneSizes(typeData), [typeData]);

  return (
    <div className="additionalDetail">
      <AdditionalDetailTitle graphType={graphType} />
      <AdditionalDetailScore
        graphType={graphType}
        additionalData={additionalData}
      />
      <AdditionalDetailAverage data={typeData} graphType={graphType} />
      <Zones data={zoneSizes} sizeInfo={graphSizeInfo} />
      <AdditionalDetailZonesDescription />
    </div>
  );
}

export default AdditionalDetail;
