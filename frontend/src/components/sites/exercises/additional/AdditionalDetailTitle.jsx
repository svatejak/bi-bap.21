function getName(graphType) {
  switch (graphType) {
    case "asymmetry":
      return "Asymmetry";
    case "regularity":
      return "Regularity";
    case "stiffness":
      return "Stiffness";
    case "undulation":
      return "Undulation";
    case "contactTime":
      return "Contact time";
    case "flightTime":
      return "Flight time";
    default:
      return "";
  }
}

function AdditionalDetailTitle({ graphType }) {
  const detailName = getName(graphType);

  return <div className="additionalDetailTitle">{detailName}</div>;
}

export default AdditionalDetailTitle;
