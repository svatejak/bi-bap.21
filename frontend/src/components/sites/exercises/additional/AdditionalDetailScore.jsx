import React from "react";
import { getColor } from "../../../../utils/formatData";

function AdditionalDetailScore({ graphType, additionalData }) {
  let dataType = graphType;
  if (graphType === "contactTime") dataType = "effective_contact_time";
  else if (graphType === "flightTime") dataType = "effective_flight_time";

  const score = additionalData.find(
    (record) => record.dataType === dataType,
  ).score;
  let text = "";
  let color = "";

  switch (score) {
    case 0:
      text = "Improve";
      color = getColor("additional-color3");
      break;
    case 1:
      text = "Good";
      color = getColor("additional-color2");
      break;
    case 2:
      text = "Great";
      color = getColor("additional-color1");
      break;
    default:
      return <div className="additionalDetailScore"></div>;
  }

  return (
    <div className="additionalDetailScore" style={{ color: color }}>
      {text}
    </div>
  );
}

export default AdditionalDetailScore;
