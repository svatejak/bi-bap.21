import React from "react";
import { roundNumberToDecimalPlaces } from "../../../../utils/formatData";
import { getUnit } from "../../../../utils/formatData";

function getAverage(data) {
  const filteredRecords = data.filter((record) => record.score !== 3);
  if (filteredRecords.length > 0) {
    const sum = filteredRecords.reduce(
      (accumulator, record) => accumulator + record.yAxis,
      0,
    );
    return sum / filteredRecords.length;
  }

  return null;
}

function AdditionalDetailAverage({ data, graphType }) {
  let average = getAverage(data);

  if (!average) return <div className="additionalDetailAverage"></div>;

  let unit = "";
  switch (graphType) {
    case "asymmetry":
      average = roundNumberToDecimalPlaces(average, 1);
      if (average === 0)
        return <div className="additionalDetailAverage">Ø {average} %</div>;
      else if (average > 0)
        return <div className="additionalDetailAverage">Ø R {average} %</div>;
      else
        return <div className="additionalDetailAverage">Ø L {-average} %</div>;
    case "regularity":
      average = roundNumberToDecimalPlaces(average, 2);
      break;
    case "stiffness":
      average = roundNumberToDecimalPlaces(average, 0);
      unit = getUnit("stiffness");
      break;
    case "undulation":
      average = roundNumberToDecimalPlaces(average, 1);
      unit = getUnit("undulation");
      break;
    case "contactTime":
    case "flightTime":
      average = roundNumberToDecimalPlaces(average, 0);
      unit = getUnit("time");
      break;
    default:
      return <div className="additionalDetailAverage"></div>;
  }

  if (graphType === "stiffness") {
    average = roundNumberToDecimalPlaces(average, 2);
  }

  return (
    <div className="additionalDetailAverage">
      Ø {average} {unit}
    </div>
  );
}

export default AdditionalDetailAverage;
