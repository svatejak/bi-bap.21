import React from "react";
import { useParams } from "react-router-dom";
import { getExercise } from "../../../../utils/api";

import { useQuery } from "react-query";
import ErrorPage from "../../errorPage/ErrorPage";
import checkForErrors from "../../../../utils/checkForErrors";

import OverviewDataSection from "./OverviewDataSection";
import OverviewGraphSection from "./OverviewGraphSection";

function ExerciseOverview() {
  const { exerciseId } = useParams();
  const {
    data: exercise,
    isLoading: exerciseLoading,
    error: exerciseError,
  } = useQuery(["exercise", exerciseId], () => getExercise(exerciseId));

  const loading = exerciseLoading;
  const errors = checkForErrors([exerciseError]);

  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  if (loading) {
    return <></>;
  }

  return (
    <div className="exerciseOverviewLayout">
      <OverviewDataSection exercise={exercise} />
      <OverviewGraphSection exercise={exercise} />
    </div>
  );
}

export default ExerciseOverview;
