import DataSectionWrapper from "./DataSectionWrapper";
import DataSectionHeader from "./DataSectionHeader";

function OverviewDataSection({ exercise }) {
  return (
    <div className="dataSectionLayout">
      <DataSectionHeader exercise={exercise} />
      <DataSectionWrapper exercise={exercise} />
    </div>
  );
}

export default OverviewDataSection;
