import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { getExerciseMotionData } from "../../../../utils/api";
import checkForErrors from "../../../../utils/checkForErrors";
import { useMemo } from "react";
import { processSpeedData } from "../../../../utils/processData";
import { remToPixels } from "../../../../utils/formatData";
import SpeedGraph from "../../../../charts/SpeedGraph";
import { getUnit } from "../../../../utils/formatData";

function MotionSection({ exercise, size }) {
  const graphSizeInfo = {
    width: size.width,
    height: size.height,
    marginTop: remToPixels(5),
    marginRight: remToPixels(4),
    marginBottom: remToPixels(5),
    marginLeft: remToPixels(4),
  };

  const { exerciseId } = useParams();
  const {
    data: motionData,
    isLoading: motionDataLoading,
    error: motionDataError,
  } = useQuery(["exerciseMotionData", exerciseId], () =>
    getExerciseMotionData(exerciseId),
  );

  const loading = motionDataLoading;
  const errors = checkForErrors([motionDataError]);

  const procMotionData = useMemo(() => {
    if (
      loading ||
      errors.hasError ||
      !motionData ||
      motionData.length === 0 ||
      !exercise
    )
      return;
    const startTime = new Date(exercise.startTime).getTime();
    return processSpeedData(motionData, startTime);
  }, [motionData, exercise, loading, errors]);

  if (errors.hasError || loading) {
    return <div className="section motionSection" />;
  }

  return (
    <div className="section motionSection">
      <div className="sectionHeader">Speed</div>
      <SpeedGraph
        data={procMotionData}
        yLabel={`Speed (${getUnit("speed")})`}
        graphSizeInfo={graphSizeInfo}
      />
    </div>
  );
}

export default MotionSection;
