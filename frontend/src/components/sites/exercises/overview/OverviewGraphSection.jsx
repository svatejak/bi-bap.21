import MotionSection from "./MotionSection";
import MapSection from "./MapSection";
import HeartRateSection from "./HeartRateSection";
import AdditionalOverviewSection from "./AdditionalOverviewSection";
import { remToPixels } from "../../../../utils/formatData";

const smallSectionWithHeader = {
  width: remToPixels(27.3),
  height: remToPixels(26.3),
};

const smallSectionWithoutHeader = {
  width: remToPixels(31),
  height: remToPixels(31),
};

const bigSection = {
  width: remToPixels(66),
  height: remToPixels(66),
};

function OverviewGraphSection({ exercise }) {
  if (!exercise.hasHeartRateData) {
    return <div></div>;
  }

  if (exercise.hasLocationData || exercise.hasMotionData) {
    return (
      <div className="graphSectionLayout1">
        {exercise.hasMotionData && (
          <MotionSection exercise={exercise} size={smallSectionWithHeader} />
        )}
        {exercise.hasLocationData && (
          <MapSection exercise={exercise} size={smallSectionWithoutHeader} />
        )}
        {exercise.hasHeartRateData && (
          <HeartRateSection exercise={exercise} size={smallSectionWithHeader} />
        )}
        {exercise.hasAdditionalMetrics && (
          <AdditionalOverviewSection exercise={exercise} />
        )}
      </div>
    );
  }

  return (
    <div className="graphSectionLayout2">
      {exercise.hasHeartRateData && (
        <HeartRateSection exercise={exercise} size={bigSection} />
      )}
    </div>
  );
}

export default OverviewGraphSection;
