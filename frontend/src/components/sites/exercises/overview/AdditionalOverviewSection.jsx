import React from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import { getExerciseAdditionalData } from "../../../../utils/api";
import checkForErrors from "../../../../utils/checkForErrors";
import AdditionalOverview from "../additional/AdditionalOverview";

function AdditionalOverviewSection() {
  const { exerciseId } = useParams();
  const {
    data: additionalData,
    isLoading: additionalDataLoading,
    error: additionalDataError,
  } = useQuery(["exerciseadditionalData", exerciseId], () =>
    getExerciseAdditionalData(exerciseId),
  );

  const loading = additionalDataLoading;
  const errors = checkForErrors([additionalDataError]);

  if (errors.hasError || loading) {
    return <div className="section additionalOverviewSection"></div>;
  }

  return (
    <div className="section additionalOverviewSection">
      <div className="sectionHeader">Additional data</div>
      <div className="sectionContent">
        <AdditionalOverview additionalData={additionalData} />
      </div>
    </div>
  );
}

export default AdditionalOverviewSection;
