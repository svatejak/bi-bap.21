import React, { useMemo } from "react";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import checkForErrors from "../../../../utils/checkForErrors";
import { processHeartRateData } from "../../../../utils/processData";
import { getUnit } from "../../../../utils/formatData";
import Classic from "../../../../charts/heartRateGraphs/Classic";

import { getExerciseHeartRateData } from "../../../../utils/api";
import { heartRateZones } from "../../../../constants/constants";
import { remToPixels } from "../../../../utils/formatData";

function HeartRateSection({ exercise, size }) {
  const graphSizeInfo = {
    width: size.width,
    height: size.height,
    marginTop: remToPixels(6),
    marginRight: remToPixels(4),
    marginBottom: remToPixels(5),
    marginLeft: remToPixels(6),
  };

  const { exerciseId } = useParams();
  const {
    data: heartRateData,
    isLoading: heartRateDataIsLoading,
    error: heartRateDataError,
  } = useQuery(["heartRateData", exerciseId], () =>
    getExerciseHeartRateData(exerciseId),
  );

  const loading = heartRateDataIsLoading;
  const errors = checkForErrors([heartRateDataError]);

  const processedHeartRateData = useMemo(() => {
    if (
      loading ||
      errors.hasError ||
      !heartRateData ||
      !exercise ||
      heartRateData.length === 0
    )
      return;
    const startTime = new Date(exercise.startTime).getTime();
    return processHeartRateData(heartRateData, startTime);
  }, [heartRateData, exercise, loading, errors]);

  if (errors.hasError || loading) {
    return <div className="section heartRateSection"></div>;
  }

  return (
    <div className="section heartRateSection">
      <div className="sectionHeader">Heart rate</div>
      <Classic
        data={processedHeartRateData}
        sizeInfo={graphSizeInfo}
        heartRateZones={heartRateZones}
        yLabel={`Heart rate (${getUnit("heartRate")})`}
      />
    </div>
  );
}

export default HeartRateSection;
