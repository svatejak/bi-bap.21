import React from "react";
import {
  remToPixels,
  formatDate,
  formatDataSource,
} from "../../../../utils/formatData";

function DataSectionHeader({ exercise }) {
  console.log("exercise: ", exercise);
  const datasource = formatDataSource(exercise.dataSource);
  return (
    <div className="dataSectionHeader">
      <img
        className="exerciseIcon"
        src={exercise.icon}
        alt="exercise"
        height={remToPixels(6)}
        width={remToPixels(6)}
        title="exercise logo"
      />
      <span className="exerciseName">{exercise.exerciseName}</span>
      <span className="exerciseTime">{formatDate(exercise.startTime)}</span>
      <span className="exerciseSource">{datasource}</span>
    </div>
  );
}

export default DataSectionHeader;
