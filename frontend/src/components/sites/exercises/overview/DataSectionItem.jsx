function DataSectionItem({ item }) {
  return (
    <div className="dataSectionItem">
      <div className="itemHeader">{item.header}</div>
      <div className="itemContent">
        {item.content.map((row) => (
          <div className="itemRow" key={row.header + " " + row.data}>
            {row.header && <div className="itemRowHeader">{row.header}</div>}
            <div className="itemRowData">
              {row.data + " " + item.unit ?? ""}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default DataSectionItem;
