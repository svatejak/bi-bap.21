import Classic from "../../../../maps/exercise/Classic";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import checkForErrors from "../../../../utils/checkForErrors";
import { getExerciseLocationData } from "../../../../utils/api";
import { useMemo } from "react";
import { processLocationData } from "../../../../utils/processData";

function MapSection({ exercise, size }) {
  const mapSize = size;
  const { exerciseId } = useParams();
  const {
    data: locationData,
    isLoading: locationDataLoading,
    error: locationDataError,
  } = useQuery(["exerciseLocationData", exerciseId], () =>
    getExerciseLocationData(exerciseId),
  );

  const loading = locationDataLoading;
  const errors = checkForErrors([locationDataError]);

  const processedLocationData = useMemo(() => {
    if (
      loading ||
      errors.hasError ||
      !locationData ||
      !exercise ||
      locationData.length === 0
    )
      return;
    const startTime = new Date(exercise.startTime).getTime();
    return processLocationData(locationData, startTime);
  }, [locationData, exercise, loading, errors]);

  if (errors.hasError || loading) {
    return <div className="section mapSection" />;
  }

  console.log("processedLocationData", processedLocationData);

  return (
    <div className="section mapSection">
      <Classic locationData={processedLocationData} mapSize={mapSize} />
    </div>
  );
}

export default MapSection;
