import DataSectionItem from "./DataSectionItem";
import {
  formatDuration,
  convertDistance,
  getUnit,
} from "../../../../utils/formatData";
import { convertSpeed, truncateDecimals } from "../../../../utils/formatData";
import { convertSpeedToPace, formatPace } from "../../../../utils/formatData";

function getItems(exercise) {
  const column1 = [];
  const column2 = [];

  let bigItemsCount1 = 0;
  let bigItemsCount2 = 0;
  let smallItemsCount1 = 0;
  let smallItemsCount2 = 0;

  const addBigItem = (item) => {
    if (bigItemsCount1 <= bigItemsCount2) {
      column1.push(item);
      bigItemsCount1++;
    } else {
      column2.push(item);
      bigItemsCount2++;
    }
  };

  const addSmallItem = (item) => {
    if (
      smallItemsCount1 < smallItemsCount2 ||
      (smallItemsCount1 === smallItemsCount2 &&
        bigItemsCount1 <= bigItemsCount2)
    ) {
      column1.push(item);
      smallItemsCount1++;
    } else {
      column2.push(item);
      smallItemsCount2++;
    }
  };

  if (exercise.duration) {
    addSmallItem({
      header: "Duration",
      unit: "",
      content: [
        {
          header: "",
          data: formatDuration(exercise.duration),
        },
      ],
    });
  }

  if (exercise.distance) {
    addSmallItem({
      header: "Distance",
      unit: getUnit("distance"),
      content: [
        {
          header: "",
          data: convertDistance(exercise.distance),
        },
      ],
    });
  }

  if (exercise.totalCalories) {
    addSmallItem({
      header: "Calories",
      unit: getUnit("calories"),
      content: [
        {
          header: "",
          data: truncateDecimals(exercise.totalCalories, 0),
        },
      ],
    });
  }

  if (exercise.maxSpeed || exercise.meanSpeed) {
    addBigItem({
      header: "Speed",
      unit: getUnit("speed"),
      content: [
        exercise.maxSpeed
          ? {
              header: "Max speed",
              data: convertSpeed(exercise.maxSpeed),
            }
          : null,
        exercise.meanSpeed
          ? {
              header: "Mean speed",
              data: convertSpeed(exercise.meanSpeed),
            }
          : null,
      ].filter((item) => item !== null),
    });

    addBigItem({
      header: "Pace",
      unit: getUnit("paceShort"),
      content: [
        exercise.maxSpeed
          ? {
              header: "Max pace",
              data: formatPace(convertSpeedToPace(exercise.maxSpeed, "s/km")),
            }
          : null,
        exercise.meanSpeed
          ? {
              header: "Mean pace",
              data: formatPace(convertSpeedToPace(exercise.meanSpeed, "s/km")),
            }
          : null,
      ].filter((item) => item !== null),
    });
  }

  if (exercise.inclineDistance || exercise.declineDistance) {
    addBigItem({
      header: "Slope",
      unit: getUnit("altitude"),
      content: [
        exercise.inclineDistance
          ? {
              header: "Incline distance",
              data: truncateDecimals(exercise.inclineDistance, 0),
            }
          : null,
        exercise.declineDistance
          ? {
              header: "Decline distance",
              data: truncateDecimals(exercise.declineDistance, 0),
            }
          : null,
      ].filter((item) => item !== null),
    });
  }

  if (exercise.maxAltitude || exercise.minAltitude) {
    addBigItem({
      header: "Altitude",
      unit: getUnit("altitude"),
      content: [
        exercise.maxAltitude
          ? {
              header: "Max altitude",
              data: truncateDecimals(exercise.maxAltitude, 0),
            }
          : null,
        exercise.minAltitude
          ? {
              header: "Min altitude",
              data: truncateDecimals(exercise.minAltitude, 0),
            }
          : null,
      ].filter((item) => item !== null),
    });
  }

  if (exercise.maxHeartRate || exercise.minHeartRate) {
    addBigItem({
      header: "Heart rate",
      unit: getUnit("heartRate"),
      content: [
        exercise.maxHeartRate
          ? {
              header: "Max heart rate",
              data: truncateDecimals(exercise.maxHeartRate, 0),
            }
          : null,
        exercise.meanHeartRate
          ? {
              header: "Mean heart rate",
              data: truncateDecimals(exercise.meanHeartRate, 0),
            }
          : null,
      ].filter((item) => item !== null),
    });
  }

  if (exercise.maxCadence || exercise.meanCadence) {
    addBigItem({
      header: "Cadence",
      unit: getUnit("cadence"),
      content: [
        exercise.maxCadence
          ? {
              header: "Max cadence",
              data: truncateDecimals(exercise.maxCadence, 0),
            }
          : null,
        exercise.meanCadence
          ? {
              header: "Mean cadence",
              data: truncateDecimals(exercise.meanCadence, 0),
            }
          : null,
      ].filter((item) => item !== null),
    });
  }

  if (exercise.stepCount) {
    addSmallItem({
      header: "Steps",
      unit: "",
      content: [
        {
          header: "",
          data: exercise.stepCount,
        },
      ],
    });
  }

  return [column1, column2];
}

function DataSectionWrapper({ exercise }) {
  const [column1, column2] = getItems(exercise);

  return (
    <div className="dataSectionWrapper">
      <div className="column">
        {column1.map((item) => {
          return <DataSectionItem key={item.header} item={item} />;
        })}
      </div>

      <div className="column">
        {column2.map((item) => {
          return <DataSectionItem key={item.header} item={item} />;
        })}
      </div>
    </div>
  );
}

export default DataSectionWrapper;
