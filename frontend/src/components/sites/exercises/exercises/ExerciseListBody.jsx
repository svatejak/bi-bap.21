import React from "react";
import { formatDate, remToPixels } from "../../../../utils/formatData";
import { useNavigate } from "react-router-dom";

function ExerciseListBody({ exercises, loading }) {
  const navigate = useNavigate();

  const handleExerciseClick = (id) => {
    navigate(`/exercises/${id}`);
  };

  return (
    <div className="exerciseListBody">
      {!loading &&
        exercises.map((exercise) => (
          <div
            className="exerciseItem"
            key={exercise.id}
            onClick={() => handleExerciseClick(exercise.id)}
          >
            <img
              className="exerciseIcon"
              src={exercise.icon}
              alt="exercise"
              height={remToPixels(6)}
              width={remToPixels(6)}
            />
            <span className="exerciseName">{exercise.exerciseName}</span>
            <span className="exerciseTime">
              {formatDate(exercise.startTime)}
            </span>
            <span className="exerciseSource">{exercise.datasource}</span>
          </div>
        ))}
    </div>
  );
}

export default ExerciseListBody;
