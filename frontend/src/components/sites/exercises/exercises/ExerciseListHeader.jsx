function ExerciseListHeader({ activeSort, setActiveSort, setFilter }) {
  const handleSort = (sortName) => {
    const newDirection =
      activeSort.name === sortName && activeSort.direction === "asc"
        ? "desc"
        : "asc";
    setActiveSort({ name: sortName, direction: newDirection });
  };

  const getArrowClasses = (sortName, direction) => {
    const isVisible = activeSort.name === sortName ? "visible" : "";
    const isActive =
      activeSort.name === sortName && activeSort.direction === direction
        ? "active"
        : "";
    return `${isVisible} ${isActive}`;
  };

  const handleFilter = (value) => {
    if (value === "Source") setFilter("");
    else setFilter(value);
  };

  return (
    <div className="exerciseListHeader">
      <button className="sortButton" onClick={() => handleSort("date")}>
        <span>Date</span>
        <div className={`arrowUp ${getArrowClasses("date", "asc")}`} />
        <div className={`arrowDown ${getArrowClasses("date", "desc")}`} />
      </button>
      <button className="sortButton" onClick={() => handleSort("exercise")}>
        <span>Exercise</span>
        <div className={`arrowUp ${getArrowClasses("exercise", "asc")}`} />
        <div className={`arrowDown ${getArrowClasses("exercise", "desc")}`} />
      </button>
      <select
        className="selectButton"
        defaultValue="Source"
        onChange={(e) => handleFilter(e.target.value)}
      >
        <option value="Source">Source</option>
        <option value="GoogleFit">Google Fit</option>
        <option value="SamsungHealth">Samsung Health</option>
      </select>
    </div>
  );
}

export default ExerciseListHeader;
