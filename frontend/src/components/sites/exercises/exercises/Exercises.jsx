import React, { useState } from "react";
import { getExercises, getExercise } from "../../../../utils/api";
import ExerciseSummary from "./ExerciseSummary";
import ExerciseListHeader from "./ExerciseListHeader";
import ExerciseListBody from "./ExerciseListBody";
import { useParams } from "react-router-dom";
import { useQuery } from "react-query";
import ErrorPage from "../../errorPage/ErrorPage";
import checkForErrors from "../../../../utils/checkForErrors";

function getSortBy(activeSort) {
  switch (activeSort.name) {
    case "date":
      return "start_time";
    case "exercise":
      return "exercise_name";
    default:
      return "";
  }
}

export default function Exercises() {
  const { exerciseId } = useParams();
  const { data: exercise, error: exerciseError } = useQuery(
    ["exercise", exerciseId],
    () => getExercise(exerciseId),
    { enabled: !!exerciseId },
  );

  const [activeSort, setActiveSort] = useState({
    name: "date",
    direction: "asc",
  });
  const [filter, setFilter] = useState("");

  const sortBy = getSortBy(activeSort);
  const order = activeSort.direction;
  const {
    data: exercises,
    isLoading: exercisesLoading,
    error: exercisesError,
  } = useQuery(["exercises", sortBy, order, filter], () =>
    getExercises({ sort_by: sortBy, order: order, filter: filter }),
  );

  const errors = checkForErrors([exercisesError, exerciseError]);
  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  const exercisesData = exercisesLoading ? [] : exercises.exercises;
  if (filter === "" && !exercisesLoading && exercisesData.length === 0) {
    return <p className="message">No exercises found.</p>;
  }

  return (
    <div className="defaultLayout">
      <div className="exerciseList">
        <ExerciseListHeader
          activeSort={activeSort}
          setActiveSort={setActiveSort}
          setFilter={setFilter}
        />
        <ExerciseListBody
          exercises={exercisesData}
          loading={exercisesLoading}
        />
      </div>

      <ExerciseSummary exercise={exercise} />
    </div>
  );
}
