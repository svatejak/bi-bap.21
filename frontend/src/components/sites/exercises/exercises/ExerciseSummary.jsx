import React, { useMemo } from "react";
import {
  getUnit,
  formatDuration,
  truncateDecimals,
  convertDistance,
  convertSpeed,
  formatDateInterval,
  convertSpeedToPace,
  formatPace,
} from "../../../../utils/formatData";
import moreInfoIcon from "../../../../assets/moreInfo.png";
import { Link } from "react-router-dom";

function generateInfoboxes(exercise) {
  if (!exercise) {
    return [];
  }

  const boxes = [];

  function addBox(title, value, units = "") {
    boxes.push({ title, value, units });
  }

  if (exercise.subsetData && exercise.subsetData.length > 0) {
    addBox("Sets", exercise.subsetData.length);
    const hasReps = exercise.subsetData.some((data) => data.reps !== null);
    if (hasReps) {
      const totalReps = exercise.subsetData.reduce(
        (acc, data) => acc + (data.reps || 0),
        0,
      );
      addBox("Reps", totalReps);
    }
    const hasWeight = exercise.subsetData.some((data) => data.weight !== null);
    if (hasWeight) {
      addBox("Weight", exercise.subsetData[0].weight, getUnit("weight"));
    }
  }

  if (exercise.duration !== null) {
    const duration = formatDuration(exercise.duration);
    addBox("Duration", duration);
  }

  if (exercise.meanSpeed !== null) {
    addBox("Mean Speed", convertSpeed(exercise.meanSpeed), getUnit("speed"));
    addBox(
      "Mean Pace",
      formatPace(convertSpeedToPace(exercise.meanSpeed, "s/km")),
      getUnit("paceShort"),
    );
  }

  if (exercise.totalCalories !== null) {
    addBox(
      "Calories",
      truncateDecimals(exercise.totalCalories, 0),
      getUnit("calories"),
    );
  }

  if (exercise.meanHeartRate !== null) {
    addBox("Mean heart rate", exercise.meanHeartRate, getUnit("heartRate"));
  }

  if (exercise.distance !== null) {
    addBox("Distance", convertDistance(exercise.distance), getUnit("distance"));
  }

  if (exercise.meanCadence !== null) {
    addBox(
      "Mean cadence",
      truncateDecimals(exercise.meanCadence, 0),
      getUnit("cadence"),
    );
  }

  if (exercise.stepCount !== null && !exercise.subsetData.length) {
    addBox("Step count", exercise.stepCount);
  }

  if (exercise.maxHeartRate !== null) {
    addBox("Max heart rate", exercise.maxHeartRate, getUnit("heartRate"));
  }

  return boxes;
}

export default function ExerciseSummary({ exercise }) {
  const infoBoxes = useMemo(() => generateInfoboxes(exercise), [exercise]);

  return (
    <div className="exerciseOverview">
      {exercise && (
        <>
          <div className="infoHeader">
            <img
              className="infoHeaderIcon"
              src={exercise.icon}
              alt="exercise"
            />
            <p className="infoHeaderDateInterval">
              {formatDateInterval(exercise.startTime, exercise.endTime)}
            </p>
            <Link
              className="moreInfoButton"
              title="exercise detail"
              to={`/exercises/${exercise.id}/overview`}
            >
              <img
                className="moreInfoIcon"
                src={moreInfoIcon}
                alt="more info"
              />
            </Link>
          </div>
          {infoBoxes.slice(0, 6).map((box) => (
            <div key={box.title} className="infoBox">
              <div className="infoBoxLabel">{box.title}</div>
              <p className="infoBoxContent">{box.value + " " + box.units}</p>
            </div>
          ))}
        </>
      )}
    </div>
  );
}
