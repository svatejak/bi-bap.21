import React, { useMemo } from "react";

function getLegendItems(data) {
  return data.map((item) => {
    return (
      <div className="legendItem" key={item.name}>
        <div
          className="legendItemColor"
          style={{ backgroundColor: item.color }}
        ></div>
        <span>{item.name}</span>
      </div>
    );
  });
}

function MapLegend({ data }) {
  const legendItems = useMemo(() => {
    return getLegendItems(data);
  }, [data]);

  return <div className="mapLegend">{legendItems}</div>;
}

export default MapLegend;
