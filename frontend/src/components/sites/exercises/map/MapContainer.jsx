import Classic from "../../../../maps/exercise/Classic";
import Altitude from "../../../../maps/exercise/Altitude";
import GraphTypeDropdown from "../../../common/GraphTypeDropdown";
import React, { useState, useMemo } from "react";
import { getColor } from "../../../../utils/formatData";
import MapLegend from "./MapLegend";
import { remToPixels } from "../../../../utils/formatData";

const mapSize = { width: remToPixels(135), height: remToPixels(70) };

function getMapContent(mapType, locationData) {
  switch (mapType) {
    case "classic":
      return <Classic locationData={locationData} mapSize={mapSize} />;
    // case "altitude":
    //   return <Altitude locationData={locationData} />;
    default:
      return null;
  }
}

function getMapLines(mapType) {
  switch (mapType) {
    case "classic":
      return [{ name: "Route", color: getColor("primary-color") }];
    // case "altitude":
    //   return [
    //     { name: "Heart rate", color: "red" },
    //     { name: "Speed", color: getColor("secondary-color") },
    //   ];
    default:
      return [];
  }
}

function getOptions() {
  let resOptions = [
    { value: "classic", label: "Classic" },
    // { value: "altitude", label: "Altitude" },
  ];
  // add more options here
  return resOptions;
}

function MapContainer({ locationData }) {
  const [mapType, setMapType] = useState("classic");
  const mapContent = useMemo(() => {
    return getMapContent(mapType, locationData);
  }, [mapType, locationData]);
  const mapLines = useMemo(() => {
    return getMapLines(mapType);
  }, [mapType]);

  const dropDownOptions = getOptions();

  return (
    <div className="mapContainer">
      {mapContent}
      <GraphTypeDropdown setGraphType={setMapType} options={dropDownOptions} />
      <MapLegend data={mapLines} />
    </div>
  );
}

export default MapContainer;
