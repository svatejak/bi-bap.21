import React, { useMemo } from "react";
import { useParams } from "react-router-dom";
import { getExercise, getExerciseLocationData } from "../../../../utils/api";
import MapContainer from "./MapContainer";
import { processLocationData } from "../../../../utils/processData";
import { useQuery } from "react-query";
import ErrorPage from "../../errorPage/ErrorPage";
import checkForErrors from "../../../../utils/checkForErrors";

function ExerciseMap() {
  const { exerciseId } = useParams();
  const {
    data: exercise,
    isLoading: exerciseLoading,
    error: exerciseError,
  } = useQuery(["exercise", exerciseId], () => getExercise(exerciseId));
  const {
    data: locationData,
    isLoading: locationLoading,
    error: locationError,
  } = useQuery(["exerciseLocationData", exerciseId], () =>
    getExerciseLocationData(exerciseId),
  );

  const loading = exerciseLoading || locationLoading;
  const errors = checkForErrors([exerciseError, locationError]);

  const processedLocationData = useMemo(() => {
    if (
      loading ||
      errors.hasError ||
      !locationData ||
      !exercise ||
      locationData.length === 0
    )
      return;
    const startTime = new Date(exercise.startTime).getTime();
    return processLocationData(locationData, startTime);
  }, [locationData, exercise, loading, errors]);

  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  if (loading) {
    return <></>;
  }

  if (!processedLocationData) {
    return <p className="message">No location data found.</p>;
  }

  return (
    <div className="mapLayout">
      <MapContainer locationData={processedLocationData} />
    </div>
  );
}

export default ExerciseMap;
