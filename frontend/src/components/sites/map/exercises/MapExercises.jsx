import React, {useMemo} from "react";
import { getExercises } from "../../../../utils/api";
import MapContainer from "./MapContainer";
import { useQuery } from "react-query";
import checkForErrors from "../../../../utils/checkForErrors";
import ErrorPage from "../../errorPage/ErrorPage";

function MapExercises() {
  const {
    data: exercises,
    isLoading: exercisesLoading,
    error: exercisesError,
  } = useQuery(["map", "exercises"], () =>
    getExercises({ include_position: true }),
  );

  const exercisesWithLocation = useMemo(() => {
    if (!exercises || !exercises.exercises) {
      return [];
    }
    return exercises.exercises.filter((exercise) => exercise.position);
  }, [exercises]);

  const loading = exercisesLoading;
  const errors = checkForErrors([exercisesError]);

  if (errors.hasError) {
    return <ErrorPage errorCode={errors.errorCode} />;
  }

  if (loading) {
    return <></>;
  }

  if (exercisesWithLocation.length === 0) {
    return <p className="message">No exercises found.</p>;
  }


  return (
    <div className="mapLayout">
      <MapContainer exercises={exercisesWithLocation}/>
    </div>
  );
}

export default MapExercises;
