import React from "react";
import Exercises from "../../../../maps/map/Exercises";

function MapContainer({ exercises }) {

  return (
    <div className="exerciseMapContainer">
      <Exercises exercises={exercises} />
    </div>
  );
}

export default MapContainer;
