import { MapContainer, TileLayer, Popup, Marker } from "react-leaflet";
import React from "react";
import MarkerClusterGroup from "react-leaflet-cluster";
import { formatDate } from "../../utils/formatData";
import * as d3 from "d3";
import L from "leaflet";

import mapMarker from "../../assets/mapStart2.png";
import moreInfoIcon from "../../assets/moreInfo.png";

import { Link } from "react-router-dom";

const mapMarkerIcon = new L.Icon({
  iconUrl: mapMarker,
  iconSize: [30, 30],
  iconAnchor: [15, 30],
});

function getZoomLevel(latitudeRange, longitudeRange, mapWidth, mapHeight) {
  const containerWidth = mapWidth || window.innerWidth;
  const containerHeight = mapHeight || window.innerHeight;

  const maxZoom = 18; // max zoom level for leaflet

  const latRange = Math.abs(latitudeRange[1] - latitudeRange[0]);
  const lngRange = Math.abs(longitudeRange[1] - longitudeRange[0]);

  const zoomLat = Math.log2((360 * (containerHeight / 256)) / latRange);
  const zoomLng = Math.log2((360 * (containerWidth / 256)) / lngRange);

  return Math.min(Math.floor(zoomLat), Math.floor(zoomLng), maxZoom) - 1;
}

function getCenter(latitudeRange, longitudeRange) {
  return [
    (latitudeRange[0] + latitudeRange[1]) / 2,
    (longitudeRange[0] + longitudeRange[1]) / 2,
  ];
}

function Exercises({ exercises }) {
  const latitudeRange = d3.extent(exercises, (d) => d.position.latitude);
  const longitudeRange = d3.extent(exercises, (d) => d.position.longitude);

  const center = getCenter(latitudeRange, longitudeRange);
  const zoomLevel = getZoomLevel(latitudeRange, longitudeRange);

  function generateMarkers() {
    return exercises.map((exercise) => {
      const position = [
        exercise.position.latitude,
        exercise.position.longitude,
      ];
      return (
        <Marker position={position} icon={mapMarkerIcon} key={exercise.id}>
          <Popup>
            <div className="infoPopup">
              <img
                className="infoPopupIcon"
                src={exercise.icon}
                alt="exercise"
              />
              <p className="infoPopupDateInterval">
                {formatDate(exercise.startTime)}
              </p>
              <Link
                className="moreInfoButtonPopup"
                title="exercise detail"
                to={`/exercises/${exercise.id}/overview`}
              >
                <img
                  className="moreInfoIcon"
                  src={moreInfoIcon}
                  alt="more info"
                />
              </Link>
            </div>
          </Popup>
        </Marker>
      );
    });
  }

  function createClusterCustomIcon(cluster) {
    const childCount = cluster.getChildCount();
    let c = "custom-marker-cluster-";
    if (childCount < 10) {
      c += "small";
    } else if (childCount < 100) {
      c += "medium";
    } else {
      c += "large";
    }

    return new L.divIcon({
      html: '<div class="' + c + '" ><span>' + childCount + "</span></div>",
      className: "marker-cluster" + c,
      iconSize: new L.Point(40, 40),
    });
  }

  return (
    <div className="map">
      <MapContainer center={center} zoom={zoomLevel}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <MarkerClusterGroup iconCreateFunction={createClusterCustomIcon}>
          {generateMarkers()}
        </MarkerClusterGroup>
      </MapContainer>
    </div>
  );
}

export default Exercises;
