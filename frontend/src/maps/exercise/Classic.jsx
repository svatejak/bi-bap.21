import { MapContainer, TileLayer, Polyline, Marker } from "react-leaflet";
import React from "react";
import { getColor } from "../../utils/formatData";
import * as d3 from "d3";
import L from "leaflet";

import mapStartIcon from "../../assets/mapStart2.png";
import mapEndIcon from "../../assets/mapFinish3.png";

const polylineOptions = {
  color: getColor("primary-color"),
  weight: 5,
  lineJoin: "round",
  lineCap: "round",
  smoothFactor: 1,
};

const startIcon = new L.Icon({
  iconUrl: mapStartIcon,
  iconSize: [30, 30],
  iconAnchor: [15, 30],
});

const endIcon = new L.Icon({
  iconUrl: mapEndIcon,
  iconSize: [30, 30],
  iconAnchor: [5, 30],
});

function getZoomLevel(latitudeRange, longitudeRange, mapWidth, mapHeight) {
  const containerWidth = mapWidth || window.innerWidth;
  const containerHeight = mapHeight || window.innerHeight;

  const maxZoom = 18; // max zoom level for leaflet

  const latRange = Math.abs(latitudeRange[1] - latitudeRange[0]);
  const lngRange = Math.abs(longitudeRange[1] - longitudeRange[0]);

  const zoomLat = Math.log2((360 * (containerHeight / 256)) / latRange);
  const zoomLng = Math.log2((360 * (containerWidth / 256)) / lngRange);

  return Math.min(Math.floor(zoomLat), Math.floor(zoomLng), maxZoom) - 1;
}

function getCenter(latitudeRange, longitudeRange) {
  return [
    (latitudeRange[0] + latitudeRange[1]) / 2,
    (longitudeRange[0] + longitudeRange[1]) / 2,
  ];
}

function Classic({ locationData, mapSize }) {
  const { width, height } = mapSize;
  const latitudeRange = d3.extent(locationData, (d) => d.latitude);
  const longitudeRange = d3.extent(locationData, (d) => d.longitude);
  const center = getCenter(latitudeRange, longitudeRange);
  const zoomLevel = getZoomLevel(latitudeRange, longitudeRange, width, height);

  const path = locationData.map((loc) => [loc.latitude, loc.longitude]);
  const startPosition = [locationData[0].latitude, locationData[0].longitude];
  const endPosition = [
    locationData[locationData.length - 1].latitude,
    locationData[locationData.length - 1].longitude,
  ];

  return (
    <div className="map">
      <MapContainer center={center} zoom={zoomLevel}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Polyline positions={path} pathOptions={polylineOptions} />
        <Marker position={startPosition} icon={startIcon} />
        <Marker position={endPosition} icon={endIcon} />
      </MapContainer>
    </div>
  );
}

export default Classic;
