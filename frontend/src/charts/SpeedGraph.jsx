import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import { remToPixels } from "../utils/formatData";
import { getColor } from "../utils/formatData";

function MotionGraph({ data, yLabel, graphSizeInfo }) {
  const svgRef = useRef(null);
  const { width, height, marginTop, marginRight, marginBottom, marginLeft } =
    graphSizeInfo;
  const yAxisMultiplier = 1.2;
  const timeRange = d3.extent(data, (d) => d.time);
  const x = getX();
  const y = getY(data);

  function createSVG() {
    return d3
      .create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");
  }

  function addLine(svg) {
    const line = d3
      .line()
      .x((d) => x(d.time))
      .y((d) => y(d.yAxis))
      .curve(d3.curveBasis);

    svg
      .append("path")
      .attr("fill", "none")
      .attr("stroke", getColor("primary-color"))
      .attr("stroke-width", 2.5)
      .attr("d", line(data));
  }

  function addAxis(svg) {
    svg
      .append("g")
      .attr("transform", `translate(0,${height - marginBottom})`)
      .call(d3.axisBottom(x).ticks(5).tickSizeOuter(0))

      .call((g) =>
        g
          .append("text")
          .attr("text-anchor", "end")
          .attr("x", width - remToPixels(2))
          .attr("y", remToPixels(4.5))
          .attr("fill", "currentColor")
          .text("Time (min)"),
      );

    svg
      .append("g")
      .attr("transform", `translate(${marginLeft},0)`)
      .call(d3.axisLeft(y).ticks(height / remToPixels(4)))
      .call((g) => g.select(".domain").remove())
      .call((g) =>
        g
          .selectAll(".tick line")
          .clone()
          .attr("x2", width - marginLeft - marginRight)
          .attr("stroke-opacity", 0.1),
      )
      .call((g) =>
        g
          .append("text")
          .attr("x", remToPixels(-3.5))
          .attr("y", remToPixels(3.5))
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text(yLabel),
      );
  }

  function getX() {
    return d3
      .scaleLinear()
      .domain(timeRange)
      .range([marginLeft, width - marginRight]);
  }

  function getY(data) {
    console.log(data);
    let yRange = [0, d3.max(data, (d) => d.yAxis) * yAxisMultiplier];
    return d3
      .scaleLinear()
      .domain(yRange)
      .range([height - marginBottom, marginTop]);
  }

  useEffect(() => {
    if (data === 0) return;

    const svg = createSVG();
    addAxis(svg);
    addLine(svg);

    svgRef.current.innerHTML = "";
    svgRef.current.appendChild(svg.node());
  }, []);

  return <div ref={svgRef} className="chart-container"></div>;
}

export default MotionGraph;
