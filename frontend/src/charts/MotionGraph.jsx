import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import { remToPixels, getColor } from "../utils/formatData";

function MotionGraph({
  data,
  yLabel,
  type,
  hoveredTime,
  setHoveredTime,
  graphSizeInfo,
}) {
  const svgRef = useRef(null);
  const { width, height, marginTop, marginRight, marginBottom, marginLeft } =
    graphSizeInfo;
  const yAxisMultiplier = 1.2;

  const timeRange = d3.extent(data, (d) => d.time);
  const x = getX();
  const y = getY(data);

  function addLine(svg) {
    const line = d3
      .line()
      .x((d) => x(d.time))
      .y((d) => y(d.yAxis))
      .curve(d3.curveBasis);

    svg
      .append("path")
      .attr("fill", "none")
      .attr("stroke", getColor("primary-color"))
      .attr("stroke-width", 2.5)
      .attr("d", line(data));
  }

  function createSVG() {
    return d3
      .create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");
  }

  function addAxis(svg) {
    svg
      .append("g")
      .attr("transform", `translate(0,${height - marginBottom})`)
      .call(
        d3
          .axisBottom(x)
          .ticks(width / 80)
          .tickSizeOuter(0),
      )

      .call((g) =>
        g
          .append("text")
          .attr("text-anchor", "end")
          .attr("x", width - remToPixels(2))
          .attr("y", remToPixels(4.5))
          .attr("fill", "currentColor")
          .text("Time (min)"),
      );

    svg
      .append("g")
      .attr("transform", `translate(${marginLeft},0)`)
      .call(d3.axisLeft(y).ticks(5))
      .call((g) => g.select(".domain").remove())
      .call((g) =>
        g
          .selectAll(".tick line")
          .clone()
          .attr("x2", width - marginLeft - marginRight)
          .attr("stroke-opacity", 0.1),
      )
      .call((g) =>
        g
          .append("text")
          .attr("x", remToPixels(-4))
          .attr("y", remToPixels(3))
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text(yLabel),
      );
  }

  function getX() {
    return d3
      .scaleLinear()
      .domain(timeRange)
      .range([marginLeft, width - marginRight]);
  }

  function getY(data) {
    let yRange;
    if (type === "pace") {
      yRange = [d3.max(data, (d) => d.yAxis), 0];
    } else {
      yRange = [0, d3.max(data, (d) => d.yAxis) * yAxisMultiplier];
    }
    return d3
      .scaleLinear()
      .domain(yRange)
      .range([height - marginBottom, marginTop]);
  }

  useEffect(() => {
    if (data === 0) return;

    const svg = createSVG();
    addAxis(svg);
    addLine(svg);

    const handleMouseMove = (() => {
      return (event) => {
        const mouseX = d3.pointer(event)[0];
        const xValue = x.invert(mouseX);
        if (!xValue) return;
        if (xValue > timeRange[0] && xValue < timeRange[1]) {
          setHoveredTime(xValue);
        } else {
          setHoveredTime(null);
        }
      };
    })();

    svg
      .append("rect")
      .attr("width", width)
      .attr("height", height)
      .style("fill", "none")
      .style("pointer-events", "all")
      .on("mousemove", handleMouseMove);

    svgRef.current.innerHTML = "";
    svgRef.current.appendChild(svg.node());
  }, []);

  useEffect(() => {
    const svg = d3.select(svgRef.current.querySelector("svg"));
    if (hoveredTime === null) {
      svg.select(".hover-line").remove();
      return;
    }
    if (svg === null) return;

    svg.select(".hover-line").remove();

    svg
      .append("line")
      .attr("class", "hover-line")
      .attr("stroke", "black")
      .attr("stroke-dasharray", "4")
      .attr("x1", x(hoveredTime))
      .attr("x2", x(hoveredTime))
      .attr("y1", marginTop)
      .attr("y2", height - marginBottom);
  }, [hoveredTime]);

  return <div ref={svgRef} className="chart-container"></div>;
}

export default MotionGraph;
