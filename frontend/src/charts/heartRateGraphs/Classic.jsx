import React, { useRef, useEffect } from "react";
import * as d3 from "d3";
import { remToPixels } from "../../utils/formatData";

function Classic({ data, sizeInfo, yLabel, heartRateZones }) {
  const { width, height, marginTop, marginRight, marginBottom, marginLeft } =
    sizeInfo;
  const svgRef = useRef(null);
  const yAxisMultiplier = 1.2;

  function addLine(svg, x, y) {
    const gradientId = "gradient";

    const gradientColor = (d) => {
      const section = heartRateZones.find(
        (section) => d.yAxis >= section.min && d.yAxis < section.max,
      );
      return section ? section.color : "black";
    };

    svg
      .append("linearGradient")
      .attr("id", gradientId)
      .attr("gradientUnits", "userSpaceOnUse")
      .attr("x1", 0)
      .attr("x2", width)
      .selectAll("stop")
      .data(data)
      .join("stop")
      .attr("offset", (d) => x(d.time) / width)
      .attr("stop-color", (d) => gradientColor(d));

    const line = d3
      .line()
      .x((d) => x(d.time))
      .y((d) => y(d.yAxis))
      .curve(d3.curveBasis);

    svg
      .append("path")
      .datum(data)
      .attr("fill", "none")
      .attr("stroke", `url(#${gradientId})`)
      .attr("stroke-width", 2.5)
      .attr("d", line);
  }

  function createSVG() {
    return d3
      .create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");
  }

  function addAxis(svg, x, y) {
    svg
      .append("g")
      .attr("transform", `translate(0,${height - marginBottom})`)
      .call(d3.axisBottom(x).ticks(5).tickSizeOuter(0))

      .call((g) =>
        g
          .append("text")
          .attr("text-anchor", "end")
          .attr("x", width - remToPixels(2))
          .attr("y", remToPixels(4.5))
          .attr("fill", "currentColor")
          .text("Time (min)"),
      );

    svg
      .append("g")
      .attr("transform", `translate(${marginLeft},0)`)
      .call(d3.axisLeft(y).ticks(5))
      .call((g) => g.select(".domain").remove())
      .call((g) =>
        g
          .selectAll(".tick line")
          .clone()
          .attr("x2", width - marginLeft - marginRight)
          .attr("stroke-opacity", 0.1),
      )
      .call((g) =>
        g
          .append("text")
          .attr("x", remToPixels(-5))
          .attr("y", remToPixels(4))
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text(yLabel),
      );
  }

  useEffect(() => {
    if (!data.length) return;

    const timeRange = d3.extent(data, (d) => d.time);
    const x = d3
      .scaleLinear()
      .domain(timeRange)
      .range([marginLeft, width - marginRight]);

    const yRange = [
      Math.min(
        80,
        d3.min(data, (d) => d.yAxis),
      ),
      Math.max(190, d3.max(data, (d) => d.yAxis) * yAxisMultiplier),
    ];
    const y = d3
      .scaleLinear()
      .domain(yRange)
      .range([height - marginBottom, marginTop]);

    const svg = createSVG();
    addAxis(svg, x, y);
    addLine(svg, x, y);

    svgRef.current.innerHTML = "";
    svgRef.current.appendChild(svg.node());
  }, []);

  return <div ref={svgRef} className="chart-container"></div>;
}

export default Classic;
