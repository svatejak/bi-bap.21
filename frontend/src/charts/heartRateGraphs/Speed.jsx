import React, { useRef, useEffect } from "react";
import * as d3 from "d3";
import { remToPixels } from "../../utils/formatData";
import { getColor } from "../../utils/formatData";

function Speed({ heartRateData, data, sizeInfo, yLabel }) {
  const { width, height, marginTop, marginRight, marginBottom, marginLeft } =
    sizeInfo;
  const svgRef = useRef(null);
  const yAxisMultiplier = 1.2;

  function addLine(svg, x, y1, y2) {
    const area1 = d3
      .area()
      .x((d) => x(d.time))
      .y0(height - marginBottom)
      .y1((d) => y1(d.yAxis))
      .curve(d3.curveBasis);

    const redAreaColor = "rgba(255, 0, 0, 0.3)";
    svg
      .append("path")
      .attr("fill", redAreaColor)
      .attr("d", area1(heartRateData));

    const line1 = d3
      .line()
      .x((d) => x(d.time))
      .y((d) => y1(d.yAxis))
      .curve(d3.curveBasis);

    svg
      .append("path")
      .attr("fill", "none")
      .attr("stroke", "red")
      .attr("stroke-width", 2.5)
      .attr("d", line1(heartRateData));

    const line2 = d3
      .line()
      .x((d) => x(d.time))
      .y((d) => y2(d.yAxis))
      .curve(d3.curveBasis);

    svg
      .append("path")
      .attr("fill", "none")
      .attr("stroke", getColor("secondary-color"))
      .attr("stroke-width", 2.5)
      .attr("d", line2(data));
  }

  function createSVG() {
    return d3
      .create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");
  }

  function addAxis(svg, x, y1, y2) {
    svg
      .append("g")
      .attr("transform", `translate(0,${height - marginBottom})`)
      .call(d3.axisBottom(x).ticks(10).tickSizeOuter(0))

      .call((g) =>
        g
          .append("text")
          .attr("text-anchor", "end")
          .attr("x", width - remToPixels(2))
          .attr("y", remToPixels(4.5))
          .attr("fill", "currentColor")
          .text("Time (min)"),
      );

    svg
      .append("g")
      .attr("transform", `translate(${marginLeft},0)`)
      .call(d3.axisLeft(y1).ticks(10))
      .call((g) => g.select(".domain").remove())
      .call((g) =>
        g
          .append("text")
          .attr("x", remToPixels(-5))
          .attr("y", remToPixels(4))
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text("Heart rate (b/m)"),
      );

    svg
      .append("g")
      .attr("transform", `translate(${width - marginRight},0)`)
      .call(d3.axisRight(y2).ticks(10))
      .call((g) => g.select(".domain").remove())
      .call((g) =>
        g
          .append("text")
          .attr("x", remToPixels(-7))
          .attr("y", remToPixels(4))
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text(yLabel),
      );
  }

  useEffect(() => {
    if (!data.length || !heartRateData.length) return;

    const timeRangeMax = Math.max(
      d3.max(heartRateData, (d) => d.time),
      d3.max(data, (d) => d.time),
    );
    const timeRangeMin = Math.min(
      d3.min(heartRateData, (d) => d.time),
      d3.min(data, (d) => d.time),
    );
    const timeRange = [timeRangeMin, timeRangeMax];
    const x = d3
      .scaleLinear()
      .domain(timeRange)
      .range([marginLeft, width - marginRight]);

    const yHeartRateRange = [
      Math.min(
        80,
        d3.min(heartRateData, (d) => d.yAxis),
      ),
      Math.max(190, d3.max(heartRateData, (d) => d.yAxis) * yAxisMultiplier),
    ];
    const yHeartRate = d3
      .scaleLinear()
      .domain(yHeartRateRange)
      .range([height - marginBottom, marginTop]);

    const yDifference =
      d3.max(data, (d) => d.yAxis) - d3.min(data, (d) => d.yAxis);
    const yDifferenceMultiplier = 3;
    const yDifferenceMargin = yDifference * yDifferenceMultiplier;

    const yDataRange = [0, d3.max(data, (d) => d.yAxis) + yDifferenceMargin];
    const yData = d3
      .scaleLinear()
      .domain(yDataRange)
      .range([height - marginBottom, marginTop]);

    const svg = createSVG();

    addAxis(svg, x, yHeartRate, yData);
    addLine(svg, x, yHeartRate, yData);

    svgRef.current.innerHTML = "";
    svgRef.current.appendChild(svg.node());
  }, []);

  return <div ref={svgRef} className="chart-container"></div>;
}

export default Speed;
