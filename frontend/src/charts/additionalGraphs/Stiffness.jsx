import React, { useRef, useEffect } from "react";
import * as d3 from "d3";
import { remToPixels, getColor } from "../../utils/formatData";

function Stiffness({ data, sizeInfo, yLabel }) {
  const { width, height, marginTop, marginRight, marginBottom, marginLeft } =
    sizeInfo;
  const svgRef = useRef(null);
  const yAxisMultiplier = 1.2;

  function addDots(svg, x, y) {
    svg
      .append("g")
      .attr("fill", getColor("additional-color3"))
      .selectAll("circle")
      .data(data.filter((d) => d.score === 0))
      .join("circle")
      .attr("cx", (d) => x(d.time))
      .attr("cy", (d) => y(d.yAxis))
      .attr("r", remToPixels(0.3));

    svg
      .append("g")
      .attr("fill", getColor("additional-color2"))
      .selectAll("circle")
      .data(data.filter((d) => d.score === 1))
      .join("circle")
      .attr("cx", (d) => x(d.time))
      .attr("cy", (d) => y(d.yAxis))
      .attr("r", remToPixels(0.3));

    svg
      .append("g")
      .attr("fill", getColor("additional-color1"))
      .selectAll("circle")
      .data(data.filter((d) => d.score === 2))
      .join("circle")
      .attr("cx", (d) => x(d.time))
      .attr("cy", (d) => y(d.yAxis))
      .attr("r", remToPixels(0.3));
  }

  function createSVG() {
    return d3
      .create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");
  }

  function addAxis(svg, x, y) {
    svg
      .append("g")
      .attr("transform", `translate(0,${height - marginBottom})`)
      .call(d3.axisBottom(x).ticks(10).tickSizeOuter(0))

      .call((g) =>
        g
          .append("text")
          .attr("text-anchor", "end")
          .attr("x", width - remToPixels(2))
          .attr("y", remToPixels(4.5))
          .attr("fill", "currentColor")
          .text("Time (min)"),
      );

    svg
      .append("g")
      .attr("transform", `translate(${marginLeft},0)`)
      .call(d3.axisLeft(y).ticks(10))
      .call((g) => g.select(".domain").remove())
      .call((g) =>
        g
          .selectAll(".tick line")
          .clone()
          .attr("x2", width - marginLeft - marginRight)
          .attr("stroke-opacity", 0.1),
      )
      .call((g) =>
        g
          .append("text")
          .attr("x", remToPixels(-5))
          .attr("y", remToPixels(4))
          .attr("fill", "currentColor")
          .attr("text-anchor", "start")
          .text(yLabel),
      );
  }

  useEffect(() => {
    if (!data.length) return;

    const timeRange = d3.extent(data, (d) => d.time);
    const x = d3
      .scaleLinear()
      .domain(timeRange)
      .range([marginLeft, width - marginRight]);

    const yRange = [0, d3.max(data, (d) => d.yAxis) * yAxisMultiplier];
    const y = d3
      .scaleLinear()
      .domain(yRange)
      .range([height - marginBottom, marginTop]);

    const svg = createSVG();
    addAxis(svg, x, y);
    addDots(svg, x, y);

    svgRef.current.innerHTML = "";
    svgRef.current.appendChild(svg.node());
  }, []);

  return <div ref={svgRef} className="chart-container"></div>;
}

export default Stiffness;
