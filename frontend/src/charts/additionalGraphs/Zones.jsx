import React, { useRef, useEffect, useState } from "react";
import * as d3 from "d3";
import { getColor } from "../../utils/formatData";

const color = d3
  .scaleOrdinal()
  .range([
    getColor("additional-color3"),
    getColor("additional-color2"),
    getColor("additional-color1"),
  ]);

function Zones({ data, sizeInfo }) {
  const { width, height, radius } = sizeInfo;
  const [isMouseOverTooltip, setIsMouseOverTooltip] = useState(false);
  const svgRef = useRef(null);

  const [tooltip, setTooltip] = useState(null);

  function createSVG() {
    return d3
      .create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [-width / 2, -height / 2, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");
  }

  function createPath(svg, arcs, color) {
    const arcGenerator = d3.arc().innerRadius(0).outerRadius(radius);

    svg
      .selectAll("path")
      .data(arcs)
      .join("path")
      .attr("fill", (d, i) => color(i))
      .attr("stroke", "white")
      .attr("stroke-width", 2)
      .attr("d", arcGenerator)
      .on("mouseenter", (event, d) => {
        const [x, y] = arcGenerator.centroid(d);
        const percent = (d.data / d3.sum(data)) * 100;
        setTooltip({
          zone: d.index,
          percent: `${percent.toFixed(2)}%`,
          x: x + width / 3,
          y: y + height / 3,
        });
      })
      .on("mouseleave", () => {
        if (!isMouseOverTooltip) {
          setTooltip(null);
        }
      });
  }

  useEffect(() => {
    const svg = createSVG();
    const pie = d3.pie();
    const arcs = pie(data);
    createPath(svg, arcs, color);

    svgRef.current.innerHTML = "";
    svgRef.current.appendChild(svg.node());
  }, [data]);

  return (
    <div className="zonesChartWrapper">
      <div style={{ position: "relative" }} className="zones-chart">
        {tooltip && (
          <div
            style={{
              position: "absolute",
              top: tooltip.y,
              left: tooltip.x,
              backgroundColor: "white",
              padding: "4px",
              border: "1px solid black",
              zIndex: 1000,
            }}
          >
            {tooltip.percent}
          </div>
        )}
        <div
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            pointerEvents: "none",
          }}
          onMouseEnter={() => setIsMouseOverTooltip(true)}
          onMouseLeave={() => setIsMouseOverTooltip(false)}
        ></div>
        <div ref={svgRef}></div>
      </div>
    </div>
  );
}

export default Zones;
