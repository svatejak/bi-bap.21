import AWS from "aws-sdk";

const s3 = new AWS.S3({
  accessKeyId: process.env.REACT_APP_MINIO_ACCESS_KEY,
  secretAccessKey: process.env.REACT_APP_MINIO_SECRET_KEY,
  endpoint: process.env.REACT_APP_MINIO_URL,
  s3ForcePathStyle: true,
  signatureVersion: "v4",
});

export default s3;
