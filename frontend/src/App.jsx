import React from "react";
import Logo from "./components/common/Logo";
import Sidebar from "./components/common/Sidebar";
import Navigation from "./components/common/Navigation";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";

import ErrorPage from "./components/sites/errorPage/ErrorPage";

import Upload from "./components/sites/upload/zipUpload/Upload";

import CalendarExercises from "./components/sites/calendar/exercises/CalendarExercises";
import CalendarCalories from "./components/sites/calendar/calories/CalendarCalories";
import CalendarSleep from "./components/sites/calendar/sleep/CalendarSleep";

import Exercises from "./components/sites/exercises/exercises/Exercises";
import ExerciseOverview from "./components/sites/exercises/overview/ExerciseOverview";
import ExerciseMap from "./components/sites/exercises/map/ExerciseMap";
import ExerciseHeartRate from "./components/sites/exercises/heartRate/ExerciseHeartRate";
import ExerciseMotion from "./components/sites/exercises/motion/ExerciseMotion";
import ExerciseAdditional from "./components/sites/exercises/additional/ExerciseAdditional";

import MapExercises from "./components/sites/map/exercises/MapExercises";

import "./styles/App.css";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 1000 * 60 * 10, // 10 min
      cacheTime: 1000 * 60 * 30, // 30 min
      retry: false,
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Router>
        <div className="app">
          <Logo />
          <Sidebar sidebarItems />
          <Navigation navigationItems />

          <main>
            <Routes>
              <Route path="/" element={<Navigate to="/upload" replace />} />
              <Route path="/upload" element={<Upload />} />
              <Route path="/exercises" element={<Exercises />} />
              <Route path="/exercises/:exerciseId" element={<Exercises />} />
              <Route
                path="/exercises/:exerciseId/overview"
                element={<ExerciseOverview />}
              />
              <Route
                path="/exercises/:exerciseId/map"
                element={<ExerciseMap />}
              />
              <Route
                path="/exercises/:exerciseId/heart-rate"
                element={<ExerciseHeartRate />}
              />
              <Route
                path="/exercises/:exerciseId/motion"
                element={<ExerciseMotion />}
              />
              <Route
                path="/exercises/:exerciseId/additional"
                element={<ExerciseAdditional />}
              />

              <Route path="/map" element={<MapExercises />} />

              <Route path="*" element={<ErrorPage errorCode={404} />} />
            </Routes>
          </main>
        </div>
      </Router>
    </QueryClientProvider>
  );
}

export default App;
