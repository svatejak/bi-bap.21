import uploadIcon from "../assets/upload.png";
import exerciseIcon from "../assets/exercise.png";
import calendarIcon from "../assets/calendar.png";
import hearthRateIcon from "../assets/heartRate.png";
import mapIcon from "../assets/map.png";
import sleepIcon from "../assets/sleep.png";

const defaultSidebar = [
  {
    src: uploadIcon,
    id: "upload",
    text: "Upload",
    linkTo: "/upload",
    active: false,
  },
  {
    src: exerciseIcon,
    id: "exercises",
    text: "Exercises",
    linkTo: "/exercises",
    active: false,
  },
  {
    src: calendarIcon,
    id: "calendar",
    text: "Calendar",
    linkTo: "/calendar/exercises",
    active: false,
  },
  {
    src: hearthRateIcon,
    id: "heart-rate",
    text: "Heart rate",
    linkTo: "/heart-rate",
    active: false,
  },
  { src: mapIcon, id: "map", text: "Map", linkTo: "/map", active: false },
  {
    src: sleepIcon,
    id: "sleep",
    text: "Sleep",
    linkTo: "/sleep",
    active: false,
  },
];

export default defaultSidebar;
