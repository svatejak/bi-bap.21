function checkForErrors(errors) {
  for (let i = 0; i < errors.length; i++) {
    const error = errors[i];
    if (error) {
      return {
        hasError: true,
        errorCode: error.status || 500,
      };
    }
  }
  return {
    hasError: false,
    errorCode: 0,
  };
}

export default checkForErrors;
