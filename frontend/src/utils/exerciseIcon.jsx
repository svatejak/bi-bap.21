const iconsContext = require.context(
  "../assets/exerciseIcons/",
  false,
  /\.png$/,
);

const icons = iconsContext.keys().reduce((acc, key) => {
  const iconName = key.replace("./", "").replace(".png", "");
  acc[iconName] = iconsContext(key);
  return acc;
}, {});

export const getIcon = (exerciseName) => {
  switch (exerciseName) {
    case "Running":
      return icons.running;
    case "Walking":
      return icons.walking;
    case "Push-ups (Press-ups)":
      return icons.pushUps;
    case "Plank":
      return icons.plank;
    case "Lunges":
      return icons.lunges;
    case "Crunches":
      return icons.crunches;
    case "Cycling":
      return icons.cycling;
    case "Biking":
      return icons.cycling;
    case "Guided_breathing":
      return icons.guidedBreathing;
    case "Strength_training":
      return icons.strengthTraining;
    default:
      return icons.customType;
  }
};
