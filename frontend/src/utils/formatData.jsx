const moment = require("moment");

export function formatDate(inputDate, useCustomFormat = false) {
  if (!useCustomFormat) {
    return moment(inputDate).format("MMM DD, YYYY hh:mm A");
  } else {
    const date = new Date(inputDate);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    return `${year}-${month}-${day} ${hours}:${minutes}`;
  }
}

export function formatDataSource(dataSource) {
  return `${dataSource.dataSource} (${formatDate(dataSource.uploadTime, true)})`;
}

export function formatDuration(duration) {
  return moment.utc(duration).format("HH:mm:ss");
}

export function truncateDecimals(number, decimals) {
  const multiplier = Math.pow(10, decimals);
  const truncatedNumber = Math.trunc(number * multiplier) / multiplier;
  return parseFloat(truncatedNumber.toFixed(decimals));
}

export function convertDistance(meters) {
  return truncateDecimals(meters / 1000, 2);
}

export function convertSpeed(metersPerSecond) {
  return truncateDecimals(metersPerSecond * 3.6, 2);
}

export function convertSpeedToPace(metersPerSecond, format = "s/km") {
  let pace = 0;
  let minPace = 15;
  if (metersPerSecond < 1) return minPace;
  if (format === "s/km") {
    pace = 1000 / metersPerSecond;
  } else if (format === "min/km") {
    pace = 1000 / (metersPerSecond * 60);
  }
  return truncateDecimals(pace, 2);
}

export function milisecondsToMinutes(miliseconds) {
  return miliseconds / 60000;
}

export function minutesToMiliseconds(minutes) {
  return minutes * 60000;
}

export function metersToCentimeters(meters) {
  return meters * 100;
}

export function NmToKnm(Nm) {
  return Nm / 1000;
}

export function formatPace(seconds) {
  const minutes = Math.floor(seconds / 60);
  const remainingSeconds = Math.floor(seconds % 60);
  return `${minutes}'${remainingSeconds < 10 ? "0" : ""}${remainingSeconds}''`;
}

export function formatDateInterval(startDate, endDate) {
  const startFormatted = moment(startDate).format("DD. MM. HH:mm");
  const endFormatted = moment(endDate).format("HH:mm"); // Formátování pouze na HH:mm
  return `${startFormatted} - ${endFormatted}`;
}

export function remToPixels(rem) {
  return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
}

export function getColor(colorName) {
  return getComputedStyle(document.documentElement).getPropertyValue(
    `--${colorName}`,
  );
}

export function getUnit(type) {
  if (type === "speed") return "km/h";
  if (type === "pace") return "min/km";
  if (type === "paceShort") return "/km";
  if (type === "distance") return "km";
  if (type === "calories") return "kcal";
  if (type === "heartRate") return "b/m";
  if (type === "cadence") return "st./m";
  if (type === "altitude") return "m";
  if (type === "weight") return "kg";
  if (type === "time") return "ms";
  if (type === "stiffness") return "kNm";
  if (type === "undulation") return "cm";
}

export function roundNumberToDecimalPlaces(number, decimalPlaces) {
  const factor = Math.pow(10, decimalPlaces);
  return Math.floor(number * factor) / factor;
}
