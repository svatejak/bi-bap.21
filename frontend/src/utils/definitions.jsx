import { getIcon } from "./exerciseIcon";
import { formatDataSource } from "./formatData";

export class PartialExerciseDTO {
  constructor(data) {
    this.id = data.id;
    this.startTime = data.startTime;
    this.endTime = data.endTime;
    this.exerciseName = data.exerciseName;
    this.datasource = formatDataSource(data.dataSource);
    this.icon = getIcon(data.exerciseName);
    this.position = data.position;
  }
}

export class ExerciseListRecordDTO {
  constructor(data) {
    this.exerciseList = data.exerciseList.map(
      (exercise) => new PartialExerciseDTO(exercise),
    );
  }
}

export class AdditionalMetricDTO {
  constructor(data) {
    this.id = data.id;
    this.dataType = data.dataType;
    this.score = data.score;
    this.records = data.records.map(
      (record) => new AdditionalMetricRecordDTO(record),
    );
  }
}

export class AdditionalMetricRecordDTO {
  constructor(record) {
    this.id = record.id;
    this.duration = record.duration;
    this.score = record.score;
    this.value = record.value;
  }
}
