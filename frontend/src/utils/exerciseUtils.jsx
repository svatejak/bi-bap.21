import { getExercise } from "./api";

export function updateCurrentExerciseById(
  exerciseId,
  currentExercise,
  setCurrentExercise,
) {
  const currentExerciseID = currentExercise.id;

  if (currentExerciseID === exerciseId) return;
  getExercise(exerciseId)
    .then((data) => {
      setCurrentExercise(data);
    })
    .catch((error) => {
      console.error("There was a problem fetching the exercise:", error);
    });
}
