function generateNavigation(
  sidebarItem,
  navigationItem,
  setNavigationItems,
  id,
  exercise,
) {
  let newNavigationItems = [];

  switch (sidebarItem) {
    case "upload":
      newNavigationItems = [
        { text: "Zip upload", linkTo: "/upload", active: true },
      ];
      break;

    case "exercises":
      newNavigationItems = [
        {
          text: "Exercises",
          linkTo: `/exercises/${id}`,
          active: navigationItem === "exercises",
        },
      ];
      if (exercise) {
        newNavigationItems.push({
          text: "Overview",
          linkTo: `/exercises/${id}/overview`,
          active: navigationItem === "overview",
        });
        if (exercise.hasLocationData)
          newNavigationItems.push({
            text: "Map",
            linkTo: `/exercises/${id}/map`,
            active: navigationItem === "map",
          });
        if (exercise.hasHeartRateData)
          newNavigationItems.push({
            text: "Heart rate",
            linkTo: `/exercises/${id}/heart-rate`,
            active: navigationItem === "heart-rate",
          });
        if (exercise.hasMotionData)
          newNavigationItems.push({
            text: "Motion",
            linkTo: `/exercises/${id}/motion`,
            active: navigationItem === "motion",
          });
        if (exercise.hasAdditionalMetrics)
          newNavigationItems.push({
            text: "Additional",
            linkTo: `/exercises/${id}/additional`,
            active: navigationItem === "additional",
          });
      }
      break;

    case "calendar":
      newNavigationItems = [
        {
          text: "Exercises",
          linkTo: "/calendar/exercises",
          active: navigationItem === "exercises",
        },
        {
          text: "Calories",
          linkTo: "/calendar/calories",
          active: navigationItem === "calories",
        },
        {
          text: "Sleep",
          linkTo: "/calendar/sleep",
          active: navigationItem === "sleep",
        },
      ];
      break;

    case "map":
      newNavigationItems = [
        { text: "Exercises", linkTo: "/map", active: true },
      ];
      break;

    default:
      break;
  }

  setNavigationItems(newNavigationItems);
}

export default generateNavigation;
