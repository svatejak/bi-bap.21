import { PartialExerciseDTO, AdditionalMetricDTO } from "./definitions";
import { getIcon } from "./exerciseIcon";

const apiUrl = process.env.REACT_APP_API_URL || "http://localhost:5000";

export const uploadFiles = async (fileData) => {
  try {
    const response = await fetch(`${apiUrl}/api/upload`, {
      method: "POST",
      body: JSON.stringify(fileData),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();

    if (!response.ok) {
      const error = new Error(data.message || "Network response was not ok");
      error.status = response.status;
      error.message = data.message;
      throw error;
    }

    return data.message;
  } catch (error) {
    console.error("There was a problem with uploading the files:", error);
    throw error;
  }
};

export const getExercises = async ({
  sort_by = "start_time",
  order = "asc",
  page = undefined,
  include_position = false,
  limit = undefined,
  filter = undefined,
}) => {
  try {
    const queryParams = new URLSearchParams({ include_position });
    if (limit) {
      queryParams.append("limit", limit);
    }
    if (page) {
      queryParams.append("page", page);
    }
    if (filter) {
      queryParams.append("filter", filter);
    }
    if (sort_by && order) {
      queryParams.append("sort_by", sort_by);
      queryParams.append("order", order);
    }
    const response = await fetch(`${apiUrl}/api/exercises?${queryParams}`);
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }

    let newResponse = await response.json();
    newResponse.exercises = newResponse.exercises.map(
      (exercise) => new PartialExerciseDTO(exercise),
    );
    return newResponse;
  } catch (error) {
    console.error("There was a problem with fetching exercises:", error);
    console.log("There was a problem with fetching exercises:", error);
    throw error;
  }
};

export const getExercise = async (id) => {
  try {
    const response = await fetch(`${apiUrl}/api/exercises/${id}`);
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }
    let newResponse = await response.json();
    newResponse.icon = getIcon(newResponse.exerciseName);
    return newResponse;
  } catch (error) {
    console.error("There was a problem with fetching the exercise:", error);
    throw error;
  }
};

export const getExerciseMotionData = async (id) => {
  try {
    const response = await fetch(`${apiUrl}/api/exercises/${id}/motion-data`);
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }
    return await response.json();
  } catch (error) {
    console.error("There was a problem with fetching the motion data:", error);
    throw error;
  }
};

export const getExerciseLocationData = async (id) => {
  try {
    const response = await fetch(`${apiUrl}/api/exercises/${id}/location-data`);
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }
    return await response.json();
  } catch (error) {
    console.error(
      "There was a problem with fetching the location data:",
      error,
    );
    throw error;
  }
};

export const getExerciseHeartRateData = async (id) => {
  try {
    const response = await fetch(`${apiUrl}/api/exercises/${id}/heart-rate`);
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }
    return await response.json();
  } catch (error) {
    console.error(
      "There was a problem with fetching the heart rate data:",
      error,
    );
    throw error;
  }
};

export const getExerciseRecoveryHeartRate = async (id) => {
  try {
    const response = await fetch(
      `${apiUrl}/api/exercises/${id}/recovery-heart-rate`,
    );
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }
    return await response.json();
  } catch (error) {
    console.error(
      "There was a problem with fetching the recovery heart rate:",
      error,
    );
    throw error;
  }
};

export const getExerciseAdditionalData = async (id) => {
  try {
    const response = await fetch(
      `${apiUrl}/api/exercises/${id}/additional-metrics`,
    );
    if (!response.ok) {
      const error = new Error("Network response was not ok");
      error.status = response.status;
      throw error;
    }
    const newResponse = await response.json();
    const metrics = newResponse.map(
      (metric) => new AdditionalMetricDTO(metric),
    );
    return metrics;
  } catch (error) {
    console.error(
      "There was a problem with fetching the additional metrics:",
      error,
    );
    throw error;
  }
};
