import defaultSidebar from "./defaultSidebar";

function generateSidebar(sidebarItems, setSidebarItems, currentItem) {
  if (currentItem === sidebarItems.find((item) => item.active === true)) return;

  const newSidebarItems = defaultSidebar.map((item) => {
    if (item.id === currentItem) {
      return { ...item, active: true };
    } else {
      return { ...item, active: false };
    }
  });
  setSidebarItems(newSidebarItems);
}

export default generateSidebar;
