import {
  milisecondsToMinutes,
  convertSpeed,
  convertSpeedToPace,
  metersToCentimeters,
  NmToKnm,
} from "./formatData";

export function processSpeedData(motionData, startTime) {
  return motionData
    .filter((d) => d.speed !== null)
    .map((d) => ({
      time: milisecondsToMinutes(d.startTime - startTime),
      yAxis: convertSpeed(d.speed),
    }));
}

export function processPaceData(motionData, startTime) {
  return motionData
    .filter((d) => d.speed !== null)
    .map((d) => ({
      time: milisecondsToMinutes(d.startTime - startTime),
      yAxis: convertSpeedToPace(d.speed, "min/km"),
    }));
}

export function processCadenceData(motionData, startTime) {
  return motionData
    .filter((d) => d.cadence !== null)
    .map((d) => ({
      time: milisecondsToMinutes(d.startTime - startTime),
      yAxis: d.cadence,
    }));
}

export function processAltitudeData(locationData, startTime) {
  return locationData
    .filter((d) => d.altitude !== null)
    .map((d) => ({
      time: milisecondsToMinutes(d.startTime - startTime),
      yAxis: d.altitude,
    }));
}

export function processHeartRateData(heartRateData, startTime) {
  return heartRateData.map((d) => ({
    time: milisecondsToMinutes(d.startTime - startTime),
    yAxis: d.heartRate,
  }));
}

export function processRecoveryHeartRateData(recoveryHeartRate, startTime) {
  return recoveryHeartRate.map((d) => ({
    time: milisecondsToMinutes(d.startTime - startTime),
    yAxis: d.heartRate,
  }));
}

export function processSpecAdditionalData(data) {
  return data.map((d) => ({
    time: milisecondsToMinutes(d.duration),
    score: d.score,
    yAxis: d.value,
  }));
}

export function processStiffnessData(data) {
  return data.map((d) => ({
    time: milisecondsToMinutes(d.duration),
    score: d.score,
    yAxis: NmToKnm(d.value),
  }));
}

export function processUndulationData(data) {
  return data.map((d) => ({
    time: milisecondsToMinutes(d.duration),
    score: d.score,
    yAxis: metersToCentimeters(d.value),
  }));
}

export function processLocationData(locationData, startTime) {
  return locationData.map((d) => ({
    time: milisecondsToMinutes(d.startTime - startTime),
    altitude: d.altitude,
    latitude: d.latitude,
    longitude: d.longitude,
  }));
}
