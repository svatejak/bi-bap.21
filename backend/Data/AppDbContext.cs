using Microsoft.EntityFrameworkCore;
using backend.Models;


namespace backend.Data
{
    public class AppDbContext : DbContext
    {
        private readonly IConfiguration _config;

        public AppDbContext(IConfiguration configuration)
        {
            _config = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_config.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Exercise>()
                .HasKey(e => e.Id);

            // modelBuilder.Entity<Exercise>()
            //     .HasOne(e => e.DataSource)
            //     .WithMany(ds => ds.Exercises)
            //     .IsRequired();

        }


        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseAdditionalMetric> ExerciseAdditionalMetric { get; set; }
        public DbSet<ExerciseAdditionalMetricRecord> ExerciseAdditionalMetricRecord { get; set; }
        public DbSet<ExerciseHeartRate> ExerciseHeartRate { get; set; }
        public DbSet<ExerciseLocationData> ExerciseLocationData { get; set; }
        public DbSet<ExerciseMotionData> ExerciseMotionData { get; set; }
        public DbSet<ExerciseRecoveryHeartRateData> ExerciseRecoveryHeartRate { get; set; }
        public DbSet<ExerciseSubsetData> ExerciseSubsetData { get; set; }
        public DbSet<DatasourceUpload> DatasourceUploads { get; set; }
        public DbSet<Sleep> Sleep { get; set; }
        public DbSet<SleepStage> SleepStages { get; set; }
        public DbSet<DaySummary> DaySummaries { get; set; }
        public DbSet<HeartRate> HeartRates { get; set; }
        public DbSet<OxygenSaturation> OxygenSaturations { get; set; }
        public DbSet<Stress> Stress { get; set; }
    }
}
