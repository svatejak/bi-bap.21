﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace backend.Migrations
{
    /// <inheritdoc />
    public partial class AddedNewTables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SleepScore",
                table: "Sleep",
                newName: "Score");

            migrationBuilder.RenameColumn(
                name: "SleepDuration",
                table: "Sleep",
                newName: "Duration");

            migrationBuilder.RenameColumn(
                name: "SleepCycle",
                table: "Sleep",
                newName: "Cycle");

            migrationBuilder.CreateTable(
                name: "DaySummaries",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Date = table.Column<long>(type: "bigint", nullable: true),
                    Calories = table.Column<float>(type: "real", nullable: true),
                    StepCount = table.Column<long>(type: "bigint", nullable: true),
                    ActiveTime = table.Column<long>(type: "bigint", nullable: true),
                    MaxHeartRate = table.Column<int>(type: "integer", nullable: true),
                    MinHeartRate = table.Column<int>(type: "integer", nullable: true),
                    AverageHeartRate = table.Column<int>(type: "integer", nullable: true),
                    Distance = table.Column<float>(type: "real", nullable: true),
                    DataSourceId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DaySummaries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DaySummaries_DatasourceUploads_DataSourceId",
                        column: x => x.DataSourceId,
                        principalTable: "DatasourceUploads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HeartRates",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Offset = table.Column<string>(type: "text", nullable: true),
                    Min = table.Column<float>(type: "real", nullable: true),
                    Max = table.Column<float>(type: "real", nullable: true),
                    Average = table.Column<float>(type: "real", nullable: true),
                    DataSourceId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HeartRates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HeartRates_DatasourceUploads_DataSourceId",
                        column: x => x.DataSourceId,
                        principalTable: "DatasourceUploads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OxygenSaturations",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Offset = table.Column<string>(type: "text", nullable: true),
                    Max = table.Column<float>(type: "real", nullable: true),
                    Min = table.Column<float>(type: "real", nullable: true),
                    DataSourceId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OxygenSaturations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OxygenSaturations_DatasourceUploads_DataSourceId",
                        column: x => x.DataSourceId,
                        principalTable: "DatasourceUploads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Stress",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Offset = table.Column<string>(type: "text", nullable: true),
                    Score = table.Column<float>(type: "real", nullable: true),
                    DataSourceId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Stress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Stress_DatasourceUploads_DataSourceId",
                        column: x => x.DataSourceId,
                        principalTable: "DatasourceUploads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DaySummaries_DataSourceId",
                table: "DaySummaries",
                column: "DataSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_HeartRates_DataSourceId",
                table: "HeartRates",
                column: "DataSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_OxygenSaturations_DataSourceId",
                table: "OxygenSaturations",
                column: "DataSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Stress_DataSourceId",
                table: "Stress",
                column: "DataSourceId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DaySummaries");

            migrationBuilder.DropTable(
                name: "HeartRates");

            migrationBuilder.DropTable(
                name: "OxygenSaturations");

            migrationBuilder.DropTable(
                name: "Stress");

            migrationBuilder.RenameColumn(
                name: "Score",
                table: "Sleep",
                newName: "SleepScore");

            migrationBuilder.RenameColumn(
                name: "Duration",
                table: "Sleep",
                newName: "SleepDuration");

            migrationBuilder.RenameColumn(
                name: "Cycle",
                table: "Sleep",
                newName: "SleepCycle");
        }
    }
}
