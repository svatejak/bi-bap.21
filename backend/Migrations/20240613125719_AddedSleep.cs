﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace backend.Migrations
{
    /// <inheritdoc />
    public partial class AddedSleep : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sleep",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DataUuid = table.Column<Guid>(type: "uuid", nullable: true),
                    MentalRecovery = table.Column<int>(type: "integer", nullable: true),
                    Factor1 = table.Column<int>(type: "integer", nullable: true),
                    Factor2 = table.Column<int>(type: "integer", nullable: true),
                    Factor3 = table.Column<int>(type: "integer", nullable: true),
                    Factor4 = table.Column<int>(type: "integer", nullable: true),
                    Factor5 = table.Column<int>(type: "integer", nullable: true),
                    Factor6 = table.Column<int>(type: "integer", nullable: true),
                    Factor7 = table.Column<int>(type: "integer", nullable: true),
                    Factor8 = table.Column<int>(type: "integer", nullable: true),
                    Factor9 = table.Column<int>(type: "integer", nullable: true),
                    Factor10 = table.Column<int>(type: "integer", nullable: true),
                    PhysicalRecovery = table.Column<int>(type: "integer", nullable: true),
                    MovementAwakening = table.Column<int>(type: "integer", nullable: true),
                    SleepCycle = table.Column<int>(type: "integer", nullable: true),
                    Efficiency = table.Column<int>(type: "integer", nullable: true),
                    SleepScore = table.Column<int>(type: "integer", nullable: true),
                    SleepDuration = table.Column<int>(type: "integer", nullable: true),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    TimeOffset = table.Column<string>(type: "text", nullable: true),
                    DataSourceId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sleep", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sleep_DatasourceUploads_DataSourceId",
                        column: x => x.DataSourceId,
                        principalTable: "DatasourceUploads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SleepStages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    TimeOffset = table.Column<string>(type: "text", nullable: true),
                    Stage = table.Column<string>(type: "text", nullable: true),
                    SleepId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SleepStages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SleepStages_Sleep_SleepId",
                        column: x => x.SleepId,
                        principalTable: "Sleep",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Sleep_DataSourceId",
                table: "Sleep",
                column: "DataSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_SleepStages_SleepId",
                table: "SleepStages",
                column: "SleepId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SleepStages");

            migrationBuilder.DropTable(
                name: "Sleep");
        }
    }
}
