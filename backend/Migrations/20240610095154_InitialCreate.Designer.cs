﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using backend.Data;

#nullable disable

namespace backend.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20240610095154_InitialCreate")]
    partial class InitialCreate
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("backend.Models.DatasourceUpload", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<string>("DataSource")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<DateTime>("UploadTime")
                        .HasColumnType("timestamp with time zone");

                    b.HasKey("Id");

                    b.ToTable("DatasourceUploads");
                });

            modelBuilder.Entity("backend.Models.Exercise", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long>("DataSourceId")
                        .HasColumnType("bigint");

                    b.Property<Guid?>("DataUuid")
                        .HasColumnType("uuid");

                    b.Property<float?>("DeclineDistance")
                        .HasColumnType("real");

                    b.Property<float?>("Distance")
                        .HasColumnType("real");

                    b.Property<long?>("Duration")
                        .HasColumnType("bigint");

                    b.Property<DateTime?>("EndTime")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("ExerciseName")
                        .HasColumnType("text");

                    b.Property<int?>("HeartRateSampleCount")
                        .HasColumnType("integer");

                    b.Property<float?>("InclineDistance")
                        .HasColumnType("real");

                    b.Property<float?>("MaxAltitude")
                        .HasColumnType("real");

                    b.Property<float?>("MaxCadence")
                        .HasColumnType("real");

                    b.Property<float?>("MaxHeartRate")
                        .HasColumnType("real");

                    b.Property<float?>("MaxSpeed")
                        .HasColumnType("real");

                    b.Property<float?>("MeanCadence")
                        .HasColumnType("real");

                    b.Property<float?>("MeanHeartRate")
                        .HasColumnType("real");

                    b.Property<float?>("MeanSpeed")
                        .HasColumnType("real");

                    b.Property<float?>("MinAltitude")
                        .HasColumnType("real");

                    b.Property<float?>("MinHeartRate")
                        .HasColumnType("real");

                    b.Property<DateTime?>("StartTime")
                        .HasColumnType("timestamp with time zone");

                    b.Property<int?>("StepCount")
                        .HasColumnType("integer");

                    b.Property<string>("TimeOffset")
                        .HasColumnType("text");

                    b.Property<float?>("TotalCalories")
                        .HasColumnType("real");

                    b.HasKey("Id");

                    b.HasIndex("DataSourceId");

                    b.ToTable("Exercises");
                });

            modelBuilder.Entity("backend.Models.ExerciseAdditionalMetric", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<string>("DataType")
                        .HasColumnType("text");

                    b.Property<long>("ExerciseId")
                        .HasColumnType("bigint");

                    b.Property<int?>("Score")
                        .HasColumnType("integer");

                    b.Property<int?>("Score_ratio")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.ToTable("ExerciseAdditionalMetric");
                });

            modelBuilder.Entity("backend.Models.ExerciseAdditionalMetricRecord", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long>("Duration")
                        .HasColumnType("bigint");

                    b.Property<long>("ExerciseAdditionalMetricId")
                        .HasColumnType("bigint");

                    b.Property<int>("Score")
                        .HasColumnType("integer");

                    b.Property<double>("Value")
                        .HasColumnType("double precision");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseAdditionalMetricId");

                    b.ToTable("ExerciseAdditionalMetricRecord");
                });

            modelBuilder.Entity("backend.Models.ExerciseHeartRate", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long>("ExerciseId")
                        .HasColumnType("bigint");

                    b.Property<int?>("HeartRate")
                        .HasColumnType("integer");

                    b.Property<long?>("StartTime")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.ToTable("ExerciseHeartRate");
                });

            modelBuilder.Entity("backend.Models.ExerciseLocationData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<float?>("Accuracy")
                        .HasColumnType("real");

                    b.Property<double?>("Altitude")
                        .HasColumnType("double precision");

                    b.Property<long>("ExerciseId")
                        .HasColumnType("bigint");

                    b.Property<double?>("Latitude")
                        .HasColumnType("double precision");

                    b.Property<double?>("Longitude")
                        .HasColumnType("double precision");

                    b.Property<long?>("StartTime")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.ToTable("ExerciseLocationData");
                });

            modelBuilder.Entity("backend.Models.ExerciseMotionData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<int?>("Cadence")
                        .HasColumnType("integer");

                    b.Property<float?>("Distance")
                        .HasColumnType("real");

                    b.Property<long>("ExerciseId")
                        .HasColumnType("bigint");

                    b.Property<float?>("Speed")
                        .HasColumnType("real");

                    b.Property<long?>("StartTime")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.ToTable("ExerciseMotionData");
                });

            modelBuilder.Entity("backend.Models.ExerciseRecoveryHeartRateData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long?>("ElapsedTime")
                        .HasColumnType("bigint");

                    b.Property<long>("ExerciseId")
                        .HasColumnType("bigint");

                    b.Property<int?>("HeartRate")
                        .HasColumnType("integer");

                    b.Property<long?>("StartTime")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.ToTable("ExerciseRecoveryHeartRate");
                });

            modelBuilder.Entity("backend.Models.ExerciseSubsetData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<int?>("Duration")
                        .HasColumnType("integer");

                    b.Property<long>("ExerciseId")
                        .HasColumnType("bigint");

                    b.Property<int?>("Reps")
                        .HasColumnType("integer");

                    b.Property<float?>("Weight")
                        .HasColumnType("real");

                    b.HasKey("Id");

                    b.HasIndex("ExerciseId");

                    b.ToTable("ExerciseSubsetData");
                });

            modelBuilder.Entity("backend.Models.Exercise", b =>
                {
                    b.HasOne("backend.Models.DatasourceUpload", "DataSource")
                        .WithMany("Exercises")
                        .HasForeignKey("DataSourceId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("DataSource");
                });

            modelBuilder.Entity("backend.Models.ExerciseAdditionalMetric", b =>
                {
                    b.HasOne("backend.Models.Exercise", "Exercise")
                        .WithMany("AdditionalMetrics")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Exercise");
                });

            modelBuilder.Entity("backend.Models.ExerciseAdditionalMetricRecord", b =>
                {
                    b.HasOne("backend.Models.ExerciseAdditionalMetric", "ExerciseAdditionalMetric")
                        .WithMany("Records")
                        .HasForeignKey("ExerciseAdditionalMetricId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ExerciseAdditionalMetric");
                });

            modelBuilder.Entity("backend.Models.ExerciseHeartRate", b =>
                {
                    b.HasOne("backend.Models.Exercise", "Exercise")
                        .WithMany("HeartRate")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Exercise");
                });

            modelBuilder.Entity("backend.Models.ExerciseLocationData", b =>
                {
                    b.HasOne("backend.Models.Exercise", "Exercise")
                        .WithMany("LocationData")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Exercise");
                });

            modelBuilder.Entity("backend.Models.ExerciseMotionData", b =>
                {
                    b.HasOne("backend.Models.Exercise", "Exercise")
                        .WithMany("MotionData")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Exercise");
                });

            modelBuilder.Entity("backend.Models.ExerciseRecoveryHeartRateData", b =>
                {
                    b.HasOne("backend.Models.Exercise", "Exercise")
                        .WithMany("RecoveryHeartRateData")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Exercise");
                });

            modelBuilder.Entity("backend.Models.ExerciseSubsetData", b =>
                {
                    b.HasOne("backend.Models.Exercise", "Exercise")
                        .WithMany("SubsetData")
                        .HasForeignKey("ExerciseId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Exercise");
                });

            modelBuilder.Entity("backend.Models.DatasourceUpload", b =>
                {
                    b.Navigation("Exercises");
                });

            modelBuilder.Entity("backend.Models.Exercise", b =>
                {
                    b.Navigation("AdditionalMetrics");

                    b.Navigation("HeartRate");

                    b.Navigation("LocationData");

                    b.Navigation("MotionData");

                    b.Navigation("RecoveryHeartRateData");

                    b.Navigation("SubsetData");
                });

            modelBuilder.Entity("backend.Models.ExerciseAdditionalMetric", b =>
                {
                    b.Navigation("Records");
                });
#pragma warning restore 612, 618
        }
    }
}
