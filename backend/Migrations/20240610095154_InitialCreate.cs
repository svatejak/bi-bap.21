﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace backend.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DatasourceUploads",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DataSource = table.Column<string>(type: "text", nullable: false),
                    UploadTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DatasourceUploads", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Exercises",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DataUuid = table.Column<Guid>(type: "uuid", nullable: true),
                    StartTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    EndTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    Duration = table.Column<long>(type: "bigint", nullable: true),
                    Distance = table.Column<float>(type: "real", nullable: true),
                    TotalCalories = table.Column<float>(type: "real", nullable: true),
                    ExerciseName = table.Column<string>(type: "text", nullable: true),
                    MaxSpeed = table.Column<float>(type: "real", nullable: true),
                    MeanSpeed = table.Column<float>(type: "real", nullable: true),
                    InclineDistance = table.Column<float>(type: "real", nullable: true),
                    DeclineDistance = table.Column<float>(type: "real", nullable: true),
                    MaxAltitude = table.Column<float>(type: "real", nullable: true),
                    MinAltitude = table.Column<float>(type: "real", nullable: true),
                    MeanHeartRate = table.Column<float>(type: "real", nullable: true),
                    MaxHeartRate = table.Column<float>(type: "real", nullable: true),
                    MinHeartRate = table.Column<float>(type: "real", nullable: true),
                    HeartRateSampleCount = table.Column<int>(type: "integer", nullable: true),
                    MeanCadence = table.Column<float>(type: "real", nullable: true),
                    MaxCadence = table.Column<float>(type: "real", nullable: true),
                    TimeOffset = table.Column<string>(type: "text", nullable: true),
                    StepCount = table.Column<int>(type: "integer", nullable: true),
                    DataSourceId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exercises", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Exercises_DatasourceUploads_DataSourceId",
                        column: x => x.DataSourceId,
                        principalTable: "DatasourceUploads",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseAdditionalMetric",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    DataType = table.Column<string>(type: "text", nullable: true),
                    Score = table.Column<int>(type: "integer", nullable: true),
                    Score_ratio = table.Column<int>(type: "integer", nullable: true),
                    ExerciseId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseAdditionalMetric", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseAdditionalMetric_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseHeartRate",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    HeartRate = table.Column<int>(type: "integer", nullable: true),
                    StartTime = table.Column<long>(type: "bigint", nullable: true),
                    ExerciseId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseHeartRate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseHeartRate_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseLocationData",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartTime = table.Column<long>(type: "bigint", nullable: true),
                    Accuracy = table.Column<float>(type: "real", nullable: true),
                    Altitude = table.Column<double>(type: "double precision", nullable: true),
                    Latitude = table.Column<double>(type: "double precision", nullable: true),
                    Longitude = table.Column<double>(type: "double precision", nullable: true),
                    ExerciseId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseLocationData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseLocationData_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseMotionData",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    StartTime = table.Column<long>(type: "bigint", nullable: true),
                    Cadence = table.Column<int>(type: "integer", nullable: true),
                    Distance = table.Column<float>(type: "real", nullable: true),
                    Speed = table.Column<float>(type: "real", nullable: true),
                    ExerciseId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseMotionData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseMotionData_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseRecoveryHeartRate",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    ElapsedTime = table.Column<long>(type: "bigint", nullable: true),
                    HeartRate = table.Column<int>(type: "integer", nullable: true),
                    StartTime = table.Column<long>(type: "bigint", nullable: true),
                    ExerciseId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseRecoveryHeartRate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseRecoveryHeartRate_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseSubsetData",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Duration = table.Column<int>(type: "integer", nullable: true),
                    Weight = table.Column<float>(type: "real", nullable: true),
                    Reps = table.Column<int>(type: "integer", nullable: true),
                    ExerciseId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseSubsetData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseSubsetData_Exercises_ExerciseId",
                        column: x => x.ExerciseId,
                        principalTable: "Exercises",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExerciseAdditionalMetricRecord",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Duration = table.Column<long>(type: "bigint", nullable: false),
                    Score = table.Column<int>(type: "integer", nullable: false),
                    Value = table.Column<double>(type: "double precision", nullable: false),
                    ExerciseAdditionalMetricId = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExerciseAdditionalMetricRecord", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExerciseAdditionalMetricRecord_ExerciseAdditionalMetric_Exe~",
                        column: x => x.ExerciseAdditionalMetricId,
                        principalTable: "ExerciseAdditionalMetric",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseAdditionalMetric_ExerciseId",
                table: "ExerciseAdditionalMetric",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseAdditionalMetricRecord_ExerciseAdditionalMetricId",
                table: "ExerciseAdditionalMetricRecord",
                column: "ExerciseAdditionalMetricId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseHeartRate_ExerciseId",
                table: "ExerciseHeartRate",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseLocationData_ExerciseId",
                table: "ExerciseLocationData",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseMotionData_ExerciseId",
                table: "ExerciseMotionData",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseRecoveryHeartRate_ExerciseId",
                table: "ExerciseRecoveryHeartRate",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_ExerciseSubsetData_ExerciseId",
                table: "ExerciseSubsetData",
                column: "ExerciseId");

            migrationBuilder.CreateIndex(
                name: "IX_Exercises_DataSourceId",
                table: "Exercises",
                column: "DataSourceId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExerciseAdditionalMetricRecord");

            migrationBuilder.DropTable(
                name: "ExerciseHeartRate");

            migrationBuilder.DropTable(
                name: "ExerciseLocationData");

            migrationBuilder.DropTable(
                name: "ExerciseMotionData");

            migrationBuilder.DropTable(
                name: "ExerciseRecoveryHeartRate");

            migrationBuilder.DropTable(
                name: "ExerciseSubsetData");

            migrationBuilder.DropTable(
                name: "ExerciseAdditionalMetric");

            migrationBuilder.DropTable(
                name: "Exercises");

            migrationBuilder.DropTable(
                name: "DatasourceUploads");
        }
    }
}
