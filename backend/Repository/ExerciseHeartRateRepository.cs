using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class ExerciseHeartRateRepository : IExerciseHeartRateRepository
    {
        private readonly AppDbContext _context;
        
        public ExerciseHeartRateRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<ExerciseHeartRate>> GetExerciseHeartRate(long exerciseId)
        {
            return await _context.ExerciseHeartRate
                .Where(x => x.Exercise.Id == exerciseId)
                .ToListAsync();
        }
    }
}