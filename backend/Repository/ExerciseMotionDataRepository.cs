using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class ExerciseMotionDataRepository : IExerciseMotionDataRepository
    {
        private readonly AppDbContext _context;
        
        public ExerciseMotionDataRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<ExerciseMotionData>> GetExerciseMotionData(long exerciseId)
        {
            return await _context.ExerciseMotionData
                .Where(x => x.Exercise.Id == exerciseId)
                .ToListAsync();
        }
    }
}