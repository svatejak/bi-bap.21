using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class ExerciseRecoveryHeartRateDataRepository : IExerciseRecoveryHeartRateDataRepository
    {
        private readonly AppDbContext _context;
        
        public ExerciseRecoveryHeartRateDataRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<ExerciseRecoveryHeartRateData>> GetExerciseRecoveryHeartRateData(long exerciseId)
        {
            return await _context.ExerciseRecoveryHeartRate
                .Where(x => x.Exercise.Id == exerciseId)
                .ToListAsync();
        }
    }
}