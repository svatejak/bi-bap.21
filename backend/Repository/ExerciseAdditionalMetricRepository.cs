using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;
using backend.Models.Dto;


namespace backend.Repository
{
    public class ExerciseAdditionalMetricRepository : IExerciseAdditionalMetricRepository
    {
        private readonly AppDbContext _context;
        
        public ExerciseAdditionalMetricRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<ExerciseAdditionalMetric>> GetExerciseAdditionalMetric(long exerciseId)
        {
            return await _context.ExerciseAdditionalMetric
                .Include(x => x.Records)
                .Where(x => x.Exercise.Id == exerciseId)
                .ToListAsync();
        }

        public async Task<IEnumerable<ExerciseAdditionalMetricDto>>  GetExerciseAdditionalMetricDto(long exerciseId)
        {
            var exerciseAdditionalMetricList = await GetExerciseAdditionalMetric(exerciseId);

            var additionalMetricDtoList = new List<ExerciseAdditionalMetricDto>();

            foreach (var exerciseAdditionalMetric in exerciseAdditionalMetricList)
            {
                var additionalMetricDto = new ExerciseAdditionalMetricDto
                {
                    Id = exerciseAdditionalMetric.Id,
                    DataType = exerciseAdditionalMetric.DataType,
                    Score = exerciseAdditionalMetric.Score,
                    Score_ratio = exerciseAdditionalMetric.Score_ratio,
                    Records = exerciseAdditionalMetric.Records != null ? exerciseAdditionalMetric.Records
                        .Select(x => new ExerciseAdditionalMetricRecordDto
                        {
                            Id = x.Id,
                            Duration = x.Duration,
                            Score = x.Score,
                            Value = x.Value
                        }).ToList() : new List<ExerciseAdditionalMetricRecordDto>()
                };

                additionalMetricDtoList.Add(additionalMetricDto);

            }

            return additionalMetricDtoList;
        }
    }
}