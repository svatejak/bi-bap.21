using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class SleepStageRepository : ISleepStageRepository
    {
        private readonly AppDbContext _context;
        public SleepStageRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SleepStage>> GetSleepStageList(long sleepId)
        {
            return await _context.SleepStages
            .Where(x => x.Sleep.Id == sleepId)
            .OrderBy(s => s.StartTime)
            .ToListAsync();
        }

    }
}