using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class ExerciseLocationDataRepository : IExerciseLocationDataRepository
    {
        private readonly AppDbContext _context;
        
        public ExerciseLocationDataRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<ExerciseLocationData>> GetExerciseLocationData(long exerciseId)
        {
            return await _context.ExerciseLocationData
                .Where(x => x.Exercise.Id == exerciseId)
                .ToListAsync();
        }
    }
}