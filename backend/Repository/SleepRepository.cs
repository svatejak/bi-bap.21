using backend.Models;
using backend.Data;
using backend.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace backend.Repository
{
    public class SleepRepository : ISleepRepository
    {
        private readonly AppDbContext _context;
        public SleepRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Sleep>> GetSleepList()
        {
            return await _context.Sleep
            .Include(e => e.DataSource)
            .OrderBy(s => s.StartTime)
            .ToListAsync();
        }

        public async Task<Sleep?> GetSleepById(int id)
        {
            return await _context.Sleep
            .Include(e => e.DataSource)
            .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}

