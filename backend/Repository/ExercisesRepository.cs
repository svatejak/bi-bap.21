using backend.Models;
using backend.Data;
using backend.Interfaces;
using backend.Models.Dto;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backend.Repository
{
    public class ExercisesRepository : IExercisesRepository
    {
        private readonly AppDbContext _context;
        
        public ExercisesRepository(AppDbContext context)
        {
            _context = context;
        }
        
        public async Task<IEnumerable<Exercise>> GetExercises()
        {
            return await _context.Exercises.ToListAsync();
        }

        public async Task<IEnumerable<Exercise>> GetExercisesSorted(string sortBy, bool ascending, string filter)
        {
            IQueryable<Exercise> exercisesQuery = _context.Exercises
                                    .Include(e => e.SubsetData)
                                    .Include(e => e.DataSource)
                                    .Include(e => e.LocationData);

            if (!string.IsNullOrEmpty(filter))
            {
                exercisesQuery = exercisesQuery.Where(exercise => exercise.DataSource.DataSource == filter);
            }

            if (ascending)
            {
                exercisesQuery = sortBy.ToLower() switch
                {
                    "start_time" => exercisesQuery.OrderBy(exercise => exercise.StartTime),
                    "exercise_name" => exercisesQuery.OrderBy(exercise => exercise.ExerciseName),
                    "datasource" => exercisesQuery.OrderBy(exercise => exercise.DataSource.DataSource),
                    _ => exercisesQuery.OrderBy(exercise => exercise.StartTime),
                };
            }
            else
            {
                exercisesQuery = sortBy.ToLower() switch
                {
                    "start_time" => exercisesQuery.OrderByDescending(exercise => exercise.StartTime),
                    "exercise_name" => exercisesQuery.OrderByDescending(exercise => exercise.ExerciseName),
                    "datasource" => exercisesQuery.OrderByDescending(exercise => exercise.DataSource.DataSource),
                    _ => exercisesQuery.OrderByDescending(exercise => exercise.StartTime),
                };
            }

            return await exercisesQuery.ToListAsync();
        }

        public async Task<Exercise?> GetExercise(long exerciseId)
        {
            return await _context.Exercises
                .Include(e => e.DataSource)
                .Include(e => e.SubsetData)
                .FirstOrDefaultAsync(x => x.Id == exerciseId);
        }


        public async Task<ExerciseDto?> GetExerciseDto(long exerciseId)
        {
            var exercise = await GetExercise(exerciseId);
            if (exercise == null)
            {
                return null;
            }

            bool hasLocationData = await _context.ExerciseLocationData
                .AnyAsync(ld => ld.Exercise.Id == exerciseId);

            bool hasAdditionalMetrics = await _context.ExerciseAdditionalMetric
                .AnyAsync(am => am.Exercise.Id == exerciseId);

            bool hasMotionData = await _context.ExerciseMotionData
                .AnyAsync(md => md.Exercise.Id == exerciseId);

            bool hasRecoveryHeartRateData = await _context.ExerciseRecoveryHeartRate
                .AnyAsync(rhr => rhr.Exercise.Id == exerciseId);

            bool hasHeartRateData = await _context.ExerciseHeartRate
                .AnyAsync(hr => hr.Exercise.Id == exerciseId);

            var exerciseDto = new ExerciseDto
            {
                Id = exercise.Id,
                StartTime = exercise.StartTime,
                EndTime = exercise.EndTime,
                Duration = exercise.Duration,
                Distance = exercise.Distance,
                TotalCalories = exercise.TotalCalories,
                ExerciseName = exercise.ExerciseName,
                MaxSpeed = exercise.MaxSpeed,
                MeanSpeed = exercise.MeanSpeed,
                InclineDistance = exercise.InclineDistance,
                DeclineDistance = exercise.DeclineDistance,
                MaxAltitude = exercise.MaxAltitude,
                MinAltitude = exercise.MinAltitude,
                MeanHeartRate = exercise.MeanHeartRate,
                MaxHeartRate = exercise.MaxHeartRate,
                MinHeartRate = exercise.MinHeartRate,
                HeartRateSampleCount = exercise.HeartRateSampleCount,
                MeanCadence = exercise.MeanCadence,
                MaxCadence = exercise.MaxCadence,
                TimeOffset = exercise.TimeOffset,
                StepCount = exercise.StepCount,
                DataSource = new DatasourceUploadDto
                {
                    Id = exercise.DataSource.Id,
                    DataSource = exercise.DataSource.DataSource,
                    UploadTime = exercise.DataSource.UploadTime
                },
                SubsetData = exercise.SubsetData != null ? 
                    exercise.SubsetData.Select(subsetData => new SubsetDataDto
                    {
                        Id = subsetData.Id,
                        Duration = subsetData.Duration,
                        Weight = subsetData.Weight,
                        Reps = subsetData.Reps
                    }).ToList() 
                    : new List<SubsetDataDto>(),
                HasLocationData = hasLocationData,
                HasAdditionalMetrics = hasAdditionalMetrics,
                HasMotionData = hasMotionData,
                HasRecoveryHeartRateData = hasRecoveryHeartRateData,
                HasHeartRateData = hasHeartRateData,
            };

            return exerciseDto;
        }
    }
}
