namespace backend.Models
{
    public class Stress
    {
        public long Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string? Offset { get; set; }
        public float? Score { get; set; }
        public required DatasourceUpload DataSource { get; set; } 
    }
}