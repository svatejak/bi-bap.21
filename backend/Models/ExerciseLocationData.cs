namespace backend.Models
{
    public class ExerciseLocationData
    {
        public long Id { get; set; }
        public long? StartTime { get; set; } // Epoch time in milliseconds.
        public float? Accuracy { get; set; } // Accuracy of the location in meters.
        public double? Altitude { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public required Exercise Exercise { get; set; } // Reference to the exercise.
    }
}