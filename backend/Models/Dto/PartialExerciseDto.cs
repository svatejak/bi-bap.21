
namespace backend.Models.Dto
{
    public class PartialExerciseDto
    {
        public required long Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string? ExerciseName { get; set; }
        public required DatasourceUploadDto DataSource { get; set; }

        public PositionDto? Position { get; set; }
    }

    public class DatasourceUploadDto
    {
        public long Id { get; set; }
        public required string DataSource { get; set; }
        public DateTime UploadTime { get; set; }
    }

    public class PositionDto
    {
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
