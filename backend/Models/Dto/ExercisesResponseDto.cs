namespace backend.Models.Dto
{
    public class ExercisesResponse
    {
        public int NumFound { get; set; }
        public required List<PartialExerciseDto> Exercises { get; set; }
    }
}
