
namespace backend.Models.Dto
{
    public class ExerciseAdditionalMetricDto
    {
        public long Id { get; set; }
        public string? DataType { get; set; }
        public int? Score { get; set; }
        public int? Score_ratio { get; set; }
        public List<ExerciseAdditionalMetricRecordDto>? Records { get; set; }
    }

    public class ExerciseAdditionalMetricRecordDto
    {
        public long Id { get; set; }
        public long Duration { get; set; }
        public int Score { get; set; }
        public double Value { get; set; }
    }
}