namespace backend.Models
{
    public class SleepStage
    {
        public int Id { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string? TimeOffset { get; set; }

        public string? Stage { get; set; }

        public required Sleep Sleep { get; set; }
    }
}