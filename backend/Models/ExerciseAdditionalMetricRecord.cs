namespace backend.Models
{
    public class ExerciseAdditionalMetricRecord
    {
        public long Id { get; set; }
        public required long Duration { get; set; }
        public required int Score { get; set; }
        public required double Value { get; set; }

        public required ExerciseAdditionalMetric ExerciseAdditionalMetric { get; set; }
    }
}