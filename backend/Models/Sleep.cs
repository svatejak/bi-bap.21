namespace backend.Models
{
    public class Sleep
    {
        public int Id { get; set; }

        public Guid? DataUuid { get; set; }
        public float? MentalRecovery { get; set; }
        public int? Factor1 { get; set; }
        public int? Factor2 { get; set; }
        public int? Factor3 { get; set; }
        public int? Factor4 { get; set; }
        public int? Factor5 { get; set; }
        public int? Factor6 { get; set; }
        public int? Factor7 { get; set; }
        public int? Factor8 { get; set; }
        public int? Factor9 { get; set; }
        public int? Factor10 { get; set; }
        public float? PhysicalRecovery { get; set; }
        public float? MovementAwakening { get; set; }
        public int? Cycle { get; set; }
        public float? Efficiency { get; set; }
        public int? Score { get; set; }
        public int? Duration { get; set; }
        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public string? TimeOffset { get; set; }

        public required DatasourceUpload DataSource { get; set; }

        // public ICollection<SleepStage>? SleepStages { get; set; }
    }
}


