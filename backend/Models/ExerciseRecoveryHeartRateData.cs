namespace backend.Models
{
    public class ExerciseRecoveryHeartRateData
    {
        public long Id { get; set; }
        public long? ElapsedTime { get; set; } // Elapsed time in milliseconds.
        public int? HeartRate { get; set; } // Heart rate in beats per minute.
        public long? StartTime { get; set; } // Epoch time in milliseconds.
        public required Exercise Exercise { get; set; } // Reference to the exercise.
    }
}