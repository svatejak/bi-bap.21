namespace backend.Models
{
    public class DaySummary
    {
        public long Id { get; set; }
        public long? Date { get; set; }
        public float? Calories { get; set; }
        public long? StepCount { get; set; }
        public long? ActiveTime { get; set; }
        public int? MaxHeartRate { get; set; }
        public int? MinHeartRate { get; set; }
        public int? AverageHeartRate { get; set; }
        public float? Distance { get; set; }
        public required DatasourceUpload DataSource { get; set; } 
    }
}