namespace backend.Models
{
    public class ExerciseMotionData
    {
        public long Id { get; set; }
        public long? StartTime { get; set; } // Epoch time in milliseconds.
        public int? Cadence { get; set; }
        public float? Distance { get; set; }
        public float? Speed { get; set; }

        public required Exercise Exercise { get; set; } // Reference to the exercise.
    }
}