namespace backend.Models
{
    public class DatasourceUpload
    {
        public long Id { get; set; }
        public required string DataSource { get; set; }
        public DateTime UploadTime { get; set; }
    }
}