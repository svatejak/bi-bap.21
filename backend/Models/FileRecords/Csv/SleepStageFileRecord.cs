using CsvHelper.Configuration.Attributes;

namespace backend.Models.FileRecords.Csv
{
    public class SleepStageFileRecord
    {
        [Name("sleep_id")]
        public Guid? SleepGuid { get; set; }

        [Name("start_time")]
        public DateTime? StartTime { get; set; }

        [Name("end_time")]
        public DateTime? EndTime { get; set; }

        [Name("time_offset")]
        public string? TimeOffset { get; set; }

        [Name("stage")]
        public int? Stage { get; set; }
    }
}

