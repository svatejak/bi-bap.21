using CsvHelper.Configuration.Attributes;
using System;

namespace backend.Models.FileRecords.Csv
{
    public class SleepFileRecord
    {
        [Name("com.samsung.health.sleep.datauuid")]
        public Guid DataUuid { get; set; }

        [Name("mental_recovery")]
        public float? MentalRecovery { get; set; }

        [Name("factor_01")]
        public int? Factor1 { get; set; }

        [Name("factor_02")]
        public int? Factor2 { get; set; }

        [Name("factor_03")]
        public int? Factor3 { get; set; }

        [Name("factor_04")]
        public int? Factor4 { get; set; }

        [Name("factor_05")]
        public int? Factor5 { get; set; }

        [Name("factor_06")]
        public int? Factor6 { get; set; }

        [Name("factor_07")]
        public int? Factor7 { get; set; }

        [Name("factor_08")]
        public int? Factor8 { get; set; }

        [Name("factor_09")]
        public int? Factor9 { get; set; }

        [Name("factor_10")]
        public int? Factor10 { get; set; }

        [Name("physical_recovery")]
        public float? PhysicalRecovery { get; set; }

        [Name("movement_awakening")]
        public float? MovementAwakening { get; set; }

        [Name("sleep_cycle")]
        public int? SleepCycle { get; set; }

        [Name("efficiency")]
        public float? Efficiency { get; set; }

        [Name("sleep_score")]
        public int? SleepScore { get; set; }

        [Name("sleep_duration")]
        public int? SleepDuration { get; set; }

        [Name("com.samsung.health.sleep.start_time")]
        public DateTime? StartTime { get; set; }

        [Name("com.samsung.health.sleep.end_time")]
        public DateTime? EndTime { get; set; }

        [Name("com.samsung.health.sleep.time_offset")]
        public string? TimeOffset { get; set; }
    }
}