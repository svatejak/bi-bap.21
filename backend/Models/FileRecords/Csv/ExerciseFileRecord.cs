using CsvHelper.Configuration.Attributes;

namespace backend.Models.FileRecords.Csv
{
    public class ExerciseFileRecord
    {
        [Name("com.samsung.health.exercise.datauuid")]
        public Guid DataUuid { get; set; }

        [Name("com.samsung.health.exercise.start_time")]
        public DateTime? StartTime { get; set; } // Start time of the exercise. format: yyyy-MM-dd HH:mm:ss.SSS

        [Name("com.samsung.health.exercise.end_time")]
        public DateTime? EndTime { get; set; } // End time of the exercise. format: yyyy-MM-dd HH:mm:ss.SSS

        [Name("com.samsung.health.exercise.duration")]
        public long? Duration { get; set; } // Duration of this exercise in milliseconds.

        [Name("com.samsung.health.exercise.distance")]
        public float? Distance { get; set; } // Distance covered during the exercise in meters.

        [Name("com.samsung.health.exercise.calorie")]
        public float? TotalCalories { get; set; } // Burned calorie during the activity in kilocalories.

        [Name("com.samsung.health.exercise.exercise_type")]
        public string? ExerciseName { get; set; } // Exercise name, created from the activity type.

        [Name("com.samsung.health.exercise.max_speed")]
        public float? MaxSpeed { get; set; } // Maximum speed in meters per second.

        [Name("com.samsung.health.exercise.mean_speed")]
        public float? MeanSpeed { get; set; } // Mean speed in meters per second.

        [Name("com.samsung.health.exercise.incline_distance")]
        public float? InclineDistance { get; set; } // Uphill distance during the activity in meters.

        [Name("com.samsung.health.exercise.decline_distance")]
        public float? DeclineDistance { get; set; } // Downhill distance during the activity in meters.

        [Name("com.samsung.health.exercise.max_altitude")]
        public float? MaxAltitude { get; set; } // Maximum altitude in meters.

        [Name("com.samsung.health.exercise.min_altitude")]
        public float? MinAltitude { get; set; } // Minimum altitude in meters.

        [Name("com.samsung.health.exercise.mean_heart_rate")]
        public float? MeanHeartRate { get; set; } // Mean heart rate per minute.

        [Name("com.samsung.health.exercise.max_heart_rate")]
        public float? MaxHeartRate { get; set; } // Maximum heart rate per minute.

        [Name("com.samsung.health.exercise.min_heart_rate")]
        public float? MinHeartRate { get; set; } // Minimum heart rate per minute.

        [Name("heart_rate_sample_count")]
        public int? HeartRateSampleCount { get; set; } // Number of heart rate samples.

        [Name("com.samsung.health.exercise.mean_cadence")]
        public float? MeanCadence { get; set; } // Mean cadence rate per minute.

        [Name("com.samsung.health.exercise.max_cadence")]
        public float? MaxCadence { get; set; } // Maximum cadence rate per minute.

        [Name("com.samsung.health.exercise.time_offset")]
        public string? TimeOffset { get; set; }

        [Name("subset_data")]
        public string? SubsetData { get; set; }

        [Name("com.samsung.health.exercise.live_data")]
        public string? LiveData { get; set; }

        [Name("additional_internal")]
        public string? AdditionalInternal { get; set; }

        [Name("com.samsung.health.exercise.location_data")]
        public string? LocationData { get; set; }

        [Name("com.samsung.health.exercise.count")]
        public int? StepCount { get; set; } // Number of steps taken during the exercise.
    }
}