using CsvHelper.Configuration.Attributes;

namespace backend.Models.FileRecords.Csv
{
    public class RecoveryHeartRateRecord
    {
        [Name("exercise_id")]
        public Guid ExerciseId { get; set; }
        [Name("datauuid")]
        public string? FileName { get; set; }
    }
}