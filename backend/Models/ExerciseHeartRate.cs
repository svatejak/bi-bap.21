namespace backend.Models
{
    public class ExerciseHeartRate
    {
        public long Id { get; set; }
        public int? HeartRate { get; set; } // Heart rate in beats per minute.
        public long? StartTime { get; set; } // Epoch time in milliseconds.
        public required Exercise Exercise { get; set; } // Reference to the exercise.
    }
}