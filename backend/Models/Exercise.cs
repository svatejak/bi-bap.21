namespace backend.Models
{
    public class Exercise
    {
        public long Id { get; set; }

        public Guid? DataUuid { get; set; } // SamsungHealth original identifier.
        public DateTime? StartTime { get; set; } // Start time of the exercise. format: yyyy-MM-dd HH:mm:ss.SSS
        public DateTime? EndTime { get; set; } // End time of the exercise. format: yyyy-MM-dd HH:mm:ss.SSS
        public long? Duration { get; set; } // Duration of this exercise in milliseconds.
        public float? Distance { get; set; } // Distance covered during the exercise in meters.
        public float? TotalCalories { get; set; } // Burned calorie during the activity in kilocalories.
        public string? ExerciseName { get; set; } // Exercise name, created from the activity type.
        public float? MaxSpeed { get; set; } // Maximum speed in meters per second.
        public float? MeanSpeed { get; set; } // Mean speed in meters per second.
        public float? InclineDistance { get; set; } // Uphill distance during the activity in meters.
        public float? DeclineDistance { get; set; } // Downhill distance during the activity in meters.
        public float? MaxAltitude { get; set; } // Maximum altitude in meters.
        public float? MinAltitude { get; set; } // Minimum altitude in meters.
        public float? MeanHeartRate { get; set; } // Mean heart rate per minute.
        public float? MaxHeartRate { get; set; } // Maximum heart rate per minute.
        public float? MinHeartRate { get; set; } // Minimum heart rate per minute.
        public int? HeartRateSampleCount { get; set; } // Number of heart rate samples.
        public float? MeanCadence { get; set; } // Mean cadence rate per minute.
        public float? MaxCadence { get; set; } // Maximum cadence rate per minute.
        public string? TimeOffset { get; set; }

        public int? StepCount { get; set; } // Number of steps taken during the exercise.
        public required DatasourceUpload DataSource { get; set; } 

        public ICollection<ExerciseSubsetData>? SubsetData { get; set; }

        public ICollection<ExerciseLocationData>? LocationData { get; set; }

        // public ICollection<ExerciseHeartRate>? HeartRate{ get; set; }

        // public ICollection<ExerciseAdditionalMetric>? AdditionalMetrics { get; set; }

        // public ICollection<ExerciseMotionData>? MotionData { get; set; }

        // public ICollection<ExerciseRecoveryHeartRateData>? RecoveryHeartRateData { get; set; }
    }
}
