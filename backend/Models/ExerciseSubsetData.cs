namespace backend.Models
{
    public class ExerciseSubsetData
    {
        public long Id { get; set; }

        public int? Duration { get; set; } // Duration of this subset in seconds.
        public float? Weight { get; set; } // Weight in kilograms.

        public int? Reps { get; set; } // Number of repetitions.
        public required Exercise Exercise { get; set; }
    }
}