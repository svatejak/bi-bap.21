namespace backend.Models
{
    public class FileMetadata
    {
        public required string Name { get; set; }
        public required string Type { get; set; }
        public required string DataSource { get; set; }
    }
}