namespace backend.Models
{
    public class ExerciseAdditionalMetric
    {
        public long Id { get; set; }
        public string? DataType { get; set; }
        public int? Score { get; set; }
        public int? Score_ratio { get; set; }

        public required Exercise Exercise { get; set; } // Reference to the exercise.

        public ICollection<ExerciseAdditionalMetricRecord>? Records { get; set; }
    }
}