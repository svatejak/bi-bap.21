namespace backend.Models
{
    public class OxygenSaturation
    {
        public long Id { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public string? Offset { get; set; }
        public float? Max { get; set; }
        public float? Min { get; set; }
        public required DatasourceUpload DataSource { get; set; } 
    }
}