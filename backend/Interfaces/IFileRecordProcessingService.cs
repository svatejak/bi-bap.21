using backend.Models;
using backend.Models.FileRecords.Csv;

namespace backend.Interfaces
{
    public interface IFileRecordProcessingService
    {
        public List<Exercise> GetExerciseRecords(IEnumerable<ExerciseFileRecord> exerciseFileRecords, DatasourceUpload datasourceUpload);
        public List<ExerciseSubsetData> GetExerciseSubsetData(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises);
        public List<ExerciseLocationData> GetExerciseLocationData(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises );
        public List<ExerciseAdditionalMetric> GetExerciseAdditionalMetrics(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises);
        public Tuple<List<ExerciseHeartRate>, List<ExerciseMotionData>> GetExerciseLiveData(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises);
        public List<ExerciseRecoveryHeartRateData> GetRecoveryHeartRateData(IEnumerable<RecoveryHeartRateRecord> recoveryHeartRateFileRecords, List<Exercise> exercises);

        public Exercise GetExerciseFromGoogleFitFile(string fileName, DatasourceUpload datasourceUpload);

        public void GetExerciseAdditionalData(string fileName, Exercise exercise, List<ExerciseHeartRate> heartRateData, List<ExerciseLocationData> locationData);

        public List<Sleep> GetSleepRecords(IEnumerable<SleepFileRecord> sleepFileRecords, DatasourceUpload datasourceUpload);

        public List<SleepStage> GetSleepStageRecords(IEnumerable<SleepStageFileRecord> sleepStageFileRecords, List<Sleep> sleepData);
    }
}