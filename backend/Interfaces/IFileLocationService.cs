


namespace backend.Interfaces
{
    public interface IFileLocationService
    {
        public bool SamsungHealthHasDirectories();
        public bool GoogleFitHasDirectories();
        public string[] GetSamsungHealthFiles();
        public string? GetSamsungHealthCSVFilePath(IEnumerable<string> files, string pattern);
        public string GetSamsungHealthJSONFilePath(string fileName, string folderName);

        public string[] GetGoogleFitFilePaths(string pattern);

        public string GetGoogleFitActivityFilePath(string sessionPath, string[] allActivitiesPaths);
    }
}