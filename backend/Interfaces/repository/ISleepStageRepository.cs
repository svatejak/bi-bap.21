using backend.Models;
using backend.Models.Dto;

namespace backend.Interfaces
{
    public interface ISleepStageRepository
    {
        Task<IEnumerable<SleepStage>> GetSleepStageList(long sleepId);
    }
}