using backend.Models;
using backend.Models.Dto;

namespace backend.Interfaces
{
    public interface IExercisesRepository
    {
        public Task<IEnumerable<Exercise>> GetExercises();

        public Task<IEnumerable<Exercise>> GetExercisesSorted(string sortBy, bool ascending, string filter);

        public Task<Exercise?> GetExercise(long exerciseId);

        public Task<ExerciseDto?> GetExerciseDto(long exerciseId);
    }
}
