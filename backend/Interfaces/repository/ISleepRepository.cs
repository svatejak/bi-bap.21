using backend.Models;
using backend.Models.Dto;

namespace backend.Interfaces
{
    public interface ISleepRepository
    {
        Task<IEnumerable<Sleep>> GetSleepList();
        Task<Sleep?> GetSleepById(int id);
    }
}