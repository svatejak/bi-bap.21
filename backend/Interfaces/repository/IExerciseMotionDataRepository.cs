using backend.Models;

namespace backend.Interfaces
{
    public interface IExerciseMotionDataRepository
    {
        public Task<IEnumerable<ExerciseMotionData>> GetExerciseMotionData(long exerciseId);
    }
}