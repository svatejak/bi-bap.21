using backend.Models;

namespace backend.Interfaces
{
    public interface IExerciseHeartRateRepository
    {
        public Task<IEnumerable<ExerciseHeartRate>> GetExerciseHeartRate(long exerciseId);
    }
}
