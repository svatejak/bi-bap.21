using backend.Models;
using backend.Models.Dto;

namespace backend.Interfaces
{
    public interface IExerciseAdditionalMetricRepository
    {
        public Task<IEnumerable<ExerciseAdditionalMetric>> GetExerciseAdditionalMetric(long exerciseId);

        public Task<IEnumerable<ExerciseAdditionalMetricDto>>  GetExerciseAdditionalMetricDto(long exerciseId);
    }
}