using backend.Models;

namespace backend.Interfaces
{
    public interface IExerciseLocationDataRepository
    {
        public Task<IEnumerable<ExerciseLocationData>> GetExerciseLocationData(long exerciseId);
    }
}