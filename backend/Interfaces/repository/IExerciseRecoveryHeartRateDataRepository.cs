using backend.Models;

namespace backend.Interfaces
{
    public interface IExerciseRecoveryHeartRateDataRepository
    {
        public Task<IEnumerable<ExerciseRecoveryHeartRateData>> GetExerciseRecoveryHeartRateData(long exerciseId);
    }
}