using System;
using System.Threading.Tasks;
using backend.Models;


namespace backend.Interfaces;
public interface IMinioService
{
    public void ListBuckets();
    public void ListObjects(string bucketName);
    public Task GetObjectsToFile(IEnumerable<FileMetadata> files, string localDirectoryPath, string bucketName = "default");

    public void CleanBucket(string bucketName="default");
    public Task<bool> ContainsObject(string objectName, string bucketName = "default");
}