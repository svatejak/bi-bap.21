

namespace backend.Interfaces
{
    public interface IDatabaseService
    {
        void BulkInsertion<T>(IEnumerable<T> records) where T : class;
    }
}