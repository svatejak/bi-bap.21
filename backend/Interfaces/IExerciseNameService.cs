


namespace backend.Interfaces
{
    public interface ICodeToNameService
    {
        public string GetExerciseName(string? exerciseType);

        public string GetSleepStageName(int? stage);
    }
}