

namespace backend.Interfaces
{
    public interface IProcessGoogleFitDataService
    {
        void ValidateData();

        void ProcessData();
    }
}