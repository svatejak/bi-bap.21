
using backend.Models;
using Microsoft.AspNetCore.Mvc;

namespace backend.Interfaces;
public interface IDataProcessingService
{
    public IActionResult ValidateFilesMetadata(List<FileMetadata> files);


    /// <summary>
    /// Sets up file processing for the given list of files by extracting them and organizing into directories.
    /// </summary>
    /// <param name="files">The list of metadata for the files to be processed.</param>
    /// <returns>A task representing the asynchronous operation of setting up file processing.</returns>
    public Task<IActionResult> SetupFileProcessing(List<FileMetadata> files);

    public IActionResult ValidateFiles();

    public IActionResult ProcessFiles();
}