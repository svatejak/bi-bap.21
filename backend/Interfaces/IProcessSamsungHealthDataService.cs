
namespace backend.Interfaces
{
    public interface IProcessSamsungHealthDataService
    {
        public void ValidateData();
        public void ProcessData();
    }
}