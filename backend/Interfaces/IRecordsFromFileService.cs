


namespace backend.Interfaces
{
    public interface IRecordsFromFileService
    {
        public IEnumerable<T> GetRecordsFromCsv<T>(string filePath) where T : class, new();

        public dynamic GetRecordsFromJson(string filePath);
    }
}