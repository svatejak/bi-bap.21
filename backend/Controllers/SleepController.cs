using Microsoft.AspNetCore.Mvc;
using backend.Models;
using backend.Interfaces;
using backend.Models.Dto;


namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SleepController : ControllerBase
    {
        private readonly ISleepRepository _sleepRepository;
        public SleepController(ISleepRepository sleepRepository)
        {
            _sleepRepository = sleepRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Sleep>))]
        public async Task<IActionResult> GetSleepList()
        {
            var sleepList = await _sleepRepository.GetSleepList();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(sleepList);
        }


        [HttpGet("{sleepId}")]
        [ProducesResponseType(200, Type = typeof(Sleep))]
        public async Task<IActionResult> GetSleepById(int sleepId)
        {
            var sleep = await _sleepRepository.GetSleepById(sleepId);

            if (sleep == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(sleep);
        }
    }
}   