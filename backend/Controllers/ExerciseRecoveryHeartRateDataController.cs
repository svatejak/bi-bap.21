using backend.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/exercises/{exerciseId}/recovery-heart-rate")]
    [ApiController]
    public class ExerciseRecoveryHeartRateDataController : ControllerBase
    {
        private readonly IExerciseRecoveryHeartRateDataRepository _recoveryHeartRateDataRepository;

        public ExerciseRecoveryHeartRateDataController(IExerciseRecoveryHeartRateDataRepository recoveryHeartRateDataRepository)
        {
            _recoveryHeartRateDataRepository = recoveryHeartRateDataRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetExerciseRecoveryHeartRateData(long exerciseId)
        {
            var recoveryHeartRateData = await _recoveryHeartRateDataRepository.GetExerciseRecoveryHeartRateData(exerciseId);
            if (recoveryHeartRateData == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(recoveryHeartRateData);
        }
    }
}