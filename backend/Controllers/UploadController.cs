using backend.Interfaces;
using Microsoft.AspNetCore.Mvc;
using backend.Models;


namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IDataProcessingService _dataProcessingService;

        public UploadController(IDataProcessingService dataProcessingService)
        {
            _dataProcessingService = dataProcessingService;
        }

        [HttpPost()]
        public async Task<IActionResult> UploadFile([FromBody] List<FileMetadata> files)
        {
            // validate metadata
            var metadataValidationResult = _dataProcessingService.ValidateFilesMetadata(files);
            if (metadataValidationResult is BadRequestObjectResult badRequestMetadata)
            {
                return BadRequest(new { message = badRequestMetadata.Value });
            }

            // setup file processing
            var setupResult = await _dataProcessingService.SetupFileProcessing(files);
            if (setupResult is ObjectResult objectResult && objectResult.StatusCode == 500)
            {
                return StatusCode(500, new { message = objectResult.Value });
            }

            // validate files
            var fileValidationResult = _dataProcessingService.ValidateFiles();
            if (fileValidationResult is BadRequestObjectResult badRequestValidation)
            {
                return BadRequest(new { message = badRequestValidation.Value });
            }

            // process files
            var processingResult =  _dataProcessingService.ProcessFiles();
            if (processingResult is BadRequestObjectResult badRequestProcessing)
            {
                return BadRequest(new { message = badRequestProcessing.Value });
            }

            return Ok(new { message = "Files uploaded" });
        }
    }
}
