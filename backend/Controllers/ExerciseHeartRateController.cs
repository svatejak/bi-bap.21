using backend.Interfaces;
using Microsoft.AspNetCore.Mvc;



namespace backend.Controllers
{
    [Route("api/exercises/{exerciseId}/heart-rate")]
    [ApiController]
    public class ExerciseHeartRateController : ControllerBase
    {
        private readonly IExerciseHeartRateRepository _heartRateRepository;

        public ExerciseHeartRateController(IExerciseHeartRateRepository heartRateRepository)
        {
            _heartRateRepository = heartRateRepository;
        }


        [HttpGet]
        public async Task<IActionResult> GetExerciseHeartRate(long exerciseId)
        {
            var heartRate = await _heartRateRepository.GetExerciseHeartRate(exerciseId);
            if (heartRate == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(heartRate);
        }

    }

}