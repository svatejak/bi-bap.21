using backend.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/exercises/{exerciseId}/motion-data")]
    [ApiController]
    public class ExerciseMotionDataController : ControllerBase
    {
        private readonly IExerciseMotionDataRepository _motionDataRepository;

        public ExerciseMotionDataController(IExerciseMotionDataRepository motionDataRepository)
        {
            _motionDataRepository = motionDataRepository;
        }


        [HttpGet]
        public async Task<IActionResult> GetExerciseMotionData(long exerciseId)
        {
            var motionData = await _motionDataRepository.GetExerciseMotionData(exerciseId);
            if (motionData == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(motionData);
        }

    }



}


