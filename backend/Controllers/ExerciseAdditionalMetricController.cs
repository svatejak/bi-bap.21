using Microsoft.AspNetCore.Mvc;
using backend.Models;
using backend.Interfaces;
using backend.Models.Dto;


namespace backend.Controllers
{
    [Route("api/exercises/{exerciseId}/additional-metrics")]
    [ApiController]
    public class ExerciseAdditionalMetricController : ControllerBase
    {
        private readonly IExerciseAdditionalMetricRepository _exerciseAdditionalMetricRepository;
        public ExerciseAdditionalMetricController(IExerciseAdditionalMetricRepository exerciseAdditionalMetricRepository)
        {
            _exerciseAdditionalMetricRepository = exerciseAdditionalMetricRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetExerciseAdditionalMetric(long exerciseId)
        {
            var exerciseAdditionalMetric = await _exerciseAdditionalMetricRepository.GetExerciseAdditionalMetricDto(exerciseId);

            if (exerciseAdditionalMetric == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(exerciseAdditionalMetric);
        }


    }
}