using Microsoft.AspNetCore.Mvc;
using backend.Models;
using backend.Interfaces;
using backend.Models.Dto;


namespace backend.Controllers
{
    [Route("api/sleep/{sleepId}/sleep-stages")]
    [ApiController]
    public class SleepStageController : ControllerBase
    {
        private readonly ISleepStageRepository _sleepStageRepository;
        public SleepStageController(ISleepStageRepository sleepStageRepository)
        {
            _sleepStageRepository = sleepStageRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<SleepStage>))]
        public async Task<IActionResult> GetSleepStageList(long sleepId)
        {
            var sleepStageList = await _sleepStageRepository.GetSleepStageList(sleepId);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(sleepStageList);
        }
    }
}   