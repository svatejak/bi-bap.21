using backend.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace backend.Controllers
{
    [Route("api/exercises/{exerciseId}/location-data")]
    [ApiController]
    public class ExerciseLocationDataController : ControllerBase
    {
        private readonly IExerciseLocationDataRepository _locationDataRepository;

        public ExerciseLocationDataController(IExerciseLocationDataRepository locationDataRepository)
        {
            _locationDataRepository = locationDataRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetExerciseLocationData(long exerciseId)
        {
            var locationData = await _locationDataRepository.GetExerciseLocationData(exerciseId);
            if (locationData == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(locationData);
        }
    }
}