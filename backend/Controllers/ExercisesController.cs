using Microsoft.AspNetCore.Mvc;
using backend.Models;
using backend.Interfaces;
using backend.Models.Dto;

namespace backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExercisesController : ControllerBase
    {
        private readonly IExercisesRepository _exercisesRepository;
        public ExercisesController(IExercisesRepository exercisesRepository)
        {
            _exercisesRepository = exercisesRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(ICollection<Exercise>))]
        public async Task<IActionResult> GetExercises(
            [FromQuery] string sort_by = "start_time",
            [FromQuery] string order = "asc",
            [FromQuery] string filter = "",
            [FromQuery] int page = 1,
            [FromQuery] int limit = int.MaxValue,
            [FromQuery] bool include_position = false)
        {
            if (page < 1)
            {
                return BadRequest("Invalid page number. Page number must be greater than or equal to 1.");
            }


            var ascending = true;
            if (order == "desc")
            {
                ascending = false;
            }

            var exercises = await _exercisesRepository.GetExercisesSorted(sort_by, ascending, filter);

            var numFound = exercises.Count();

            var partialExercisesDto = exercises
            .Skip((page - 1) * limit)
            .Take(limit)
            .Select(exercise => new PartialExerciseDto
            {
                Id = exercise.Id,
                StartTime = exercise.StartTime,
                EndTime = exercise.EndTime,
                ExerciseName = exercise.ExerciseName,
                DataSource = new DatasourceUploadDto
                {
                    Id = exercise.DataSource.Id,
                    DataSource = exercise.DataSource.DataSource,
                    UploadTime = exercise.DataSource.UploadTime
                },
                Position = include_position && exercise.LocationData != null && exercise.LocationData.Count != 0
                ? exercise.LocationData.Where(ld => ld.StartTime == exercise.LocationData.Min(ld => ld.StartTime)).Select(ld => new PositionDto
                {
                    Latitude = ld.Latitude,
                    Longitude = ld.Longitude
                }).FirstOrDefault()
                : null
            }).ToList();

            var response = new ExercisesResponse
            {
                NumFound = numFound,
                Exercises = partialExercisesDto
            };

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(response);
        }


        [HttpGet("{exerciseId}")]
        [ProducesResponseType(200, Type = typeof(Exercise))]
        public async Task<IActionResult> GetExercise(long exerciseId)
        {
            var exerciseDto = await _exercisesRepository.GetExerciseDto(exerciseId);

            if (exerciseDto == null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            return Ok(exerciseDto);
        }
    }
}