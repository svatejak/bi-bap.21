using System.Xml;
using backend.Data;
using backend.Interfaces;
using backend.Models;


namespace backend.Services
{
    public class ProcessGoogleFitDataService : IProcessGoogleFitDataService
    {
        private readonly AppDbContext _context;
        private readonly IFileRecordProcessingService _fileRecordProcessingService;
        private readonly IFileLocationService _fileLocationService;
        private readonly IDatabaseService _databaseService;

        public ProcessGoogleFitDataService(AppDbContext context,
                                        IFileRecordProcessingService fileRecordProcessingService,
                                        IFileLocationService fileLocationService,
                                        IDatabaseService databaseService)
        {
            _context = context;
            _fileRecordProcessingService = fileRecordProcessingService;
            _fileLocationService = fileLocationService;
            _databaseService = databaseService;
        }

        public void ValidateData()
        {
            if (!_fileLocationService.GoogleFitHasDirectories())
            {
                throw new Exception($"Bad Google Fit file structure");
            }
        }

        private DatasourceUpload CreateGoogleFitDatasourceUpload()
        {
            var datasourceUpload = new DatasourceUpload
            {
                DataSource = "GoogleFit",
                UploadTime = DateTime.UtcNow
            };

            _context.DatasourceUploads.Add(datasourceUpload);
            _context.SaveChanges();
            return datasourceUpload;
        }

        public void ProcessData()
        {
            var datasourceUpload = CreateGoogleFitDatasourceUpload();
            ProcessExerciseData(datasourceUpload);
        }

        private void ProcessExerciseData(DatasourceUpload datasourceUpload)
        {
            var allSessionPaths = _fileLocationService.GetGoogleFitFilePaths("All Sessions");

            var allActivitiesPaths = _fileLocationService.GetGoogleFitFilePaths("Activities");

            var exerciseList = new List<Exercise>();
            var heartRateData = new List<ExerciseHeartRate>();
            var locationData = new List<ExerciseLocationData>();
            for (int i = 0; i < allSessionPaths.Length; i++)
            {
                var sessionPath = allSessionPaths[i];
                var activityPath = _fileLocationService.GetGoogleFitActivityFilePath(sessionPath, allActivitiesPaths);

                var exercise = _fileRecordProcessingService.GetExerciseFromGoogleFitFile(allSessionPaths[i], datasourceUpload);
                exerciseList.Add(exercise);

                _fileRecordProcessingService.GetExerciseAdditionalData(activityPath, exercise, heartRateData, locationData);
            }
            
            heartRateData = [.. heartRateData.OrderBy(x => x.StartTime)];
            locationData = [.. locationData.OrderBy(x => x.StartTime)];

            _databaseService.BulkInsertion(exerciseList);
            _databaseService.BulkInsertion(heartRateData);
            _databaseService.BulkInsertion(locationData);
        }
    }









}