using System;
using System.IO;
using System.Threading.Tasks;
using Minio.AspNetCore;

using backend.Interfaces;

using Minio;
using Minio.DataModel;
using Minio.DataModel.Args;
using Minio.Exceptions;
using System.ComponentModel;
using backend.Models;



namespace backend.Services;
public class MinioService : IMinioService
{
    private readonly IMinioClient _minioClient;

    public MinioService()
    {
        var endpoint = Environment.GetEnvironmentVariable("MINIO_ENDPOINT");
        var accessKey = Environment.GetEnvironmentVariable("MINIO_ROOT_USER");
        var secretKey = Environment.GetEnvironmentVariable("MINIO_ROOT_PASSWORD");
        var minioClient = new MinioClient()
            .WithEndpoint(endpoint)
            .WithCredentials(accessKey, secretKey)
            .Build() ?? throw new Exception("MinioClient not created");
        
        _minioClient = minioClient;
    }

    public void ListBuckets()
    {
        var list = _minioClient.ListBucketsAsync();
        foreach (Bucket bucket in list.Result.Buckets)
        {
            Console.WriteLine(bucket.Name + " " + bucket.CreationDateDateTime);
        }
    }

    public void ListObjects(string bucketName)
    {
        bool recursive = true;

        var objectArgs = new ListObjectsArgs()
            .WithBucket(bucketName)
            .WithRecursive(recursive);

        var observable = _minioClient.ListObjectsAsync(objectArgs);
        observable.Subscribe(
            item => Console.WriteLine("OnNext: {0}", item.Key),
            ex => Console.WriteLine("OnError: {0}", ex.Message),
            () => Console.WriteLine("OnComplete: {0}"));
    }

    public async Task GetObjectsToFile(IEnumerable<FileMetadata> files, string localDirectoryPath, string bucketName = "default")
    {

        foreach (var file in files)
        {
            StatObjectArgs statObjectArgs = new StatObjectArgs()
                                                .WithBucket(bucketName)
                                                .WithObject(file.Name);
            await _minioClient.StatObjectAsync(statObjectArgs);

            string localFilePath = Path.Combine(localDirectoryPath, file.Name);
            GetObjectArgs getObjectArgs = new GetObjectArgs()
                                            .WithBucket(bucketName)
                                            .WithObject(file.Name)
                                            .WithFile(localFilePath);
            await _minioClient.GetObjectAsync(getObjectArgs);
        }
    }

    public void CleanBucket(string bucketName="default")
    {
        var objectArgs = new ListObjectsArgs()
            .WithBucket(bucketName)
            .WithRecursive(true);

        var observable = _minioClient.ListObjectsAsync(objectArgs);
        observable.Subscribe(
            async item =>
            {
                RemoveObjectArgs removeObjectArgs = new RemoveObjectArgs()
                    .WithBucket(bucketName)
                    .WithObject(item.Key);
                await _minioClient.RemoveObjectAsync(removeObjectArgs);
            });
    }

    public async Task<bool> ContainsObject(string objectName, string bucketName = "default")
    {
        try
        {
            var objectArgs = new StatObjectArgs()
            .WithBucket(bucketName)
            .WithObject(objectName);

            await _minioClient.StatObjectAsync(objectArgs);
            return true;
        }
        catch
        {
            return false;
        }
    }





}
