using System.Text.RegularExpressions;
using backend.Models;
using Microsoft.VisualBasic;
using backend.Models.FileRecords.Csv;
using backend.Data;
using backend.Interfaces;

namespace backend.Services
{
    public class ProcessSamsungHealthDataService : IProcessSamsungHealthDataService
    {
        private readonly IRecordsFromFileService _recordsFromFileService;
        private readonly AppDbContext _context;
        private readonly IFileRecordProcessingService _fileRecordProcessingService;
        private readonly IFileLocationService _fileLocationService;
        private readonly IDatabaseService _databaseService;
        public ProcessSamsungHealthDataService(IRecordsFromFileService recordsFromFileService,
                                            AppDbContext context,
                                            IFileRecordProcessingService fileRecordProcessingService,
                                            IFileLocationService fileLocationService,
                                            IDatabaseService databaseService)
        {
            _recordsFromFileService = recordsFromFileService;
            _context = context;
            _fileRecordProcessingService = fileRecordProcessingService;
            _fileLocationService = fileLocationService;
            _databaseService = databaseService;
        }

        public void ValidateData()
        {
            if (!_fileLocationService.SamsungHealthHasDirectories())
            {
                throw new Exception($"Bad Samsung Health file structure");
            }
        }

        private DatasourceUpload CreateSamsungHealthDatasourceUpload()
        {
            var datasourceUpload = new DatasourceUpload
            {
                DataSource = "SamsungHealth",
                UploadTime = DateTime.UtcNow
            };

            _context.DatasourceUploads.Add(datasourceUpload);
            _context.SaveChanges();
            return datasourceUpload;
        }

        public void ProcessData()
        {
            var datasourceUpload = CreateSamsungHealthDatasourceUpload();
            var files = _fileLocationService.GetSamsungHealthFiles();

            ProcessExerciseData(files, datasourceUpload);
            ProcessSleepData(files, datasourceUpload);
        }

        private void ProcessExerciseData(IEnumerable<string> files, DatasourceUpload datasourceUpload)
        {
            var exercisePath = _fileLocationService.GetSamsungHealthCSVFilePath(files, "exercise");
            if (exercisePath == null)
            {
                return;
            }
            IEnumerable<ExerciseFileRecord> exerciseFileRecords = _recordsFromFileService.GetRecordsFromCsv<ExerciseFileRecord>(exercisePath);

            var exercises = _fileRecordProcessingService.GetExerciseRecords(exerciseFileRecords, datasourceUpload);

            var subsetData = _fileRecordProcessingService.GetExerciseSubsetData(exerciseFileRecords, exercises);

            var locationData = _fileRecordProcessingService.GetExerciseLocationData(exerciseFileRecords, exercises);

            var addtionalMetricsData = _fileRecordProcessingService.GetExerciseAdditionalMetrics(exerciseFileRecords, exercises);

            var liveData = _fileRecordProcessingService.GetExerciseLiveData(exerciseFileRecords, exercises);
            var heartRateData = liveData.Item1;
            var motionData = liveData.Item2;

            locationData = [.. locationData.OrderBy(x => x.StartTime)];
            heartRateData = [.. heartRateData.OrderBy(x => x.StartTime)];
            motionData = [.. motionData.OrderBy(x => x.StartTime)];

            _databaseService.BulkInsertion(exercises);
            _databaseService.BulkInsertion(subsetData);
            _databaseService.BulkInsertion(locationData);
            _databaseService.BulkInsertion(addtionalMetricsData);
            _databaseService.BulkInsertion(heartRateData);
            _databaseService.BulkInsertion(motionData);


            var recoveryHeartRate = _fileLocationService.GetSamsungHealthCSVFilePath(files, "exercise.recovery_heart_rate");
            if (recoveryHeartRate != null)
            {
                Console.WriteLine("Processing recovery heart rate data");
                Console.WriteLine(recoveryHeartRate);
                IEnumerable<RecoveryHeartRateRecord> recoveryHeartRateFileRecords = _recordsFromFileService.GetRecordsFromCsv<RecoveryHeartRateRecord>(recoveryHeartRate);
                var recoveryHeartRateData = _fileRecordProcessingService.GetRecoveryHeartRateData(recoveryHeartRateFileRecords, exercises);
                recoveryHeartRateData = [.. recoveryHeartRateData.OrderBy(x => x.StartTime)];
                _databaseService.BulkInsertion(recoveryHeartRateData);
            }
        }

        private void ProcessSleepData(IEnumerable<string> files, DatasourceUpload datasourceUpload)
        {
            var sleepPath = _fileLocationService.GetSamsungHealthCSVFilePath(files, "sleep");
            if (sleepPath == null)
            {
                return;
            }

            IEnumerable<SleepFileRecord> sleepFileRecords = _recordsFromFileService.GetRecordsFromCsv<SleepFileRecord>(sleepPath);

            // there are also raw sleep data which can be added in the future for additional analysis

            var sleepStagePath = _fileLocationService.GetSamsungHealthCSVFilePath(files, "sleep_stage");
            if (sleepStagePath == null)
            {
                return;
            }

            IEnumerable<SleepStageFileRecord> sleepStageFileRecords = _recordsFromFileService.GetRecordsFromCsv<SleepStageFileRecord>(sleepStagePath);

            var sleepData = _fileRecordProcessingService.GetSleepRecords(sleepFileRecords, datasourceUpload);

            var sleepStageData = _fileRecordProcessingService.GetSleepStageRecords(sleepStageFileRecords, sleepData);
            sleepStageData = [.. sleepStageData.OrderBy(x => x.StartTime)];

            _databaseService.BulkInsertion(sleepData);
            _databaseService.BulkInsertion(sleepStageData);

        }

    }

};

