using backend.Interfaces;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Configuration.Attributes;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;
using System.IO;


namespace backend.Services
{
    public class RecordsFromFileService: IRecordsFromFileService
    {
        public RecordsFromFileService()
        {
        }

        public IEnumerable<T> GetRecordsFromCsv<T>(string filePath) where T : class, new()
        {
            using (var reader = new StreamReader(filePath))
            {
                var config = new CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    HasHeaderRecord = true,
                    HeaderValidated = null,
                    MissingFieldFound = null,
                    Delimiter = ","
                };

                using (var csv = new CsvReader(reader, config))
                {
                    csv.Read(); // Skip the first row, bcs header is on the second row
                    csv.Read();
                    csv.ReadHeader();

                    var records = csv.GetRecords<T>().ToList();
                    return records;
                }
            }
        }

        public dynamic GetRecordsFromJson(string filePath)
        {
            string jsonString = File.ReadAllText(filePath);
            var data = JsonConvert.DeserializeObject<dynamic>(jsonString) ?? throw new Exception("Failed to deserialize json data");
            return data;
        }

    }


}