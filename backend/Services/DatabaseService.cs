using backend.Data;
using EFCore.BulkExtensions;
// using Z.EntityFramework.Extensions;
using backend.Interfaces;

namespace backend.Services
{
    public class DatabaseService : IDatabaseService
    {
        private readonly AppDbContext _context;
        public DatabaseService(AppDbContext context)
        {
            _context = context;
        }

        public void BulkInsertion<T>(IEnumerable<T> records) where T : class
        {
            foreach (var record in records)
            {
                _context.Add(record);
            }
            _context.SaveChanges();
        }

        // licensed version of Z.EntityFramework.Extensions, which should be more efficient.
        // public void BulkInsertion<T>(IEnumerable<T> records) where T : class
        // {
        //     EntityFrameworkManager.ContextFactory = context => _context;
        //     _context.BulkInsert(records);
        // }
    }
}