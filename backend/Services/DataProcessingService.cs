using System.Reflection.Metadata;
using backend.Interfaces;
using backend.Models;
using Microsoft.AspNetCore.Mvc;
using System.IO.Compression;

namespace backend.Services;

public class DataProcessingService : IDataProcessingService
{
    private readonly List<string> _validDataSources;
    private readonly string _localDirectoryPath;
    private readonly IMinioService _minioService;
    private readonly IProcessSamsungHealthDataService _processSamsungHealthDataService;
    private readonly IProcessGoogleFitDataService _processGoogleFitDataService;
    public DataProcessingService(IMinioService minioService, IProcessSamsungHealthDataService processSamsungHealthDataService, IConfiguration configuration, IProcessGoogleFitDataService processGoogleFitDataService)
    {
        _validDataSources = configuration.GetSection("ValidDataSources").Get<List<string>>() ?? throw new Exception("ValidDataSources not found in configuration");
        _localDirectoryPath = configuration.GetValue<string>("LocalDirectoryPath") ?? throw new Exception("LocalDirectoryPath not found in configuration");
        _minioService = minioService;
        _processSamsungHealthDataService = processSamsungHealthDataService;
        _processGoogleFitDataService = processGoogleFitDataService;
    }

    public IActionResult ValidateFilesMetadata(List<FileMetadata> files)
    {
        foreach (var file in files)
        {
            if (!file.Name.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
            {
                return new BadRequestObjectResult("Invalid file type");
            }

            if (!_validDataSources.Contains(file.DataSource))
            {
                return new BadRequestObjectResult("Invalid data source");
            }
        }

        if (files.Count == 0)
        {
            return new BadRequestObjectResult("No files to process");
        }

        if (files.Count == 2 && files[0].DataSource == files[1].DataSource)
        {
            return new BadRequestObjectResult("Duplicate data sources");
        }
        
        if ( files.Count > 2)
        {
            return new BadRequestObjectResult("Too many files");
        }

        return new OkResult();
    }

    public async Task<IActionResult> SetupFileProcessing(List<FileMetadata> files)
    {
        try {
            CleanDirectory();

            await _minioService.GetObjectsToFile(files, _localDirectoryPath);

            _minioService.CleanBucket();

            UnzipFiles(files);
        }
        catch (Exception)
        {
            return new ObjectResult("Internal error") { StatusCode = 500 };
        }
        return new OkResult();
    }

    private void CleanDirectory()
    {
        if (!Directory.Exists(_localDirectoryPath))
        {
            Directory.CreateDirectory(_localDirectoryPath);
            return;
        }

        DirectoryInfo directory = new DirectoryInfo(_localDirectoryPath);
        foreach (FileInfo file in directory.GetFiles())
        {
            file.Delete();
        }
        foreach (DirectoryInfo subDirectory in directory.GetDirectories())
        {
            subDirectory.Delete(true);
        }
    }

    private void UnzipFiles(List<FileMetadata> files)
    {
        try
        {
            foreach (var file in files)
            {
                string filePath = Path.Combine(_localDirectoryPath, file.Name);
                string extractPath = Path.Combine(_localDirectoryPath, file.DataSource);
                
                ZipFile.ExtractToDirectory(filePath, extractPath);
                File.Delete(filePath);
            }
        }
        catch (Exception)
        {
            throw new Exception("Error occurred during unzip");
        }
    }

    public IActionResult ValidateFiles() // this could be improved in the future
    {
        try {
            if (Directory.Exists(Path.Combine(_localDirectoryPath, "SamsungHealth")))
            {
                _processSamsungHealthDataService.ValidateData();
            }

            if (Directory.Exists(Path.Combine(_localDirectoryPath, "GoogleFit")))
            {
                _processGoogleFitDataService.ValidateData();
            }
        }
        catch (Exception ex)
        {
            return new BadRequestObjectResult(ex.Message);
        }

        return new OkResult();
    }

    public IActionResult ProcessFiles()
    {
        var currFile = "";
        try {
            if (Directory.Exists(Path.Combine(_localDirectoryPath, "SamsungHealth")))
            {
                currFile = "SamsungHealth";
                Console.WriteLine("Processing SamsungHealth");
                _processSamsungHealthDataService.ProcessData();
            }

            if (Directory.Exists(Path.Combine(_localDirectoryPath, "GoogleFit")))
            {
                currFile = "GoogleFit";
                Console.WriteLine("Processing GoogleFit");
                _processGoogleFitDataService.ProcessData();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new BadRequestObjectResult($"Error occurred during processing {currFile}");
        }

        return new OkResult();
    }
    

    

}