using backend.Interfaces;

namespace backend.Services {
    public class CodeToNameService : ICodeToNameService
    {
        public string GetSleepStageName(int? stage)
        {
            if (stage == null)
            {
                return "Unknown";
            }
            
            switch (stage)
            {
                case 40001:
                    return "Awake";
                case 40002:
                    return "Light";
                case 40003:
                    return "Deep";
                case 40004:
                    return "REM";
                default:
                    return "Unknown";
            }
        }

        public string GetExerciseName(string? exerciseType)
        {
            if (exerciseType == null)
            {
                return "Custom type";
            }
            
            int id = Convert.ToInt32(exerciseType);
            switch (id)
            {
                case 0:
                    return "Custom type";
                case 1001:
                    return "Walking";
                case 1002:
                    return "Running";
                case 2001:
                    return "Baseball, general";
                case 2002:
                    return "Softball, general";
                case 2003:
                    return "Cricket";
                case 3001:
                    return "Golf, general";
                case 3002:
                    return "Billiards";
                case 3003:
                    return "Bowling, alley";
                case 4001:
                    return "Hockey";
                case 4002:
                    return "Rugby, touch, non-competitive";
                case 4003:
                    return "Basketball, general";
                case 4004:
                    return "Football, general (Soccer)";
                case 4005:
                    return "Handball, general";
                case 4006:
                    return "American football, general, touch";
                case 5001:
                    return "Volleyball, general, 6~9 member team, non-competitive";
                case 5002:
                    return "Beach volleyball";
                case 6001:
                    return "Squash, general";
                case 6002:
                    return "Tennis, general";
                case 6003:
                    return "Badminton, competitive";
                case 6004:
                    return "Table tennis";
                case 6005:
                    return "Racquetball, general";
                case 7001:
                    return "T'ai chi, general";
                case 7002:
                    return "Boxing, in ring";
                case 7003:
                    return "Martial arts, moderate pace (Judo, Jujitsu, Karate, Taekwondo)";
                case 8001:
                    return "Ballet, general, rehearsal or class";
                case 8002:
                    return "Dancing, general (Fork, Irish step, Polka)";
                case 8003:
                    return "Ballroom dancing, fast";
                case 9001:
                    return "Pilates";
                case 9002:
                    return "Yoga";
                case 10001:
                    return "Stretching";
                case 10002:
                    return "Jump rope, moderate pace (100~120 skips/min), 2 foot skip";
                case 10003:
                    return "Hula-hooping";
                case 10004:
                    return "Push-ups (Press-ups)";
                case 10005:
                    return "Pull-ups (Chin-up)";
                case 10006:
                    return "Sit-ups";
                case 10007:
                    return "Circuit training, moderate effort";
                case 10008:
                    return "Mountain climbers";
                case 10009:
                    return "Jumping Jacks";
                case 10010:
                    return "Burpee";
                case 10011:
                    return "Bench press";
                case 10012:
                    return "Squats";
                case 10013:
                    return "Lunges";
                case 10014:
                    return "Leg presses";
                case 10015:
                    return "Leg extensions";
                case 10016:
                    return "Leg curls";
                case 10017:
                    return "Back extensions";
                case 10018:
                    return "Lat pull-downs";
                case 10019:
                    return "Deadlifts";
                case 10020:
                    return "Shoulder presses";
                case 10021:
                    return "Front raises";
                case 10022:
                    return "Lateral raises";
                case 10023:
                    return "Crunches";
                case 10024:
                    return "Leg raises";
                case 10025:
                    return "Plank";
                case 10026:
                    return "Arm curls";
                case 10027:
                    return "Arm extensions";
                case 11001:
                    return "Inline skating, moderate pace";
                case 11002:
                    return "Hang gliding";
                case 11003:
                    return "Pistol shooting";
                case 11004:
                    return "Archery, non-hunting";
                case 11005:
                    return "Horseback riding, general";
                case 11007:
                    return "Cycling";
                case 11008:
                    return "Flying disc, general, playing";
                case 11009:
                    return "Roller skating";
                case 12001:
                    return "Aerobics, general";
                case 13001:
                    return "Hiking";        
                case 13002:
                    return "Rock climbing, low to moderate difficulty";
                case 13003:
                    return "Backpacking";
                case 13004:
                    return "Mountain biking, general";
                case 13005:
                    return "Orienteering";
                case 14001:
                    return "Swimming, general, leisurely, not lap swimming";
                case 14002:
                    return "Aquarobics";
                case 14003:
                    return "Canoeing, general, for pleasure";
                case 14004:
                    return "Sailing, leisure, ocean sailing";
                case 14005:
                    return "Scuba diving, general";
                case 14006:
                    return "Snorkeling";
                case 14007:
                    return "Kayaking, moderate effort";
                case 14008:
                    return "Kitesurfing";
                case 14009:
                    return "Rafting";
                case 14010:
                    return "Rowing machine, general, for pleasure";
                case 14011:
                    return "Windsurfing, general";
                case 14012:
                    return "Yachting, leisure";
                case 14013:
                    return "Water skiing";
                case 15001:
                    return "Step machine";
                case 15002:
                    return "Weight machine";
                case 15003:
                    return "Exercise bike, Moderate to vigorous effort (90-100 watts)";
                case 15004:
                    return "Rowing machine";
                case 15005:
                    return "Treadmill, combination of jogging and walking";
                case 15006:
                    return "Elliptical trainer, moderate effort";
                case 16001:
                    return "Cross-country skiing, general, moderate speed (4.0~4.9 mph)";
                case 16002:
                    return "Skiing, general, downhill, moderate effort";
                case 16003:
                    return "Ice dancing";
                case 16004:
                    return "Ice skating, general";
                case 16006:
                    return "Ice hockey, general";
                case 16007:
                    return "Snowboarding, general, moderate effort";
                case 16008:
                    return "Alpine skiing, general, moderate effort";
                case 16009:
                    return "Snowshoeing, moderate effort";
                default:
                    return "Custom type";
            }
        
        }
    }
}
