using System.Text.RegularExpressions;
using backend.Interfaces;

namespace backend.Services
{
    public class FileLocationService : IFileLocationService
    {
        readonly string _SamsungHealthBasePath;
        readonly string _GoogleFitPath;

        public FileLocationService(IConfiguration configuration)
        {
            _SamsungHealthBasePath = configuration.GetValue<string>("SamsungHealthPath") ?? throw new Exception("SamsungHealthBasePath not found in configuration");
            _GoogleFitPath = configuration.GetValue<string>("GoogleFitPath") ?? throw new Exception("GoogleFitPath not found in configuration");
        }

        private string GetSamsungHealthFullPath()
        {
            var subDirectories = Directory.EnumerateDirectories(_SamsungHealthBasePath);
            var fullPath = subDirectories.FirstOrDefault() ?? throw new Exception("No subdirectory found in SamsungHealth base path");

            return fullPath;
        }

        public bool SamsungHealthHasDirectories()
        {
            string fullPath = GetSamsungHealthFullPath();
            if (Directory.Exists(fullPath))
            {
                var directories = Directory.EnumerateDirectories(fullPath);
                return directories.Any();
            }
            return false;
        }

        public bool GoogleFitHasDirectories()
        {
            if (Directory.Exists(_GoogleFitPath))
            {
                var directories = Directory.EnumerateDirectories(_GoogleFitPath);
                return directories.Any();
            }
            return false;
        }

        public string[] GetSamsungHealthFiles()
        {
            string fullPath = GetSamsungHealthFullPath();
            return Directory.GetFiles(fullPath);
        }

        public string? GetSamsungHealthCSVFilePath(IEnumerable<string> files, string pattern)
        {
            string prefix1 = "com.samsung.health.";
            string prefix2 = "com.samsung.shealth.";
            string regexPattern = $"({Regex.Escape(prefix1)}{pattern}(?=\\.\\d+)|{Regex.Escape(prefix2)}{pattern}(?=\\.\\d+))";

            string? matchingFile = files.FirstOrDefault(f => Regex.IsMatch(f, regexPattern));
            return matchingFile;
        }

        public string GetSamsungHealthJSONFilePath(string fileName, string folderName)
        {
            string fullPath = GetSamsungHealthFullPath();
            return Path.Combine(fullPath, "jsons", folderName, fileName[0].ToString(), fileName);
        }

        public string[] GetGoogleFitFilePaths(string pattern)
        {
            var directories = Directory.GetDirectories(_GoogleFitPath)
                .Where(folder => folder.EndsWith(pattern, StringComparison.OrdinalIgnoreCase))
                .ToArray();

            return Directory.GetFiles(directories[0]);
        }

        public string GetGoogleFitActivityFilePath(string sessionPath, string[] allActivitiesPaths)
        {
            var fileName = Path.GetFileName(sessionPath);
            const int fileIdLen = 25;
            var fileId = fileName[..fileIdLen];

            return allActivitiesPaths.FirstOrDefault(p => p.Contains(fileId)) ?? throw new Exception($"No matching activity file found for {fileId}");
        }
    }
}
