


using backend.Models;
using backend.Models.FileRecords.Csv;
using backend.Interfaces;
using backend.Data;
using Newtonsoft.Json;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Xml;
using backend.Extensions;

namespace backend.Services
{
    public class FileRecordProcessingService : IFileRecordProcessingService
    {
        private readonly IFileLocationService _fileLocationService;
        private readonly IRecordsFromFileService _recordsFromFileService;
        private readonly ICodeToNameService _codeToNameService;
        public FileRecordProcessingService( IFileLocationService fileLocationService, IRecordsFromFileService recordsFromFileService, ICodeToNameService codeToNameService)
        {
            _fileLocationService = fileLocationService;
            _recordsFromFileService = recordsFromFileService;
            _codeToNameService = codeToNameService;
        }

        public List<Exercise> GetExerciseRecords(IEnumerable<ExerciseFileRecord> exerciseFileRecords, DatasourceUpload datasourceUpload)
        {
            var exercises = new List<Exercise>();
            TimeSpan offset = TimeSpan.Zero;
            foreach (var record in exerciseFileRecords)
            {
                DateTime? startTimeUtc = record.StartTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(record.StartTime.Value).Add(offset) : null;
                DateTime? endTimeUtc = record.EndTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(record.EndTime.Value).Add(offset) : null;

                exercises.Add(new Exercise
                {
                    DataUuid = record.DataUuid,
                    StartTime = startTimeUtc,
                    EndTime = endTimeUtc,
                    Duration = record.Duration,
                    Distance = record.Distance,
                    TotalCalories = record.TotalCalories,
                    ExerciseName = _codeToNameService.GetExerciseName(record.ExerciseName),
                    MaxSpeed = record.MaxSpeed,
                    MeanSpeed = record.MeanSpeed,
                    InclineDistance = record.InclineDistance,
                    DeclineDistance = record.DeclineDistance,
                    MaxAltitude = record.MaxAltitude,
                    MinAltitude = record.MinAltitude,
                    MeanHeartRate = record.MeanHeartRate,
                    MaxHeartRate = record.MaxHeartRate,
                    MinHeartRate = record.MinHeartRate,
                    HeartRateSampleCount = record.HeartRateSampleCount,
                    MeanCadence = record.MeanCadence,
                    MaxCadence = record.MaxCadence,
                    TimeOffset = record.TimeOffset,
                    StepCount = record.StepCount,
                    DataSource = datasourceUpload
                });
            }
            return exercises;
        }

        public List<ExerciseSubsetData> GetExerciseSubsetData(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises)
        {
            var subsetData = new List<ExerciseSubsetData>();
            foreach (var record in exerciseFileRecords)
            {
                if (record.SubsetData != null && record.SubsetData.Length != 0)
                {
                    var subsetRecords = JsonConvert.DeserializeObject<List<ExerciseSubsetData>>(record.SubsetData) ?? throw new Exception("Failed to deserialize subset data");
                    var exercise = exercises.FirstOrDefault(e => e.DataUuid == record.DataUuid);
                    if (exercise == null) {
                        continue;
                    }
                    foreach (var subsetRecord in subsetRecords)
                    {
                        var subset = new ExerciseSubsetData
                        {
                            Duration = subsetRecord.Duration,
                            Reps = subsetRecord.Reps,
                            Weight = subsetRecord.Weight,
                            Exercise = exercise
                        };

                        subsetData.Add(subset);
                    }
                }
            }

            return subsetData;
        }

        public List<ExerciseLocationData> GetExerciseLocationData(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises)
        {
            var locationData = new List<ExerciseLocationData>();
            foreach (var record in exerciseFileRecords)
            {
                if (record.LocationData != null && record.LocationData.Length != 0)
                {
                    var exercise = exercises.FirstOrDefault(e => e.DataUuid == record.DataUuid);
                    if (exercise == null) {
                        continue;
                    }
                    var locationDataPath = _fileLocationService.GetSamsungHealthJSONFilePath(record.LocationData, "com.samsung.shealth.exercise");
                    var records = _recordsFromFileService.GetRecordsFromJson(locationDataPath);
                    foreach (var locationRecord in records)
                    {
                        var location = new ExerciseLocationData
                        {
                            Latitude = locationRecord.latitude,
                            Longitude = locationRecord.longitude,
                            Altitude = locationRecord.altitude,
                            Accuracy = locationRecord.accuracy,
                            StartTime = locationRecord.start_time,
                            Exercise = exercise
                        };
                        locationData.Add(location);
                    }
                }
            }
            return locationData;
        }

        public List<ExerciseAdditionalMetric> GetExerciseAdditionalMetrics(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises)
        {
            var additionalMetrics = new List<ExerciseAdditionalMetric>();
            foreach (var record in exerciseFileRecords)
            {
                if (record.AdditionalInternal != null && record.AdditionalInternal.Length != 0)
                {
                    var exercise = exercises.FirstOrDefault(e => e.DataUuid == record.DataUuid);
                    if (exercise == null) {
                        continue;
                    }
                    var locationDataPath = _fileLocationService.GetSamsungHealthJSONFilePath(record.AdditionalInternal, "com.samsung.shealth.exercise");
                    var records = _recordsFromFileService.GetRecordsFromJson(locationDataPath);

                    foreach (var item in records["data"])
                    {
                        var advancedMetrics = item["advanced_metrics"];
                        foreach (var metric in advancedMetrics)
                        {
                            var additionalMetric = new ExerciseAdditionalMetric
                            {
                                DataType = (string)metric["data_type"],
                                Score = (int?)metric["overall_score"],
                                Exercise = exercise
                            };
                            additionalMetrics.Add(additionalMetric);

                            var metricRecords = metric["chart_data"];
                            var additionalMetricRecords = new List<ExerciseAdditionalMetricRecord>();
                            foreach (var metricRecord in metricRecords)
                            {
                                var additionalMetricRecord = new ExerciseAdditionalMetricRecord
                                {
                                    Duration = (long)metricRecord["duration"],
                                    Score = (int)metricRecord["score"],
                                    Value = (double)metricRecord["value"],
                                    ExerciseAdditionalMetric = additionalMetric
                                };
                                additionalMetricRecords.Add(additionalMetricRecord);
                            }

                            additionalMetric.Records = additionalMetricRecords;
                        }
                    }
                }
            }
            return additionalMetrics;
        }

        public Tuple<List<ExerciseHeartRate>, List<ExerciseMotionData>> GetExerciseLiveData(IEnumerable<ExerciseFileRecord> exerciseFileRecords, List<Exercise> exercises)
        {
            var heartRateData = new List<ExerciseHeartRate>();
            var motionData = new List<ExerciseMotionData>();

            foreach (var record in exerciseFileRecords)
            {
                if (record.LiveData != null && record.LiveData.Length != 0)
                {
                    var exercise = exercises.FirstOrDefault(e => e.DataUuid == record.DataUuid);
                    if (exercise == null) {
                        continue;
                    }
                    var locationDataPath = _fileLocationService.GetSamsungHealthJSONFilePath(record.LiveData, "com.samsung.shealth.exercise");
                    var records = _recordsFromFileService.GetRecordsFromJson(locationDataPath);

                    foreach (var item in records)
                    {
                        if (item.ContainsKey("heart_rate"))
                        {
                            var heartRateRecord = new ExerciseHeartRate
                            {
                                HeartRate = Convert.ToInt32(item["heart_rate"]),
                                StartTime = Convert.ToInt64(item["start_time"]),
                                Exercise = exercise
                            };
                            heartRateData.Add(heartRateRecord);
                        }
                        else if (item.ContainsKey("cadence") || item.ContainsKey("speed") || item.ContainsKey("distance"))
                        {
                            var motionDataRecord = new ExerciseMotionData
                            {
                                Cadence = item.ContainsKey("cadence") ? Convert.ToInt32(item["cadence"]) : default(int?),
                                StartTime = Convert.ToInt64(item["start_time"]),
                                Speed = item.ContainsKey("speed") ? Convert.ToSingle(item["speed"]) : default(float?),
                                Distance = item.ContainsKey("distance") ? Convert.ToSingle(item["distance"]) : default(float?),
                                Exercise = exercise
                            };
                            motionData.Add(motionDataRecord);
                        }
                    }
                }
            }

            return Tuple.Create(heartRateData, motionData);
        }


        public List<ExerciseRecoveryHeartRateData> GetRecoveryHeartRateData(IEnumerable<RecoveryHeartRateRecord> recoveryHeartRateFileRecords, List<Exercise> exercises)
        {
            var recoveryHeartRates = new List<ExerciseRecoveryHeartRateData>();
            foreach (var record in recoveryHeartRateFileRecords)
            {
                var exercise = exercises.FirstOrDefault(e => e.DataUuid == record.ExerciseId);
                if (exercise == null) {
                    continue;
                }


                var FileName = record.FileName + ".heart_rate.json";

                var locationDataPath = _fileLocationService.GetSamsungHealthJSONFilePath(FileName, "com.samsung.shealth.exercise.recovery_heart_rate");
                var records = _recordsFromFileService.GetRecordsFromJson(locationDataPath);

                if (records.ContainsKey("chart_data"))
                {
                    foreach (var item in records["chart_data"])
                    {
                        var recoveryHeartRate = new ExerciseRecoveryHeartRateData
                        {
                            HeartRate = Convert.ToInt32(item["heart_rate"]),
                            StartTime = Convert.ToInt64(item["start_time"]),
                            ElapsedTime = Convert.ToInt64(item["elapsed_time"]),
                            Exercise = exercise
                        };
                        recoveryHeartRates.Add(recoveryHeartRate);
                    }
                }
            }
            return recoveryHeartRates;
        }

        public Exercise GetExerciseFromGoogleFitFile(string fileName, DatasourceUpload datasourceUpload)
        {
            var records = _recordsFromFileService.GetRecordsFromJson(fileName);
            string? exerciseName = records["fitnessActivity"];
            if (exerciseName != null)
            {
                exerciseName = char.ToUpper(exerciseName[0]) + exerciseName.Substring(1);
            }
            string durationString = records["duration"];
            int? duration = null;
            if (durationString != null && durationString != "")
            {
                durationString = durationString.Substring(0, durationString.Length - 1);
                double durationSeconds = double.Parse(durationString);
                duration = (int)(durationSeconds * 1000);
            }

            float? totalCalories = null;
            int? stepCount = null;
            float? distance = null;
            float? meanSpeed = null;
            if (records.ContainsKey("aggregate") && records["aggregate"].Count > 0)
            {
                var aggregate = records["aggregate"];
                foreach (var item in aggregate)
                {
                    switch(item["metricName"].ToString())
                    {
                        case "com.google.calories.expended":
                            totalCalories = item["floatValue"];
                            break;
                        case "com.google.step_count.delta":
                            stepCount = item["intValue"];
                            break;
                        case "com.google.distance.delta":
                            distance = item["floatValue"];
                            break;
                        case "com.google.speed.summary":
                            meanSpeed = item["floatValue"];
                            break;
                    }
                }
            }
            return new Exercise
            {
                StartTime = records["startTime"],
                EndTime = records["endTime"],
                Duration = duration,
                TotalCalories = totalCalories,
                StepCount = stepCount,
                Distance = distance,
                MeanSpeed = meanSpeed,
                ExerciseName = exerciseName,
                DataSource = datasourceUpload
            };
        }


        private XmlNodeList GetExerciseTrackpoints(string fileName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            return doc.GetElementsByTagName("Trackpoint");
        }

        public void GetExerciseAdditionalData(string fileName, Exercise exercise, List<ExerciseHeartRate> heartRateData, List<ExerciseLocationData> locationData)
        {
            var trackpoints = GetExerciseTrackpoints(fileName);

            foreach (XmlNode trackpoint in trackpoints)
            {
                if (trackpoint["HeartRateBpm"] != null || trackpoint["Position"] != null )
                {
                    var trackpointTime = trackpoint["Time"];
                    if (trackpointTime != null)
                    {
                        var dateTime = DateTime.Parse(trackpointTime.InnerText);
                        var unixTimeMilliseconds = new DateTimeOffset(dateTime).ToUnixTimeMilliseconds();

                        var heartRateBpm = trackpoint["HeartRateBpm"];
                        var position = trackpoint["Position"];

                        if (heartRateBpm != null)
                        {
                            var heartRateValue = heartRateBpm["Value"];
                            if (heartRateValue != null)
                            {
                                string heartRateText = heartRateValue.InnerText;
                                int heartRateInt = Convert.ToInt32(double.Parse(heartRateText));

                                var heartRate = new ExerciseHeartRate
                                {
                                    HeartRate = heartRateInt,
                                    StartTime = unixTimeMilliseconds,
                                    Exercise = exercise
                                };
                                heartRateData.Add(heartRate);
                            }
                        } 
                        else if (position != null)
                        {
                            var latitude = position["LatitudeDegrees"];
                            var longitude = position["LongitudeDegrees"];
                            var altitude = trackpoint["AltitudeMeters"];

                            if (latitude != null && longitude != null)
                            {
                                var latitudeValue = latitude.InnerText;
                                var longitudeValue = longitude.InnerText;
                                var altitudeValue = altitude?.InnerText;

                                var location = new ExerciseLocationData
                                {
                                    Latitude = Convert.ToDouble(latitudeValue, CultureInfo.InvariantCulture),
                                    Longitude = Convert.ToDouble(longitudeValue, CultureInfo.InvariantCulture),
                                    Altitude = altitudeValue != null ? Convert.ToDouble(altitudeValue, CultureInfo.InvariantCulture) : default(double?),
                                    StartTime = unixTimeMilliseconds,
                                    Exercise = exercise
                                };
                                locationData.Add(location);
                            }
                        }
                    }
                }
            }
        }

        public List<Sleep> GetSleepRecords(IEnumerable<SleepFileRecord> sleepFileRecords, DatasourceUpload datasourceUpload)
        {
            var sleepList = new List<Sleep>();
            TimeSpan offset = TimeSpan.Zero;
            foreach (var record in sleepFileRecords)
            {
                DateTime? startTimeUtc = record.StartTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(record.StartTime.Value).Add(offset) : null;
                DateTime? endTimeUtc = record.EndTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(record.EndTime.Value).Add(offset) : null;

                sleepList.Add(new Sleep
                {
                    DataUuid = record.DataUuid,
                    MentalRecovery = record.MentalRecovery,
                    Factor1 = record.Factor1,
                    Factor2 = record.Factor2,
                    Factor3 = record.Factor3,
                    Factor4 = record.Factor4,
                    Factor5 = record.Factor5,
                    Factor6 = record.Factor6,
                    Factor7 = record.Factor7,
                    Factor8 = record.Factor8,
                    Factor9 = record.Factor9,
                    Factor10 = record.Factor10,
                    PhysicalRecovery = record.PhysicalRecovery,
                    MovementAwakening = record.MovementAwakening,
                    Cycle = record.SleepCycle,
                    Efficiency = record.Efficiency,
                    Score = record.SleepScore,
                    Duration = record.SleepDuration,
                    StartTime = startTimeUtc,
                    EndTime = endTimeUtc,
                    TimeOffset = record.TimeOffset,
                    DataSource = datasourceUpload
                });
            }
            return sleepList;
        }

        public List<SleepStage> GetSleepStageRecords(IEnumerable<SleepStageFileRecord> sleepStageFileRecords, List<Sleep> sleepData)
        {
            var sleepStages = new List<SleepStage>();
            TimeSpan offset = TimeSpan.Zero;
            foreach (var record in sleepStageFileRecords)
            {
                var sleep = sleepData.FirstOrDefault(e => e.DataUuid == record.SleepGuid);
                if (sleep == null) {
                    continue;
                }

                DateTime? startTimeUtc = record.StartTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(record.StartTime.Value).Add(offset) : null;
                DateTime? endTimeUtc = record.EndTime.HasValue ? TimeZoneInfo.ConvertTimeToUtc(record.EndTime.Value).Add(offset) : null;

                var sleepStage = new SleepStage
                {
                    Stage = _codeToNameService.GetSleepStageName(record.Stage),
                    StartTime = startTimeUtc,
                    EndTime = endTimeUtc,
                    Sleep = sleep,
                    TimeOffset = record.TimeOffset
                };
                sleepStages.Add(sleepStage);                
            }
            return sleepStages;
        }
    }
}