using backend.Data;
using backend.Extensions;
using backend.Interfaces;
using backend.Repository;
using backend.Services;

using Microsoft.EntityFrameworkCore;
using Minio;
using Minio.DataModel;
using Minio.DataModel.Args;


var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;


services.AddEndpointsApiExplorer();
services.AddSwaggerGen();

services.AddDbContext<AppDbContext>(options =>
{
    Task.Delay(3000).Wait();
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));
});

services.AddScoped<IExercisesRepository, ExercisesRepository>();
services.AddScoped<IExerciseMotionDataRepository, ExerciseMotionDataRepository>();
services.AddScoped<IExerciseHeartRateRepository, ExerciseHeartRateRepository>();
services.AddScoped<IExerciseLocationDataRepository, ExerciseLocationDataRepository>();
services.AddScoped<IExerciseRecoveryHeartRateDataRepository, ExerciseRecoveryHeartRateDataRepository>();
services.AddScoped<IExerciseAdditionalMetricRepository, ExerciseAdditionalMetricRepository>();
services.AddScoped<ISleepRepository, SleepRepository>();
services.AddScoped<ISleepStageRepository, SleepStageRepository>();
services.AddControllers();

services.AddCors(options =>
{
    options.AddPolicy("corsPolicy", policyBuilder =>
    {
        policyBuilder
            .WithOrigins("http://localhost:3000", "http://localhost:9000", "http://minio:9000", "http://localhost:9001", "http://minio:9001")
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowCredentials();
    });
});

services.AddScoped<IMinioService, MinioService>();
services.AddScoped<IDatabaseService, DatabaseService>();
services.AddScoped<IDataProcessingService, DataProcessingService>();
services.AddScoped<IProcessSamsungHealthDataService, ProcessSamsungHealthDataService>();
services.AddScoped<IProcessGoogleFitDataService, ProcessGoogleFitDataService>();
services.AddScoped<IRecordsFromFileService, RecordsFromFileService>();
services.AddScoped<IFileRecordProcessingService, FileRecordProcessingService>();
services.AddScoped<IFileLocationService, FileLocationService>();
services.AddSingleton<ICodeToNameService, CodeToNameService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    Console.WriteLine("Aplikace běží v prostředí pro vývoj.");
    app.UseSwagger();
    app.UseSwaggerUI();

    app.ApplyMigrations();
}
else 
{
    Console.WriteLine("Aplikace běží v produkčním prostředí.");
    app.ApplyMigrations();
}


app.UseRouting();

app.UseCors("corsPolicy");

app.UseAuthentication();
app.MapControllers();

app.Run();

