using System;

namespace backend.Configuration
{
    public class IntervalConfiguration
    {
        public int RepetitionCount { get; set; }
        public int IntervalMilliseconds { get; set; }
    }
}