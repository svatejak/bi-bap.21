.PHONY: default
default: up_dev

.PHONY: up_dev up_prod down

up_dev:
	docker-compose -f docker-compose.dev.yml up --build -d

up_prod:
	docker-compose up -d

down:
	docker-compose down -v