# BI-BAP.21

## Start

### Production Environment
To start the production environment, use the following command:

`make up_prod`

### Development Environment
To start the development environment, use the following command:

`make up_dev` or simply `make`

##### Note
If the backend does not function correctly, try running `make` again. Sometimes, the backend, which depends on the database connection, attempts to connect before the database is available.

## Stop

To stop the environment, use the following command:

`make down`

## Webová stránka

Po sestavení je webová stránka dostupná na portu 3000 na URL: `http://localhost:3000`
